#version 330 core

/* Input attributes. */
layout (location = 0) in vec3 vert_pos;
layout (location = 1) in vec3 vert_norm;
layout (location = 2) in vec3 vert_avgpos;
layout (location = 3) in vec3 vert_avgnorm;

/* Uniform variables. */
uniform mat4 model;
uniform mat4 view;
uniform mat4 proj;
uniform bool calc_atten;
uniform float atten_scale;
uniform vec3 camera_pos;

/* Light uniforms. */
uniform vec3 light_pos;
uniform vec4 light_amb;
uniform vec4 light_diff;
uniform vec4 light_spec;

/* Material uniforms. */
uniform vec4 mat_amb;
uniform vec4 mat_diff;
uniform vec4 mat_spec;
uniform float mat_shin;

/* Shader outputs. */
out vec3 frag_pos;
out vec3 frag_norm;
out vec3 frag_avgpos;
out vec3 frag_avgnorm;
out vec4 frag_color_flat;
out vec4 frag_color_smooth;

/* Main function. */
void main() {

    /* Calculate fragment positions and normals. */
    frag_pos = (model * vec4(vert_pos, 1.0)).xyz;
    frag_norm = normalize((model * vec4(vert_norm, 1.0)).xyz);
    frag_avgpos = (model * vec4(vert_avgpos, 1.0)).xyz;
    frag_avgnorm = normalize((model * vec4(vert_avgnorm, 1.0)).xyz);

    /* Calculate constant vectors. */
    vec3 to_light_flat = normalize(light_pos - frag_avgpos);
    vec3 to_camera_flat = normalize(camera_pos - frag_avgpos);
    vec3 reflection_flat = reflect(-to_light_flat, frag_norm);
    float atten_flat = calc_atten ? (atten_scale / (1.0 + pow(distance(light_pos, frag_avgpos), 2.0))) : 1.0;

    /* Calculate constant color. */
    frag_color_flat = atten_flat *
        (mat_amb * light_amb +
        mat_diff * light_diff * max(0, dot(frag_norm, to_light_flat)) +
        mat_spec * light_spec * pow(max(0, dot(reflection_flat, to_camera_flat)), mat_shin));

    /* Calculate Gouraud vectors. */
    vec3 to_light_smooth = normalize(light_pos - frag_pos);
    vec3 to_camera_smooth = normalize(camera_pos - frag_pos);
    vec3 reflection_smooth = reflect(-to_light_smooth, frag_avgnorm);
    float atten_smooth = calc_atten ? (atten_scale / (1.0 + pow(distance(light_pos, frag_pos), 2.0))) : 1.0;

    /* Calculate Gouraud color. */
    frag_color_smooth = atten_smooth *
        (mat_amb * light_amb +
        mat_diff * light_diff * max(0, dot(frag_avgnorm, to_light_smooth)) +
        mat_spec * light_spec * pow(max(0, dot(reflection_smooth, to_camera_smooth)), mat_shin));

    /* Calculate GL position. */
    gl_Position = proj * view * vec4(frag_pos, 1.0);

}
