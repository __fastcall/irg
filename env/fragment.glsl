#version 330 core

/* Shader inputs. */
in vec3 frag_pos;
in vec3 frag_norm;
in vec3 frag_avgpos;
in vec3 frag_avgnorm;
in vec4 frag_color_flat;
in vec4 frag_color_smooth;

/* Uniform variables. */
uniform int shading_mode;
uniform bool calc_atten;
uniform float atten_scale;
uniform vec3 camera_pos;

/* Light uniforms. */
uniform vec3 light_pos;
uniform vec4 light_amb;
uniform vec4 light_diff;
uniform vec4 light_spec;

/* Material uniforms. */
uniform vec4 mat_amb;
uniform vec4 mat_diff;
uniform vec4 mat_spec;
uniform float mat_shin;

/* Fragment output color. */
layout(location = 0) out vec4 color;

/* Main function. */
void main() {

    /* Calculate Phong vectors. */
    vec3 norm_frag_avgnorm = normalize(frag_avgnorm);
    vec3 to_light_phong = normalize(light_pos - frag_pos);
    vec3 to_camera_phong = normalize(camera_pos - frag_pos);
    vec3 reflection_phong = reflect(-to_light_phong, norm_frag_avgnorm);
    float atten_phong = calc_atten ? (atten_scale / (1.0 + pow(distance(light_pos, frag_pos), 2.0))) : 1.0;

    /* Calculate Phong color. */
    vec4 frag_color_phong = atten_phong *
        (mat_amb * light_amb +
        mat_diff * light_diff * max(0, dot(norm_frag_avgnorm, to_light_phong)) +
        mat_spec * light_spec * pow(max(0, dot(reflection_phong, to_camera_phong)), mat_shin));

    /* Switch on shading mode. */
    switch (shading_mode) {

    /* Constant shading. */
    case 0:
        color = frag_color_flat;
        break;

    /* Gouraud shading. */
    case 1:
        color = frag_color_smooth;
        break;

    /* Phong shading. */
    case 2:
        color = frag_color_phong;
        break;

    }
    
}
