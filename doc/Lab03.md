# Lab 3

## Odgovori na pitanja

### *Zasto Bresenhamov algoritam iscrtava liniju korektno samo ako je njen nagib izmedju 0 i 45 stupnjeva?*

Iz razloga jer iterira po x osi (dakle svakim korakom uveca x vrijednost tocke za 1), dok y vrijednost uvecava za 1 svaki `(xe - xs) / (ye - ys)` put. Ako je nagib neka vrijednost manja od 1, onda je vidljivo da ovo ne radi.

### *Nacrtaj liniju (0, 0) -> (4, 12) koristeci Bresenhamov algoritam i objasni zasto izgleda tako.*

![Bresenham pogreska](Lab03_BresenhamError.png)

Razlog zasto se tako crta je taj da se crtanje odvija iterirajuci po x osi i za svaki piksel na x osi moze se nacrtati tocno jedan piksel na y-osi, dok za ovaj slucaj bi trebalo vise.

### *Nacrtaj istu liniju ako se zamijeni dio koji uvecava y-os za 1 sa kodom koji ju uvecava za izracunati tangens i objasni.*

![Bresenham pogreska](Lab03_BresenhamError2.png)

Zamjenili smo jednu pogresku sa drugom, dakle opet je pogreska u tome sto se crta jedan piksel y-osi za jedan piksel x-osi, samo sto se sad uvecava za vise od jedan.

### *Nacrtaj neku liniju sa nagibom -30 stupnjeva i liniju ciji je tangens -3, te ih objasni.*

Na primjer mozemo nacrtati liniju (0, 0) -> (12, -7), ciji nagib iznosi otprilike -30 stupnjeva.

![Bresenham pogreska](Lab03_BresenhamError3.png)

Algoritam nije izradjen da prihvaca negativne nagibe stoga dobijemo ravnu liniju. Isti problem ce se dogoditi tj. ista slika ce se iscrtati ako nacrtamo pravac s tangensom -3 primjerice (0, 0) -> (12, -36).

### *Objasni Bresenhamov algoritam sa cijelim brojevima.*

On je gotovo pa identičan algoritmu sa floating-point brojevima, no koristi preslikane cijele brojeve umjesto floating-point brojeva za brojanje "pomaka" po y osi. Prednost je sto je zbrajanje takvih brojeva brze od zbrajanja floating-point brojeva.

### *Objasnite na koji se nacin Bresenhamov algoritam s decimalnim brojevima prevodi na cjelobrojnu varijantu.*

Na nacin na koji je jedna jedinica floating-point pomaka po y osi poistovjecena sa `2 * (xe - xs)` cjelobrojnim zapisom.

### *Objasnite kako radi algoritam za odsijecanje linije.*

Dakle ako zelimo odsijeci liniju da bude vidljiv i iscrtan samo dio koji je unutar podpravokutnika koji se nalazi u pravokutniku, prvo pravokutnik dijelimo na devet dijelova koristeci 4 linije od koje su 2 paralelne sa x-osi i 2 paralelne sa y-osi. Dobivenim dijelovima dodijelimo 4-bitne vrijednosti na takav nacin da provodjenjem bitovnog AND-a izmedju 4 bitnih vrijednosti pocetne i krajnje tocke, ako dobijemo rezultat 0000 onda je linija vidljiva, a inace nije. Kad je vidljiva onda se pomocu sjecista linije i granicnih linija odrede prvi vidljivi pikseli linije i onda se provodi crtanje.
