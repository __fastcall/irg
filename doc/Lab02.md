# Lab 2

## Odgovori na pitanja

### *Cemu sluzi naredba `glutInit`?*

Naredba sluzi za inicijalizaciju GLUT biblioteke na temelju parametara dobivenih putem komandne linije.

### *Cemu sluzi naredba `glutInitDisplayMode`?*

Naredba sluzi za postavljanje zastavica inicijalnog nacina prikazivanja.

### *Cemu sluzi naredba `glutInitWindowSize`?*

Naredba sluzi za postavljanje inicijalne velicine prozora.

### *Cemu sluzi naredba `glutInitWindowPosition`?*

Naredba sluzi za postavljanje inicijalne pozicije prozora.

### *Cemu sluzi naredba `glutCreateWindow`?*

Naredba sluzi za izradu novog prozora.

### *Cemu sluzi naredba `glutDisplayFunc`?*

Naredba sluzi za postavljanje callback funkcije za prikazivanje.

### *Cemu sluzi naredba `glutReshapeFunc`?*

Naredba sluzi za postavljanje callback funckije za promjenu oblika (tj. veličine) prozora.

### *Cemu sluzi naredba `glutMainLoop`?*

Naredba sluzi za zapocimanje main loopa GLUT-a.

### *Koja je struktura metode `display`, odnosno sto se u njoj radi?*

Postavlja boju ciscenja na crveno, te njome pociscava obojani dio prikaza. Ucitava matricu identiteta, poziva funkciju za crtanje scene te zamijenjuje prednji i straznji buffer.

### *Koja je struktura metode `reshape`, odnosno sto se u njoj radi?*

Prima parametre novih dimenzija prozora. Onemogucuje depth testiranje, u OpenGL-u resetira velicinu viewporta na novopromijenjenu, ucitava ortografsko projekcijsku matricu kao projekcijsku matricu i vraca vrstu primljene matrice natrag na model-view matricu (matricu transformacije pogleda i modela vec pomnozenu).

### *Kako se u OpenGL-u crtaju tocke, odnosno koji se idiom (slijed naredbi) za to koristi?*

Crtanje se zapocinje sa `glBegin` naredbom i parametrom `GL_POINTS`, a zavrsava sa `glEnd` naredbom. Izmedju njih se salju 2D koordinate tocke na ekranu. Prije svega toga, boja im je bila postavljena na cijan.

### *Kako se u OpenGL-u crta obrub trokuta, odnosno koji se idiom (slijed naredbi) za to koristi?*

Crtanje se zapocinje sa `glBegin` naredbom i parametrom `GL_LINE_STRIP`, a zavrsava sa `glEnd` naredbom. Time se crtaju linije od prethodno zadane tocke na ekranu do trenutno zadane tocke na ekranu.

### *GLUT korisnicima omogucava pisanje programa koji reagira na razlicite dogadjaje. Koji su dogadaji podrzani?*

- `glutDisplayFunc` - dogadjaj prikazivanja
- `glutOverlayDisplayFunc` - dogadjaj prikazivanja obruba prozora
- `glutReshapeFunc` - dogadjaj promjene velicine prozora
- `glutKeyboardFunc` - dogadjaj pritiska tipke na tipkovnici
- `glutMouseFunc` - dogadjaj pritiska tipke na misu
- `glutMotionFunc` - kretanje misa po ekranu dok je pritisnuta tipka misa
- `glutPassiveMotionFunc` - kretanje misa po ekranu dok nije pritisnuta tipka misa
- `glutVisibilityFunc` - promjenjena vidljivost ekrana
- `glutEntryFunc` - dogadjaj ulaza/izlaza misa u prozor
- `glutSpecialFunc` - dogadjaj pritiska specijalne tipke
- `glutSpaceballMotionFunc` - kretanje *spaceball* misa
- `glutSpaceballRotateFunc` - okretaj *spaceball* misa
- `glutSpaceballButtonFunc` - pritisak na tipku *spaceball* misa
- `glutButtonBoxFunc`, `glutDialsFunc` - dial & button box callback funkcije
- `glutTabletMotionFunc` - pisanje po grafickom tabletu
- `glutTabletButtonFunc` - pritisak tipke grafickog tableta
- `glutMenuStatusFunc` - stanje globalnog menija
- `glutIdleFunc` - idle stanje programa
- `glutTimerFunc` - registriranje timera

### *Na koji se nacin moze specificirati dio koda koji je potrebno izvrsiti kada korisnik pritisne tipku misa?*

Registriranjem callback funkcije za taj dogadjaj koristeci `glutMouseFunc`.

### *Na koji se nacin moze specificirati dio koda koji je potrebno izvrsiti kada korisnik pritisne tipku na tipkovnici?*

Registriranjem callback funkcije za taj dogadjaj koristeci `glutKeyboardFunc`.
