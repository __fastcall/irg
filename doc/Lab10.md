# Lab 10

## Odgovori na pitanja

### *Sto je Mandelbrotov skup? Mozete li ga pokazati na slici 11.1a odnosno na slici 11.1b?*

Mandelbrotov skup jest skup kompleksnih brojeva za koje funkcija `f(z) = z^2 + c` ne divergira kad se iterira od `z = 0`.

### *Sto je Mandelbrotov fraktal? Mozete li ga pokazati na slici 11.1a odnosno na slici 11.1b? U kakvoj su vezi Mandelbrotov skup i Mandelbrotov fraktal?*

Mandelbrotov fraktal je vizualizacija Mandelbrotovog skupa.

### *Kako je matematicki definiran Mandelbrotov skup? Napisite izraz i sve potrebne uvjete.*

Definiran je izrazom `f(k + 1) = f(k)^2 + c`, `f(0) = 0`, a `c` je kompleksni broj. Uvjet jest da red `|f(k)|` ne divergira.

### *Kod Mandelbrotovog skupa spominje se pojam konvergencije i divergencije. Objasnite*

Pojam konvergencije znaci da se red priblizava nekoj konacnoj vrijednosti, dok divergencija znaci da se red raste do beskonacnosti.

### *Je li moguce napisati racunalni program koji ce zavrsiti u konacnom vremenu i koji ce nacrtati tocnu aproksimaciju Mandelbrotovog skupa? Objasnite.*

Nije moguce zbog vise razloga. U ispitivanju divergencije bi gornja granica trebala biti postavljena na beskonacno, stoga to se nebi moglo izvesti u konacnom vremenu. Takodjer, realni (ili kompleksni) broj se ne moze zapisati u konacnom (diskretnom) sustavu bez zaokruzivanja vrijednosti. Vizualizacija fraktala je takodjer beskonacno velika tako da ima beskonacno mnogo vrijednosti za izracunati.

### *Na koji se nacin na racunalu prikazuje Mandelbrotov skup? U kakvoj su vezi pri tome "ekran racunala" i kompleksna ravnina?*

Prikazuju ga pomocu RGB komponenti boja na ravnini gdje je x-os pridjeljena realnoj osi, a y-os pridjeljena imaginarnoj.

### *Proucite kako je u knjizi rijeseno bojanje Mandelbrotovog skupa - sto predstavljaju podrucja koja su obojana "sareno"? Sto predstavlja pojedina boja?*

Crno su obojane vrijednosti za koje red konvergira. Sareno su obojane vrijednosti za koje red divergira, od kojih svaka od komponenti RGB boja ovisi o gornjoj granici i iteraciji u kojoj vrijednost preskoci 2.

### *Algoritam za ispitivanje divergencije radi s unaprijed zadanom ogradom na maksimalni broj pokusaja. Sto se postize povecavanjem te ograde odnosno kako se to odrazava na dobiveni prikaz (posebice kod bojanja)?*

Povecavanjem gornje granice se dobiva preciznija slika, tj. bogatije boje.

### *Na koji se nacin zadaju IFS-fraktali?*

Zadaju se pomocu tablice vrijednosti matrice afine transformacije i vjerojatnosti primjene te transformacije.

### *Kako tumacimo sadrzaj tablice s funkcijama?*

Prvih 4 vrijednosti su vrijednosti matrice afine transformacije, sljedece 2 su odmak, a zadnja jest vjerojatnost primjene te transformacije.

### *Sto mora vrijediti za vjerojatnosti koje se definiraju uz svaku funkciju?*

Mora vrijediti da je suma svih vjerojatnosti jednaka 1.

### *Kakvog je oblika svaka funkcija koja je zadana u tablici? Je li to skalarna funkcija?*

```
 _        _     _         _     _      _     _   _
|  x(i+1)  |   |   a   b   |   |  x(i)  |   |  e  |
|_ y(i+1) _| = |_  c   d  _| × |_ y(i) _| + |_ f _|

x(i+1) = a*x(i) + b*y(i) + e
y(i+1) = c*x(i) + d*y(i) + f

```

### *Sto je to L-sustav?*

L-sustav je formalni jezik koji se temelji na sustavima s prepisivanjem.

### *Sto je DOL-sustav?*

DOL-sustav je deterministicki kontekstno-neovisan podskup L-sustava.

### *Kako je definiran DOL-sustav?*

Kao uredjena trojka `G = (V, w, P)` gdje je `V` skup znakova alfabeta, `w` je pocetni niz (aksiom) a `P` je skup produkcijskih pravila.

### *Uz L-sustave vezan je i pojam turtle graphics. O cemu se tu radi? Pojasnite.*

Turtle graphics jest pojam koji se odnosi na nacin generiranja grafike. Zamislimo kornjacu koju smo postavili na povrsinu na kojoj crtamo. Svaki generirani znak kornjaca tumaci kao naredbu koju mora izvesti. Izvodjenjem naredbe se ostavlja trag, a kad se izvedu navedene naredbe tragovi ce biti dobivena grafika.

### *Na koji se nacin prikazuje DOL-sustav? Sto treba biti zadano osim samog sustava?*

Trebaju biti definirane naredbe kornjace (rotacija tj. pomak), te dubina crtanja sustava.

### *Sto je potrebno ugraditi u sustav za prikazivanje L-sustava koji moze crtati fraktale s grananjima?*

Potreban nam je stog na kojem cemo pamtiti prethodna kornjacina stanja.
