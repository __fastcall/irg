# Lab 9

## Odgovori na pitanja

### *Objasnite Phongov model osvjetljavanja - koje komponente uzima u obzir te kako se one racunaju.*

Ulazne vrijednosti su za svaki izvor svjetlosti njegova pozicija u prostoru, te intenziteti ambijentalnog, difuznog i spekularnog faktora za RGBA vrijednosti. Za svaki individualni objekt, poligon ili fragment trebamo odrediti njegove ambijentalne, difuzne i spekularne vrijednosti boja RGBA, te takoder potenciju spekularnog utjecaja.

Za svaki vrh poligona uzimamo prosjecnu normalu dodirnih poligona (normaliziranu prije i poslije izracuna). Za normalu bilo kojeg fragmenta poligona uzimamo interpolaciju tih izracunatih normala od vrhova tog poligona.

Jednadzba za izvor 0 se moze prikazati na sljedeci nacin (bez atenuacije svjetlosti):

```
to_light = normalize(light[0].pos - frag_pos);
to_camera = normalize(camera - frag_pos);
reflection = reflect(-to_light, normalize(frag_norm));
color_out =
    color_amb * light[0].int_amb +
    color_diff * light[0].int_diff * max(0, dot(normalize(frag_norm), to_light)) +
    color_spec * light[0].int_spec * pow(max(0, dot(reflection, to_camera)), shininess);
```

### *Objasnite kako se provodi konstantno sjencanje. Gdje se racuna intenzitet. Mozete li se domisliti zasto bas tamo?*

Izracunamo srediste poligona, a za normalu njega uzimamo normalu stvarnog poligona. Za tu tocku trebamo odrediti ambijentalne, difuzne i spekularne vrijednosti boja RGBA, te potenciju spekularnog utjecaja. To u kombinaciji sa intenzitetom ambijentalnog, difuznog i spekularnog faktora izvora svijetlosti, te njegove pozicije koristimo za izracun finalne boje tog fragmenta, a upravo tu boju koristimo da obojamo citav poligon.

### *Kako u OpenGL-u koristeci ugradjeni mehanizam osvjetljavanja postici efekt konstantnog sjencanja? Sto je potrebno konfigurirati?*

U klasicnom OpenGL pipeline-u moramo najprije omoguciti `GL_LIGHTNING`. Tada trebamo omoguciti barem jedno od izvora svijetlosti, primjerice `GL_LIGHT0`. Sljedece postavimo vrijednosti `GL_POSITION`, `GL_AMBIENT`, `GL_DIFFUSE` i `GL_SPECULAR` za taj izvor, te za materijal poligona `GL_AMBIENT`, `GL_DIFFUSE`, `GL_SPECULAR` i `GL_SHININESS`. Nadalje za shade model postavimo `GL_FLAT`, no on nece koristiti srediste tog poligona za izracun boje nego pak neki od vrhova.

### *Objasnite kako se provodi Gouraudovo sjencanje. Gdje se racunaju intenziteti? Kako se utvrdjuju intenziteti za svaku tocku povrsine poligona?*

Za svaki od vrhova poligona se izracuna srediste normala susjednih poligona tog vrha. Za svaki od vrhova trebamo odrediti ambijentalne, difuzne i spekularne vrijednosti boja RGBA, te potenciju spekularnog utjecaja. To u kombinaciji sa intenzitetom ambijentalnog, difuznog i spekularnog faktora izvora svijetlosti, te njegove pozicije koristimo za izracun finalne boje tog fragmenta vrha. Za boju svakog fragmenta poligona interpoliramo boje fragmenata vrhova tog poligona.

### *Kako u OpenGL-u koristeci ugradjeni mehanizam osvjetljavanja postici efekt Gouraudovog sjencanja? Sto je potrebno konfigurirati?*

U klasicnom OpenGL pipeline-u moramo najprije omoguciti `GL_LIGHTNING`. Tada trebamo omoguciti barem jedno od izvora svijetlosti, primjerice `GL_LIGHT0`. Sljedece postavimo vrijednosti `GL_POSITION`, `GL_AMBIENT`, `GL_DIFFUSE` i `GL_SPECULAR` za taj izvor, te za materijal poligona `GL_AMBIENT`, `GL_DIFFUSE`, `GL_SPECULAR` i `GL_SHININESS`. Nadalje za shade model postavimo `GL_SMOOTH`.

### *Objasnite kako se provodi Phongovo sjencanje. Kako se utvrdjuju intenziteti za svaku tocku povrsine poligona?*

Detaljno odgovoreno u odgovoru na prvo pitanje.

### *Kako se u OpenGL-u konfiguriraju izvori svjetlosti a kako svojstva materijala?*

U klasicnom OpenGL pipeline-u moramo najprije omoguciti `GL_LIGHTNING`. Tada trebamo omoguciti barem jedno od izvora svijetlosti, primjerice `GL_LIGHT0`. Sljedece postavimo vrijednosti `GL_POSITION`, `GL_AMBIENT`, `GL_DIFFUSE` i `GL_SPECULAR` za taj izvor, te za materijal poligona `GL_AMBIENT`, `GL_DIFFUSE`, `GL_SPECULAR` i `GL_SHININESS`.

### *Hoce li OpenGL za nas racunati normale?*

Nece, trebamo ih zadati za svaki vrh naredbom `glNormal` prije `glVertex`.

### *Hoce li OpenGL za nas normirati normale?*

Nece osim ako to ne zatrazimo omogucavanjem `GL_NORMALIZE` sto naravno dodatno opterecuje (usporuje) pipeline.

### *Kako se racuna kosinus kuta izmedju dva vektora?*

Normaliziramo oba vektora te ih pomnozimo skalarno. Dobivena vrijednost je jednaka kosinusu kuta izmedju ta dva vektora.

### *Koji se vektori razmatraju prilikom izracuna difuzne komponente?*

Normala fragmenta te vektor od pozicije fragmenta do izvora svijetlosti.

### *Koji se vektori razmatraju prilikom izracuna zrcalne komponente?*

Refleksija vektora od izvora svijetlosti do pozicije fragmenta te vektor od pozicije fragmenta do kamere.

### *Kako se racuna reflektirani vektor?*

```
R = I - 2.0 * dot(N, I) * N
```

`R` je reflektirani vektor, `I` je vektor od izvora svijetlosti do pozicije fragmenta a `N` je normala fragmenta. Vektori trebaju biti normalizirani.

### *Kada je kosinus kuta izmedju dva vektora jednak skalarnom produktu tih vektora?*

Ako su duljine oba vektora jednake 1.0 to jest ako su vektori normalizirani. Iznimno takodjer ako je umnozak duljina vektora jednak 1.0.

### *Kada se za izracun kosinusa kuta izmedju dva vektora ne treba dijeliti s umnoskom normi vektora?*

Ako su duljine oba vektora jednake 1.0 to jest ako su vektori normalizirani. Iznimno takodjer ako je umnozak duljina vektora jednak 1.0.

### *Kako se definiraju prednji a kako straznji poligoni?*

Obicno odaberemo konvenciju zapisa slijeda vrhova u smjeru kazaljke na satu ili u smjeru suprotnom kazaljki na satu za prednje poligone. Preostala konvencija ce biti za straznje poligone. Nakon primjene transformacija ako se poredak zadrzao poligon je prednji inace je straznji.

### *Na koji se nacin moze postici odbacivanje straznjih poligona?*

Omogucavanjem `GL_CULL_FACE` i postavljanjem `glCullFace` na zeljenu vrijednost. Pozivom `glFrontFace` postavljamo konvenciju prednjih lica na `GL_CW` ili `GL_CCW` (pocetna).

### *Kako funkcionira z-spremnik?*

Dubinski z-spremnik funkcionira tako da za svaki fragment boje prikznog medjuspremnika takodjer ima i numericku vrijednost koja pamti udaljenost (dubinu) tog fragmenta od kamere. Cesto tu vrijednost zelimo koristiti da crtamo fragment samo ako se nalazi ispred prethodno nacrtanog fragmenta.

### *Mozemo li uporabom z-spremnika postici da se kod zicanog modela scene sakriju straznji poligoni te prednji poligoni koje (teoretski) ne bismo smjeli vidjeti Objasnite.*

Ne mozemo. U zicanom modelu se crtaju samo vanjske linije (outlines) poligona, a ne i unutrasnjost njega. Linije koje su blize ce biti ispred onih koje su dalje, no svejedno dio daljih koji nije prekriven blizima ce biti vidljiv.
