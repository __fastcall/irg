# Lab 4

## Odgovori na pitanja

### *Koja je razlika izmedju konveksnog i konkavnog poligona?*

U konveksnom poligonu svaka linija povucena izmedju bilo koje dvije tocke koje se nalaze unutar tog poligona ce biti sadrzana u potpunosti unutar njega, dok za konkavni poligon to nije slucaj.

### *Sto znaci da je redosljed vrhova u smjeru kazaljke na satu?*

To znaci da je redosljed njihovog navodjenja u memoriji (tj. slanja u graficku karticu) ekvivalentan redosljedu kazaljke na satu.

### *Kako se za zadani poligon moze ispitati je li mu redosljed vrhova u smjeru kazaljke na satu?*

Tako da se provjeri jeli za svaki vrh vrijedi da se tocka indeksa `i + 2 (mod n)` nalazi ispod pravca koji prolazi tockama `i + 1 (mod n)` i `i (mod n)` gdje je `n` broj vrhova. Dakle, `T * B <= 0` mora vrijediti gdje je `T` 1x3 matricni zapis te tocke, a `B` je 3x1 matricni zapis koeficijenata jednadzbe pravca brida (dobivena `cross(A, B)` gdje su `A` i `B` tocke kojima prolazi taj pravac prosirene homogenom koordinatom).

### *Kako se za zadani poligon moze ispitati je li mu redosljed vrhova u smjeru suprotnom od smjera kazaljke na satu?*

Isto kao i prethodno, osim sto treba vrijediti `T * B >= 0` (dakle da se tocka nalazi iznad pravca).

### *Neka je poligon zadan tako da su mu vrhovi zadani u smjeru kazaljke na satu.*

*Kako mozemo ispitati je li tocka T unutar poligona?*

- Moramo provjeriti da vrijedi da je tocka T ispod svakog bridnog pravca konkavnog poligona. Uvjet koji mora vrijediti je `T * B <= 0` za svaku jednadzbu brida `B`.

*Kako mozemo ispitati je li tocka T izvan poligona?*

- Jednostavno tako da ispitamo da nije tocka T unutar poligona.

*Kako mozemo ispitati je li tocka T na nekom bridu poligona?*

- Istim uvjetom kojim provjeravamo jeli je unutar poligona, ali za jednu ili dvije jednadzbe brida ce vrijediti `T * B = 0`.

### *Neka je poligon zadan tako da su mu vrhovi zadani u smjeru suprotnom od smjera kazaljke na satu.*

*Kako mozemo ispitati je li tocka T unutar poligona?*

- Moramo provjeriti da vrijedi da je tocka T iznad svakog bridnog pravca konkavnog poligona. Uvjet koji mora vrijediti je `T * B >= 0` za svaku jednadzbu brida `B`.

*Kako mozemo ispitati je li tocka T izvan poligona?*

- Jednostavno tako da ispitamo da nije tocka T unutar poligona.

*Kako mozemo ispitati je li tocka T na nekom bridu poligona?*

- Istim uvjetom kojim provjeravamo jeli je unutar poligona, ali za jednu ili dvije jednadzbe brida ce vrijediti `T * B = 0`.

### *Neka je poligon zadan tako da su mu vrhovi zadani na nepoznat nacin (ne znamo jesu li u smjeru kazaljke na satu ili u smjeru suprotnom od smjera kazaljke na satu). Prilikom odgovaranja na sljedeca podpitanja ne smijete raditi eksplicitnu provjeru nacina na koji su bridovi zadani.*

*Kako mozemo ispitati je li tocka T unutar poligona?*

- Moramo provjeriti da vrijedi da se tocka T konzistentno nalazi ispod ili iznad svakog bridnog pravca konkavnog poligona. Za svaku jednadzbu brida `B` uvjet `T * B < 0` (ili `T * B > 0`, ovisi sto nam je draze) mora vrijediti ili ne vrijediti, ali ako slucajno vrijedi `T * B = 0` (tocka je na tome bridu), onda se tocka nalazi na tome pravcu pa taj uvjet ignoriramo iz ukupnog seta uvjeta.

*Kako mozemo ispitati je li tocka T izvan poligona?*

- Jednostavno tako da ispitamo da nije tocka T unutar poligona.

*Kako mozemo ispitati je li tocka T na nekom bridu poligona?*

- Istim uvjetom kojim provjeravamo jeli je unutar poligona, ali za jednu ili dvije jednadzbe brida ce vrijediti `T * B = 0`.

### *Koliko ce se presjecista racunati s pravcem `y = yp`?*

Izracunat ce se po 8 presjecista po svakoj scanliniji (dakle 5 po lijevoj i 3 po desnoj strani).

### *Koliko poligon ima lijevih bridova? Kako se definiraju lijevi bridovi?*

Poligon ima 5 lijevih bridova. Lijevi bridovi su bridovi kojima je komponenta y rastuca (dakle ako uzimamo u obzir matematicki koordinatni sustav te definiranje u smjeru kazaljke na satu).

### *Koliko poligon ima desnih bridova? Kako se definiraju desnih bridovi?*

Poligon ima 3 desna brida. Desni bridovi su bridovi kojima je komponenta y padajuca (dakle ako uzimamo u obzir matematicki koordinatni sustav te definiranje u smjeru kazaljke na satu).

### *Koliko ce se puta azurirati tocka L a koliko puta tocka D nakon sto se postave na svoje inicijalne vrijednosti? Koje su uopce njihove inicijalne vrijednosti?*

Azurirat ce se `ymax - ymin + 1` puta nakon postavljanja na inicijalne vrijednosti. Inicijalne vrijednosti su `xmin = xmax = v0.x` i `ymin = ymax = v0.y`.

### *Pretpostavite da je poligon zadan tako da mu je redosljed vrhova suprotan od smjera kazaljke na satu. Kako bi se tada modificirao postupak bojanja?*

U tom slucaju se lijeve i desne strane moraju redefinirati kao prethodna definicija desne i lijeve respektabilno. Ako ne zelimo mijenjati dio koda koji izracunava pripadnost brida za lijevu i desnu stranu, onda ovdje samo trebamo negirati dio koji ispituje jeli je poligon desni ili lijevi pri postavljanju `L` i `D` vrijednosti.

## Odgovori na dodatna pitanja

### *Isprobajte na primjeru kako ce algoritam za popunjavanje poligona napraviti popunjavanje ako je zadani poligon konkavan. Objasnite rezultat. Znate li sada rukom na papiru objasniti za proizvoljni konkavan poligon kako ce izgledati rezultat popunjavanja ovim algoritmom?*

Nece izgledati tocno. Problem je sto vise ne vrijedi cinjenica da najmanja vrijednost lijevih bridova, to jest najveca vrijednost desnih bridova, u nekoj tocki y jest zapravo granica tog poligona za crtanje. Na papiru se lako moze replicirati pogreska u popunjavanju tako da se od svih bridova naprave pravci (skroz se produze). Tada se vidi da ce neki produzeni pravci presijecati objekt (u konveksnom tijelu to nije slucaj).

### *Isprobajte na primjeru kako ce algoritam koji ste trebali implementirati za ispitivanje odnosa tocke i poligona klasificirati tocke ako mu se zada konkavan poligon? Hoce li taj algoritam bas za sve raditi krivo? Objasnite.*

Radi dobro za neke od tocaka ali ne i sve. Razlog je sto uvjet da je ta tocka iznad ili ispred svih bridova nezadovoljena u svakoj tocki koju bi inace smatrali da mu pripada.

### *Razmislite biste li problem utvrdjivanja odnosa tocke i konkavnog poligona rijesiti ispucavanjem polupravca iz zadane tocke (u bilo kojem smjeru, a mozda je najjednostavnije vodoravno u desno) te brojanjem sjecista s bridovima poligona na koje se naidje u tom smjeru? Skicirajte pseudokod takvog algoritma.*

Mislim da bi bilo moguce. Ispucavanjem linije udesno i brojanjem udara za svaki brid (ne nuzno pravac). Ako je dobiveni broj neparan, nalazi se unutar poligona, a ako je paran, nalazi se izvan poligona. Jedino sto onda treba osigurati jest da kad jedna od sjecisnica ako slucajno prolazi kroz vrh, da pobroji to kao da je prosla kroz dva brida ili jednostavno ignorira. Takodjer ako je ispucana linija paralelna sa nekim od brida kojeg sijece, jednostavno ga treba zanemariti u potpunosti.
