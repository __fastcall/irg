# Lab 7

## Odgovori na pitanja

### *Prisjetite se svih pitanja iz vjezbi 5 i 6.*

Dokumenti za [lab 5](@ref md_Lab05) i [lab 6](@ref md_Lab06).

### *Kako se definiraju prednji i straznji poligoni?*

Prednji poligoni su poligoni cija povrsinska normala gleda prema kameri (dakle, predstavljaju vanjski vidljivi dio 3D modela). Straznji poligoni su poligoni cija povrsinska normala gleda od kamere (dakle, predstavljaju unutarnji nevidljivi dio 3D modela). Vazno je napomenuti da svaki poligon ima prednju i straznju stranu, te njegova pripadnost prednjoj ili straznjoj grupi poligona se moze odrediti samo u nekom trenutku ovisnosti o poligonovoj trenutnoj transformaciji (te transformaciji pogleda, projekciji i sl.).

### *Jesu li prednji poligoni uvijek vidljivi? O cemu to ovisi? Objasnite.*

Nisu uvijek vidljivi. Ako je **ukljuceno** testiranje dubine (`GL_DEPTH_TEST`), onda ce poligon biti vidljiv samo ako svi poligoni koji su blizi kameri od njega (tj. blizi zNear ravnini) ga ne prekrivaju u potpunosti. Ako je **iskljuceno** testiranje dubine, onda ce poligon biti vidljiv samo ako svi poligoni koji su nacrtani nakon njega (do naredbe prikaza spremnika) ga ne prekrivaju u potpunosti.

### *Jesu li straznji poligoni uvijek skriveni? O cemu to ovisi? Objasnite.*

Nisu uvijek skriveni. Pod uvjetom da je **ukljuceno** odbacivanje straznjih poligona (`GL_CULL_FACE`) onda ce se odbaciti poligon ako se nakon primjena svih transformacija zakljuci (uobicajeno smjerom definiranja vrhova) da je poligon straznji, tj. naredba crtanja se nece izvesti za taj poligon. Ako je **iskljuceno** odbacivanje straznjih poligona, onda se crta bez obzira jeli straznji poligon ili ne.

### *Jesu li "prednji poligon" i "vidljivi poligon" te "straznji poligon" i "nevidljivi poligon" sinonimi? Objasnite.*

Na temelju prethodna dva odgovra mozemo zakljuciti da nisu sinonimi.

### *Objasnite kako radi algoritam 1.*

Prvo postavimo uvjet da svi poligoni budu trokuti ili da im svi vrhovi leze na istoj ravnini tako da normala bude ista po cijeloj plohi poligona. Sljedece uzimamo slijedna tri vrha poligona (`v1`, `v2`, `v3`) i poziciju kamere (`c`). Izracunajmo normalu `n` poligona: ako je smjer definiranja vrhova suprotan kazaljki na satu, onda uzimamo `n = (v2 - v1) × (v3 - v1)` inace ako je smjer definiranja vrhova isti kao i kazaljka na satu onda uzimamo `n = (v3 - v1) × (v2 - v1)`. Sada prikazimo ravninu u obliku `Ax + By + Cz + D = 0`, te uvrstimo `A = n.x`, `B = n.y` i `C = n.z`. Jos trebamo izracunati koeficijent `D` preko `D = -A * v1.x - B * v1.y - C * v1.z`. Sada uzmimo formulu `f(v) = A * v.x + B * v.y + C * v.z + D`. Ako vrijedi `f(c) > 0` onda normala plohe gleda prema kameri i poligon je prednji. Ako vrijedi `f(c) < 0` onda normala plohe gleda od kamere i poligon je straznji. Ako je `f(c) = 0`, onda kamera lezi na plohi.

### *Sto bi trebalo promijeniti u algoritmu 1 ako bi vrijedile sve prethodno ustanovljene konvencije osim sto bi poligoni bili zadani tako da pripadne normale ravnina gledaju u unutrasnjost tijela?*

Nacin izracuna normale. Normalu moramo flippati, ako je `np` normala iz prethodnog slucaja, za trenutni slucaj normala `nt` mora biti `nt = -np`.

### *Objasnite kako radi algoritam 2.*

Prvo postavimo uvjet da svi poligoni budu trokuti ili da im svi vrhovi leze na istoj ravnini tako da normala bude ista po cijeloj plohi poligona. Sljedece uzimamo slijedna tri vrha poligona (`v1`, `v2`, `v3`) i poziciju kamere (`c`). Izracunajmo normalu `n` poligona: ako je smjer definiranja vrhova suprotan kazaljki na satu, onda uzimamo `n = (v2 - v1) × (v3 - v1)` inace ako je smjer definiranja vrhova isti kao i kazaljka na satu onda uzimamo `n = (v3 - v1) × (v2 - v1)`. Sljedece uzimamo neku tocku `s` sa plohe, primjerice centar poligona. Izracunajmo pravac od te tocke do pozicije kamere `p = c - s`. Izracunajmo kosinus kuta izmedju ta dva pravca `cos(a) = normalized(n) * normalized(p)`. Ako je kut `a` u domeni `[0, 90>` stupnjeva, poligon je prednji. Ako je kut `a` u domeni `<90, 180]` stupnjeva onda je poligon straznji. Ako je kut `a = 90` stupnjeva onda kamera lezi na plohi poligona.

### *Sto bi trebalo promijeniti u algoritmu 2 ako bi vrijedile sve prethodno ustanovljene konvencije osim sto bi poligoni bili zadani tako da pripadne normale ravnina gledaju u unutrasnjost tijela?*

Nacin izracuna normale. Normalu moramo flippati, ako je `np` normala iz prethodnog slucaja, za trenutni slucaj normala `nt` mora biti `nt = -np`.

### *Objasnite kako radi algoritam 3.*

Prvo postavimo uvjet da su svi poligoni definirani konzistentno (ili svi u smjeru suprotnom kazaljki na satu ili u smjeru kazaljke na satu). Sada obavimo sve zeljene transformacije nad vrhovima. Sljedece izracunajmo jeli smjer definiranja vrhova nakon transformacija suprotan smjeru kazaljke na satu ili jednak smjeru kazaljke na satu, te odbacimo onaj smjer koji nam ne odgovara (onaj smjer koji smo odredili da ce definirati straznje poligone). Za izracun smjera definiranja vrhova nakon primjene transformacija koristimo `fi = vi * bi` za svaki homogeno prosiren vrh poligona `vi` gdje je zapis brida `bi = v((i - 2) % n) × v((i - 1) % n)` (gdje je `n` je broj vrhova). Ako vrijedi `fi >= 0` za sve vrhove poligona, poligon je definiran u smjeru suprotnom kazaljki na satu. Ako vrijedi `fi <= 0` za sve vrhove poligona, poligon je definiran u smjeru kazaljke na satu.

### *Sto bi trebalo promijeniti u algoritmu 3 ako bi vrijedile sve prethodno ustanovljene konvencije osim sto bi poligoni bili zadani tako da pripadne normale ravnina gledaju u unutrasnjost tijela?*

Nebi trebalo napraviti izmjenu jer smo odlucili da odredjujemo prednjost i straznjost poligona na temelju unaprijed definirane konvencije definicije redosljeda vrhova poligona, a ne njegovih normala.

### *Kako se racuna skalarni produkt dvaju vektora?*

Ako su vektori `a` i `b`, njihov skalarni produkt iznosi `a * b = a.x * b.x + a.y * b.y + a.z * c.z`.

### *Kako se racuna vektorski produkt dvaju vektora?*

Ako su vektori `a` i `b`, njihov vektorski produkt iznosi `a × b = (a.y * b.z - a.z * b.y, a.z * b.x - a.x * b.z, a.x * b.y - a.y * b.x)`.

### *Kako se racuna kosinus kuta izmedju dvaju vektora?*

Ako su vektori `a` i `b`, kosinus kuta `k` izmedju njih iznosi `cos(k) = normalized(a) * normalized(b)`.

### *Kako se racuna jednadzba ravnine u kojoj se nalazi zadani trokut?*

Ako su vrhovi trokuta `v1`, `v2` i `v3`, izracunajmo normalu `n` trokuta: ako je smjer definiranja vrhova suprotan kazaljki na satu, onda uzimamo `n = (v2 - v1) × (v3 - v1)` inace ako je smjer definiranja vrhova isti kao i kazaljka na satu onda uzimamo `n = (v3 - v1) × (v2 - v1)`. Sljedece, uvrstimo `A = n.x`, `B = n.y` i `C = n.z` u opcenitu jednadzbu ravnine `f = Ax + By + Cz + D`. Koeficijent `D` mozemo izracunati pomocu bilo kojeg vrha trokuta, primjerice `v1`, preko izraza `D = -A * v1.x - B * v1.y - C * v1.z`.
