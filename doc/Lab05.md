# Lab 5

## Odgovori na pitanja

### *Na koji se nacin moze opisati 3D-tijelo?*

Matematicknom jednadzbom koja ovisi o varijablama `x`, `y` i `z`, a moze se aproksimirati vecom kolicinom sitnih poligona (najcesce trokuta jer za bilo koji odabir vrhova navedeni trokut lezi u 3D ravnini) koji su sastavljeni od odredjenog broja vrhova. Moze se takodjer aproksimirati sa jako jako ogromnom kolicinom tocaka (njihovih pozicija u 3D prostoru i bojom), koristeci uredjaje za 3D skeniranje okolisa.

### *Kako 3D-tijela opisujemo u ovoj vjezbi?*

Vrhove zapisujemo u jednom spremniku (njihovu 3D poziciju), te trokute zapisujemo u zasebnom spremniku (sa 3 indeksa koja referiraju na vrh iz prvog spremnika). Tijelo je opisano sa svim tim trokutima.

### *Na koji se nacin temeljem triju tocaka dolazi do jednadzbe ravnine? Je li ta jednadzba do kraja definirana?*

Pozicije svake tocke su zapisane vektorom `Q`, `W` i `E` (i upravo tim redosljedom). Pravce mozemo izracunati `p = W - Q` i `k = E - Q`. Sada te pravce pomnozimo vektorski (cross produkt) `n = p × k` da dobijemo normalu ravnine. Neka normala izgleda ovako `n = (A, B, C)`. Tada ce ta ravnina imati oblik `Ax + By + Cz + D = 0`. Iz toga su nam poznati svi koeficijenti osim `D`, kojeg lako mozemo izracunati tako da u jednadzbu uvrstimo neku od tocaka ravnine (a najlakse ako je ta tocka jedna od vrhova). Napomena da ce smjer normale ovisiti o redusljedu vrhova, dakle ako koristimo smjer kazaljke na satu i napravimo vakav trokut koji lezi na `xy` ravnini, onda ce njegova normala ici u smjeru `-z` osi, inace ako je u smjeru suprotom kazaljke na satu onda ce normala ici u smjeru `+z`.

### *Cime je odredjen smjer normale trokuta?*

Redosljedom definiranja vrhova i upravo koje tocke koristimo da bi izracunali pravce da izracunamo vektorski produkt (i kako mnozimo taj vektorski produkt - prvi puta drugi ili drugi puta prvi).

### *Koje zahtjeve postavljamo na zapis modela 3D-tijela u ovoj vjezbi, odnosno kojih se konvencija pridrzavamo?*

Konvencija za opis kocke jest smjer suprotan kazaljki na satu. To je inace standard za Wavefront OBJ datoteku (ako normale nisu navedene eksplicitno sa `vn` direktivom i spomenute unutar `f`).

### *Na koji se nacin ispituje odnos tocke i 3D-tijela ako je tijelo konveksno?*

Formulu za ravninu oblika `Ax + By + Cz + D = 0` zapisimo kao `f = Ax + By + Cz + D`. Neka je `fi` taj izraz za svaki poligon (trokut) `i` 3D tijela. Ako je smjer definiranja counter-clockwise, treba vrijediti `fi <= 0` za sve te izraze da bi tijelo bilo unutar toga objekta, pri tom da ako je `fi = 0` za neke od izraza, znacit ce da se tocka taman nalazi na toj od ravnina. Za slucaj clockwise vrijediti treba `fi >= 0`.

### *Bi li nacin ispitivanja odnosa tocke i 3D-tijela koji je opisan u ovoj vjezbi radio za konkavna tijela?*

Ne bi funkcionirao, s obzirom da se prejsecanje prosirenih ravnina moze dogoditi unutar prostora koji pripada tom tijelu.

### *Biste li zadatak ispitivanja odnosa tocke i konkavnog 3D-tijela mogli rijesiti ispucavanjem polupravca i utvrdjivanjem sjecista s trokutima? Objasnite.*

Mislim da bi se moglo, brojanjem kolizija sa plohama i ispitivanjem dali taj dio plohe pripada poligonu (trokutu) objekta. Ako je broj kolizija paran, onda se tocka nalazi izvan objekta, a ako je neparan onda se nalazi unutar njega.

### *Kakva je struktura .obj datoteke?*

Datoteka je tekstualni zapis scene. Datoteka se sastoji od vise tipova:

- `#` - komentar
- `v` - pozicija vrha oblika `x y z [w]`
- `vt` - koordinate na teksturi oblika `u [v w]`
- `vn` - smjer normale oblika `x y z`
- `vp` - parametarske prostorne pozicije oblika `u [v w]` (primjerice crtanje linija)
- `f` - zapis lica oblika `v/vt/vn ...`, gdje su ovo indeksi prethodno navedenog zapisa (pocevsi od 1) - `v` je nuzan, ovi ostali se mogu izostaviti, broj ovih oblika je proizvoljan ali obicno je idealno da su 3 (trokuti)
- `l` - poligonska linija oblika `vp ...`

Takodjer postoje ostali parametri koji odredjuju materijalski `mtl` fajl, nazive skupine objekata itd.
