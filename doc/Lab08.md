# Lab 8

## Odgovori na pitanja

### *Cime je odredjena Bezierova krivulja?*

Kontrolnim poligonom (kontrolnim tockama).

### *Sto je to red Bezierove krivulje i o cemu on ovisi?*

Red Bezierove krivulje je broj segmenata kontrolnog poligona, a on ovisi o broju kontrolnih tocaka.

### *Koje dvije vrste tezinskih funkcija postoje koje se mogu koristiti za izracun tocaka Bezierove krivulje?*

Bezierove i Bernsteinove tezinske funkcije.

### *Vezano uz prethodno pitanje, koja je razlika ako se koriste jedne odnosno druge tezinske funkcije (sto se mnozi u prvom a sto u drugom slucaju)? Napisite izraz kojim je odredjena proizvoljna tocka p(t) aproksimacijske Bezierove krivulje.*

Bezierove tezinske funkcije: f(i, n, t) = (1 - t) * f(i, n - 1, t) + t * f(i - 1, n - 1, t), gdje je f(0, 0, t) = 1, f(-1, k, t) = 1 te f(k + 1, k, t) = 0.

Bernsteinove tezinske funkcije: b(i, n, t) = (n! / (i! * (n - i)!)) * t^i * (1 - t)^(n - i).

Proizvoljna točka p(t) = a(0) * f(0, n, t) + ... + a(n) * f(n, n, t) = r(0) * b(0, n, t) + ... + r(n) * b(n, n, t), gdje je a(k) = T(k) - T(k - 1), a r(k) = T(k) - (0, 0) = T(k), a T(k) je kontrolna tocka k.

### *Koja je razlika izmedju aproksimacijskih krivulja i interpolacijskih krivulja?*

Kod aproksimacijskih krivulja sve kontrolne točke ne moraju nužno biti dio krivulje dok kod interpolacijskih krivulja su sve kontrolne točke dio krivulje.

### *Bezierova krivulja spada u porodicu parametarskih krivulja - sto to znaci?*

To znaci da spada u skupinu krivulja koje ovise o jednom (ili vise parametara), u ovom slucaju t.
