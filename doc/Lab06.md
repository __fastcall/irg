# Lab 6

## Odgovori na pitanja

### *Sto je to volumen pogleda?*

Volumen pogleda se odnosi na raspon `[-1, 1]` koji je inicijalno vidljiv na svakoj od `x`, `y` i `z` komponenti.

### *Kako je odredjen inicijalni volumen pogleda kod OpenGL-a?*

Inicijalno je postavljen na `[-1, 1]`, moze se izmijeniti postavljanjem projekcijske matrice.

### *U kakvoj su vezi volumen pogleda i pojam odsijecanja? Sto OpenGL odsijeca? Dajte primjer.*

OpenGL odsijeca sve izvan volumena pogleda. Primjer su vrijednosti koje imaju neku od `x`, `y` ili `z` komponenti izvan intervala `[-1, 1]`. Takodjer se odsijecaju (ako je omoguceno dubinsko testiranje) sve vrijednosti cije su `z` komponente vise od trenutne vrijednosti u dubinskom spremniku.

### *Napisite matricu operatora translacije za `(dx, dy, dz)` uz obje konvencije. Kako glase matrice koje tocku vracaju u pocetnu?*

```
------------------------------------------------------------------

      _                    _
     | 1     0     0     dx |
Rr = | 0     1     0     dy |
     | 0     0     1     dz |
     |_0     0     0     1 _|

V' = Rr * V

------------------------------------------------------------------

                       _                    _
                      | 1     0     0     0  |
Ro = transposed(Rr) = | 0     1     0     0  |
                      | 0     0     1     0  |
                      |_dx    dy    dz    1 _|

V' = V * Ro

------------------------------------------------------------------

                      _                    _
                     | 1     0     0    -dx |
Rri = inversed(Rr) = | 0     1     0    -dy |
                     | 0     0     1    -dz |
                     |_0     0     0     1 _|

V = Rri * V'

------------------------------------------------------------------

                                        _                    _
                                       | 1     0     0     0  |
Roi = inversed(Ro) = transposed(Rri) = | 0     1     0     0  |
                                       | 0     0     1     0  |
                                       |_-dx  -dy   -dz    1 _|

V = V' * Roi

------------------------------------------------------------------
```

### *Napisite matricu operatora skaliranja s faktorima `sx`, `sy` i `sz` uz obje konvencije. Kako glase matrice koje tocku vracaju u pocetnu?*

```
------------------------------------------------------------------

      _                    _
     | sx    0     0     0  |
Sr = | 0     sy    0     0  |
     | 0     0     sz    0  |
     |_0     0     0     1 _|

V' = Sr * V

------------------------------------------------------------------

           _                    _
          | sx    0     0     0  |
So = Sr = | 0     sy    0     0  |
          | 0     0     sz    0  |
          |_0     0     0     1 _|

V' = V * So

------------------------------------------------------------------

                      _                   _
                     |1/sx   0     0     0 |
Sri = inversed(Sr) = | 0    1/sy   0     0 |
                     | 0     0    1/sz   0 |
                     |_0     0     0     1_|

V = Sri * V'

------------------------------------------------------------------

                            _                   _
                           |1/sx   0     0     0 |
Soi = Sri = inversed(So) = | 0    1/sy   0     0 |
                           | 0     0    1/sz   0 |
                           |_0     0     0     1_|

V = V' * Soi

------------------------------------------------------------------
```

### *Je li transformacija pogleda jednoznacno definirana zadavanjem tocke ocista i gledista? Objasnite.*

Nije jednoznacno definirana. Nedostaje nam jos definicija vektora koji gleda prema gore. Usmjerenje vektora kamere jest odredjeno, no njena rotacija (`roll`) bez danog vektora up (tipicno +y) nije.

### *Sto je to view-up vektor i cemu sluzi? Lezi li on u ravnini projekcije?*

To je vektor koji se koristi uz tocku ocista i gledista za definiranje transformacije pogleda. Tijekom zadavanja ne mora nuzno lezati u ravnini projekcije, sam taj vektor je dovoljan da se uz njega i vektor usmjerenja izracuna ravnina koja odredjuje rotaciju (`roll`) kamere.

### *OpenGL nudi naredbu gluLookAt. Cemu sluzi ta naredba, kakvu transformaciju pogleda radi, koji su njezini argumenti i kako izgleda pripadna matrica, uz obje konvencije (matricu ne trebate uciti napamet)?*

Naredba sluzi za izracun transformacijske matrice pogleda.

```c
gluLookAt(GLdouble eyeX, GLdouble eyeY, GLdouble eyeZ, GLdouble centerX, GLdouble centerY, GLdouble centerZ, GLdouble upX, GLdouble upY, GLdouble upZ);
/*
    eyeX, eyeY, eyeZ - pozicija ocista
    centerX, centerY, centerZ - pozicija gledista
    upX, upY, upZ - view-up vektor
*/
```

```
------------------------------------------------------------------

zaxis = normal(eye - center)
xaxis = normal(cross(up, zaxis))
yaxis = cross(zaxis, xaxis)

dotx = -dot(xaxis, eye)
doty = -dot(yaxis, eye)
dotz = -dot(zaxis, eye)
      _                                         _
     |   xaxis.x    xaxis.y     xaxis.z    dotx  |
Lr = |   yaxis.x    yaxis.y     yaxis.z    doty  |
     |   zaxis.x    zaxis.y     zaxis.z    dotz  |
     |_    0          0           0         1   _|

V' = Lr * V

------------------------------------------------------------------

zaxis = normal(eye - center)
xaxis = normal(cross(up, zaxis))
yaxis = cross(zaxis, xaxis)

dotx = -dot(xaxis, eye)
doty = -dot(yaxis, eye)
dotz = -dot(zaxis, eye)
                       _                                        _
                      |   xaxis.x    yaxis.x     zaxis.x     0   |
Lo = transposed(Lr) = |   xaxis.y    yaxis.y     zaxis.y     0   |
                      |   xaxis.z    yaxis.z     zaxis.z     0   |
                      |_  dotx       doty        dotz        1  _|

V' = V * Lo

------------------------------------------------------------------
```

### *OpenGL nudi naredbu glFrustum. cemu sluzi ta naredba, kako izgleda volumen pogleda koji definira, koji su njezini argumenti i kako izgleda pripadna matrica, uz obje konvencije (matricu ne trebate uciti napamet)?*

Sluzi za izradu perspektivne matrice - tj. mnozenje nje sa trenutnom matricu. Takodjer se moze definirati pozicija vrha, dna, lijevog i desnog kraja.

```c

void glFrustum(GLdouble left, GLdouble right, GLdouble bottom, GLdouble top, GLdouble nearVal, GLdouble farVal);
/*
    left, right, bottom, top - pozicije rubova ekrana
    nearVal, farVal - volumen pogleda z komponente
*/

```

```
------------------------------------------------------------------

Q = 2 * nearVal / (right − left)
W = 2 * nearVal / (top − bottom)
A = (right + left) / (right - left)
B = (top + bottom) / (top - bottom)
C = -(farVal + nearVal) / (farVal - nearVal)
D = -(2 * farVal * nearVal) / (farVal - nearVal)
      _                                  _
     |    Q        0        A        0    |
Pr = |    0        W        B        0    |
     |    0        0        C        D    |
     |_   0        0       -1        0   _|

V' = Pr * V

------------------------------------------------------------------

Q = 2 * nearVal / (right − left)
W = 2 * nearVal / (top − bottom)
A = (right + left) / (right - left)
B = (top + bottom) / (top - bottom)
C = -(farVal + nearVal) / (farVal - nearVal)
D = -(2 * farVal * nearVal) / (farVal - nearVal)
                       _                                  _
                      |    Q        0        0        0    |
Po = transposed(Pr) = |    0        W        0        0    |
                      |    A        B        C       -1    |
                      |_   0        0        D        0   _|

V' = V * Po

------------------------------------------------------------------
```

### *OpenGL nudi naredbu gluPerspective. cemu sluzi ta naredba, kako izgleda volumen pogleda koji definira, koji su njezini argumenti i kako izgleda pripadna matrica, uz obje konvencije (matricu ne trebate uciti napamet)?*

Sluzi za izradu perspektivne matrice.

```c

void gluPerspective(GLdouble fovy, GLdouble aspect, GLdouble zNear, GLdouble zFar);
/*
    fovy - field of view na y osi
    aspect - omjer horizontalne i vertikalne dimenzije prikaznog ekrana
    zNear, zFar - volumen pogleda z komponente
*/

```

```
------------------------------------------------------------------

yScale = ctg(fovy/2)
xScale = yScale / aspect
A = zFar / (zNear - zFar)
B = zNear * zFar / (zNear - zFar)

      _                                    _
     |    xScale     0        0        0    |
Pr = |      0      yScale     0        0    |
     |      0        0        A        B    |
     |_     0        0       -1        0   _|

V' = Pr * V

------------------------------------------------------------------

yScale = ctg(fovy/2)
xScale = yScale / aspect
A = zFar / (zNear - zFar)
B = zNear * zFar / (zNear - zFar)

                       _                                    _
                      |    xScale     0        0        0    |
Po = transposed(Pr) = |      0      yScale     0        0    |
                      |      0        0        A       -1    |
                      |_     0        0        B        0   _|

V' = V * Po

------------------------------------------------------------------
```

### *OpenGL nudi naredbu glViewport. Cemu sluzi ta naredba i koji su njezini argumenti?*

Ta naredba nam sluzi za definiranje konstanti preslikavanja inicijalnog volumena OpenGL-a u volumen ekrana.

```c
void glViewport(GLint x, GLint y, GLsizei width, GLsizei height);
/*
    x, y - ishodiste prikaznog ekrana
    width, height - sirina i visina prikaznog ekrana
*/

```

### *Uporabom OpenGL naredbe glMatrixMode do sada ste odabirali dva moda: GL_PROJECTION i GL_MODEL. Sto se podesava u jednom modu a sto u drugom?*

Mod `GL_PROJECTION` odabire postavljanje projekcijske matrice, a `GL_MODELVIEW` odabire postavljanje premultiplicirane matrice modela i pogleda. Naravno, nije nuzno koristiti matrice za tocno tu ulogu kakvo im je ime. `GL_MODEL` ne postoji.
