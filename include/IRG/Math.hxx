#ifndef __IRG_MATH_HXX__
#define __IRG_MATH_HXX__

#include "Math/Vector.hxx"
#include "Math/Matrix.hxx"
#include "Math/VectorTransformation.hxx"
#include "Math/VectorUtil.hxx"
#include "Math/MatrixUtil.hxx"
#include "Math/VectorComparator.hxx"
#include "Math/MatrixComparator.hxx"

#endif
