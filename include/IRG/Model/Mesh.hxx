#ifndef __IRG_MODEL_MESH_HXX__
#define __IRG_MODEL_MESH_HXX__

#include "PositionState.hxx"
#include "../Math/Vector.hxx"
#include <vector>
#include <memory>

namespace IRG { namespace Model {

    /**
     * @brief Represents a 3D mesh.
     */
    class Mesh {

    /**
     * @brief %Wavefront class can access private member of %Mesh class.
     */
    friend class Wavefront;

    public:

        /**
         * @brief Constructs an empty 3D mesh instance.
         */
        Mesh();

        /**
         * @brief Constructs a 3D mesh instance by copying another mesh.
         * @param mesh %Mesh to copy.
         */
        Mesh(const Mesh& mesh) = default;

        /**
         * @brief Constructs a 3D mesh instance by moving another mesh.
         * @param mesh %Mesh to move.
         */
        Mesh(Mesh&& mesh) = default;

        /**
         * @brief Copies a mesh instance.
         * @param mesh %Mesh to copy.
         * @return Mesh& Resulting mesh.
         */
        Mesh& operator=(const Mesh& mesh) = default;

        /**
         * @brief Moves a mesh instance.
         * @param mesh %Mesh to move.
         * @return Mesh& Resulting mesh.
         */
        Mesh& operator=(Mesh&& mesh) = default;

        /**
         * @brief Centers the vertices.
         */
        void CenterVertices();

        /**
         * @brief Normalizes the vertices and centers them if set to true.
         * @param center Centers the vertices if set to true.
         */
        void NormalizeVertices(bool center = true);

        /**
         * @brief Caclulates minimum and maximum values for all three components of vertices.
         * @param xMinMax Resulting minimum and maximum of x component.
         * @param yMinMax Resulting minimum and maximum of y component.
         * @param zMinMax Resulting minimum and maximum of z component.
         * @return true When the results were written.
         * @return false When the results were not written (if there are no vertices stored in this mesh intance).
         */
        bool CalculateMinMax(Math::Vector2f& xMinMax, Math::Vector2f& yMinMax, Math::Vector2f& zMinMax) const;

        /**
         * @brief Calculates normals for each face. Previous normals will be erased.
         * @note Assumes all face vertices lay on the same 3D plane.
         * @param isCCW Specifies wether face vertices are defined in counter-clockwise order.
         */
        void CalculateNormals(bool isCCW = true);

        /**
         * @brief Tests point position to determine its relation to the 3D model.
         * @note Assumes all face vertices lay on the same 3D plane and that all faces are convex.
         * @param position Point position.
         * @param isCCW Specifies wether face vertices are defined in counter-clockwise order.
         * @return PositionState Relation between the point and a 3D model.
         */
        PositionState TestPosition(const Math::Vector3f& position, bool isCCW = true) const;

        /**
         * @brief Gets the array of vertices.
         * @return const std::vector<Math::Vector3f>& Array of vertices.
         */
        inline const std::vector<Math::Vector3f>& GetVertices() const { return *m_Vertices; }

        /**
         * @brief Gets the array of texture coordinates.
         * @return const std::vector<Math::Vector3f>& Array of texture coordinates.
         */
        inline const std::vector<Math::Vector2f>& GetTexCoords() const { return *m_TexCoords; }

        /**
         * @brief Gets the array of normals.
         * @return const std::vector<Math::Vector3f>& Array of normals.
         */
        inline const std::vector<Math::Vector3f>& GetNormals() const { return *m_Normals; }

        /**
         * @brief Gets the array of faces.
         * @return const std::vector<std::vector<Math::Vector3i>>& Array of faces.
         */
        inline const std::vector<std::vector<Math::Vector3i>>& GetFaces() const { return *m_Faces; }

    private:

        std::unique_ptr<std::vector<Math::Vector3f>> m_Vertices;

        std::unique_ptr<std::vector<Math::Vector2f>> m_TexCoords;

        std::unique_ptr<std::vector<Math::Vector3f>> m_Normals;

        std::unique_ptr<std::vector<std::vector<Math::Vector3i>>> m_Faces;

    };

} }

#endif
