#ifndef __IRG_MODEL_WAVEFRONT_HXX__
#define __IRG_MODEL_WAVEFRONT_HXX__

#include <string>
#include "Mesh.hxx"
#include <sstream>

namespace IRG { namespace Model {

    /**
     * @brief A collection of static methods that can open %Wavefront OBJ model files.
     */
    class Wavefront {

    public:

        /**
         * @brief Loads a %Wavefront OBJ file from path.
         * @note Ignores materials and textures, as well as all parametric curve/line data.
         * @param filename File path.
         * @return Mesh Resulting loaded mesh.
         */
        static Mesh Load(const std::string& filename);

        /**
         * @brief Dumps mesh content to %Wavefront OBJ file stream.
         * @note Ignores materials and textures, as well as all parametric curve/line data.
         * @param mesh Targeted mesh.
         * @param ofs Output file stream.
         */
        static void Dump(const Mesh& mesh, std::ostream& ofs);

    private:

        template<std::size_t N>
        static Math::Vector<N, float> ReadVector(std::istringstream& line);

        static std::vector<Math::Vector3i> ReadFace(std::istringstream& line);

        Wavefront() = delete;

    };

} }

#endif
