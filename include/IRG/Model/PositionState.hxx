#ifndef __IRG_MODEL_POSITIONSTATE_HXX__
#define __IRG_MODEL_POSITIONSTATE_HXX__

namespace IRG { namespace Model {

    /**
     * @brief Relation between a point and a 3D model.
     */
    enum class PositionState {
        
        /**
         * @brief Point is inside the 3D model.
         */
        Inside,

        /**
         * @brief Point is on the edge of the 3D model.
         */
        OnEdge,

        /**
         * @brief Point is outside the 3D model.
         */
        Outside

    };

} }

#endif
