#ifndef __IRG_MATH_MATRIX_HXX__
#define __IRG_MATH_MATRIX_HXX__

#include <cstddef>
#include <type_traits>
#include <algorithm>
#include <ostream>
#include <istream>
#include <sstream>
#include <string>
#include <initializer_list>

namespace IRG { namespace Math {

    /**
     * @brief Represents a mathematical matrix.
     * @note %Matrix must consist of at least two rows and two columns. %Matrix element must be of arithmetic type.
     * @tparam M Number of rows.
     * @tparam N Number of columns.
     * @tparam T Element type.
     */
    template<std::size_t M, std::size_t N, typename T>
    class Matrix {

        static_assert(M >= 2, "Matrix must consist of at least two rows.");
        static_assert(N >= 2, "Matrix must consist of at least two columns.");
        static_assert(std::is_arithmetic<T>::value, "Matrix element must be of arithmetic type.");

    public:

        /**
         * @brief Constructs a new matrix instance.
         * @param value Value of every element.
         */
        inline Matrix(T value = static_cast<T>(0)) {
            std::fill_n(reinterpret_cast<T*>(m_Elements), M * N, value);
        }

        /**
         * @brief Constructs a new matrix instance from an initializer list.
         * @param list Initializer list of element values.
         */
        inline Matrix(const std::initializer_list<T>& list) {
            std::size_t limit = (M * N > list.size()) ? list.size() : M * N;
            std::copy_n(list.begin(), limit, reinterpret_cast<T*>(m_Elements));
            std::fill_n(reinterpret_cast<T*>(m_Elements) + limit, M * N - limit, static_cast<T>(0));
        }

        /**
         * @brief Constructs a new matrix instance by copying another matrix of same element type.
         * @param matrix %Matrix to copy.
         */
        inline Matrix(const Matrix& matrix) {
            std::copy_n(reinterpret_cast<const T*>(matrix.m_Elements), M * N, reinterpret_cast<T*>(m_Elements));
        }

        /**
         * @brief Constructs a new matrix instance by partially copying another matrix of same element type.
         * @details If the desired number of rows or columns exceeds the number of rows or columns in the other matrix, the remaining elements will be set to the provided value.
         * @tparam K Number of rows in the matrix to partially copy.
         * @tparam P Number of columns in the matrix to partially copy.
         * @param matrix %Matrix to partially copy.
         * @param value Value of every remaining element.
         */
        template<std::size_t K, std::size_t P>
        inline Matrix(const Matrix<K, P, T>& matrix, T value = static_cast<T>(0)) {
            constexpr std::size_t limitRows = (M > K) ? K : M;
            constexpr std::size_t limitColumns = (N > P) ? P : N;
            for (std::size_t i = 0; i < limitRows; ++i) {
                std::copy_n(matrix[i], limitColumns, static_cast<T*>(m_Elements[i]));
                std::fill_n(static_cast<T*>(m_Elements[i] + limitColumns), N - limitColumns, value);
            }
            std::fill_n(static_cast<T*>(m_Elements[limitRows]), (M - limitRows) * N, value);
        }

        /**
         * @brief Constructs a new matrix instance from its string representation.
         * @param string %Matrix string representation.
         */
        inline Matrix(const std::string& string) {
            std::istringstream istr(string);
            istr >> *this;
        }

        /**
         * @brief Constructs a new matrix instance from raw data of element values.
         * @note Raw data must be stored in row-major order.
         * @param rawData Raw data of element values.
         */
        inline Matrix(const T* rawData) {
            std::copy_n(rawData, M * N, reinterpret_cast<T*>(m_Elements));
        }

        /**
         * @brief Copies element values from another matrix of same element type.
         * @param matrix %Matrix to copy.
         * @return Matrix& Resulting matrix.
         */
        inline Matrix& operator=(const Matrix& matrix) {
            std::copy_n(reinterpret_cast<const T*>(matrix.m_Elements), M * N, reinterpret_cast<T*>(m_Elements));
            return *this;
        }

        /**
         * @brief Adds matrix of same dimension and element type.
         * @param matrix %Matrix to add.
         * @return Matrix& Resulting matrix.
         */
        inline Matrix& operator+=(const Matrix& matrix) {
            for (std::size_t i = 0; i < M; ++i) {
                for (std::size_t j = 0; j < N; ++j) {
                    m_Elements[i][j] += matrix.m_Elements[i][j];
                }
            }
            return *this;
        }

        /**
         * @brief Subtracts matrix of same dimension and element type.
         * @param matrix %Matrix to subtract.
         * @return Matrix& Resulting matrix.
         */
        inline Matrix& operator-=(const Matrix& matrix) {
            for (std::size_t i = 0; i < M; ++i) {
                for (std::size_t j = 0; j < N; ++j) {
                    m_Elements[i][j] -= matrix.m_Elements[i][j];
                }
            }
            return *this;
        }

        /**
         * @brief Multiplies with a matrix of same dimension and element type.
         * @note %Matrix must be a square matrix.
         * @param matrix %Matrix to multiply.
         * @return Matrix& Resulting matrix.
         */
        inline Matrix& operator*=(const Matrix& matrix) {
            static_assert(M == N, "Matrix must be a square matrix.");
            Matrix self = *this;
            for (std::size_t i = 0; i < M; ++i) {
                for (std::size_t j = 0; j < M; ++j) {
                    m_Elements[i][j] = static_cast<T>(0);
                    for (std::size_t k = 0; k < M; ++k) {
                        m_Elements[i][j] += self.m_Elements[i][k] * matrix.m_Elements[k][j];
                    }
                }
            }
            return *this;
        }

        /**
         * @brief Multiplies with a scalar value.
         * @param scalar Scalar value.
         * @return Matrix& Resulting matrix.
         */
        inline Matrix& operator*=(T scalar) {
            for (std::size_t i = 0; i < M; ++i) {
                for (std::size_t j = 0; j < N; ++j) {
                    m_Elements[i][j] *= scalar;
                }
            }
            return *this;
        }

        /**
         * @brief Divides by a scalar value.
         * @param scalar Scalar value.
         * @return Matrix& Resulting matrix.
         */
        inline Matrix& operator/=(T scalar) {
            for (std::size_t i = 0; i < M; ++i) {
                for (std::size_t j = 0; j < N; ++j) {
                    m_Elements[i][j] /= scalar;
                }
            }
            return *this;
        }

        /**
         * @brief Gets the immutable row element array.
         * @param index Row index.
         * @return const T* Row array.
         */
        inline const T* operator[](std::size_t index) const {
            return static_cast<const T*>(m_Elements[index]);
        }

        /**
         * @brief Gets the mutable row element array.
         * @param index Row index.
         * @return T* Row array.
         */
        inline T* operator[](std::size_t index) {
            return static_cast<T*>(m_Elements[index]);
        }

        /**
         * @brief Converts matrix to string.
         * @return std::string %Matrix string.
         */
        inline operator std::string() const {
            std::ostringstream ostr;
            ostr << *this;
            return ostr.str();
        }

        /**
         * @brief Gets the number of rows.
         * @return constexpr std::size_t Number of rows.
         */
        inline constexpr std::size_t GetNumberOfRows() const {
            return M;
        }

        /**
         * @brief Gets the number of columns.
         * @return constexpr std::size_t Number of columns.
         */
        inline constexpr std::size_t GetNumberOfColumns() const {
            return N;
        }

        /**
         * @brief Computes the matrix determinant.
         * @note %Matrix must be a square matrix.
         * @return T %Matrix determinant.
         */
        inline T GetDeterminant() const {
            static_assert(M == N, "Matrix must be a square matrix.");
            if (M == 2) {
                return m_Elements[0][0] * m_Elements[1][1] - m_Elements[0][1] * m_Elements[1][0];
            } else if (M == 3) {
                return m_Elements[0][0] * m_Elements[1][1] * m_Elements[2][2] + m_Elements[0][1] * m_Elements[1][2] * m_Elements[2][0] + m_Elements[0][2] * m_Elements[1][0] * m_Elements[2][1] - m_Elements[0][2] * m_Elements[1][1] * m_Elements[2][0] - m_Elements[0][1] * m_Elements[1][0] * m_Elements[2][2] - m_Elements[0][0] * m_Elements[1][2] * m_Elements[2][1];
            }
            T result = static_cast<T>(0);
            for (std::size_t i = 0; i < M; ++i) {
                result += ((i % 2 == 0) ? static_cast<T>(1) : static_cast<T>(-1)) * m_Elements[i][0] * GetMinor(i, 0);
            }
            return result;
        }

        /**
         * @brief Computes an inverted matrix copy.
         * @note %Matrix must be a square matrix.
         * @return Matrix Inverted matrix.
         */
        inline Matrix GetInverse() const {
            static_assert(M == N, "Matrix must be a square matrix.");
            Matrix matrix = *this;
            matrix.Invert();
            return matrix;
        }

        /**
         * @brief Inverts a matrix.
         * @note %Matrix must be a square matrix.
         */
        inline void Invert() {
            static_assert(M == N, "Matrix must be a square matrix.");
            if (M == 2) {
                T determinant = GetDeterminant();
                T swap = m_Elements[0][0] / determinant;
                m_Elements[0][0] = m_Elements[1][1] / determinant;
                m_Elements[1][1] = swap;
                m_Elements[0][1] /= -determinant;
                m_Elements[1][0] /= -determinant;
            } else {
                Matrix result;
                T determinant = static_cast<T>(0);
                for (std::size_t i = 0; i < M; ++i) {
                    result.m_Elements[i][0] = GetMinor(i, 0);
                    determinant += ((i % 2 == 0) ? static_cast<T>(1) : static_cast<T>(-1)) * m_Elements[i][0] * result.m_Elements[i][0];
                    for (std::size_t j = 1; j < M; ++j) {
                        result.m_Elements[i][j] = GetMinor(i, j);
                    }
                }
                for (std::size_t i = 0; i < M; ++i) {
                    for (std::size_t j = 0; j < M; ++j) {
                        m_Elements[i][j] = (((i + j) % 2 == 0) ? static_cast<T>(1) : static_cast<T>(-1)) * result.m_Elements[j][i] / determinant;
                    }
                }
            }
        }

        /**
         * @brief Computes a transposed matrix copy.
         * @return Matrix<N, M, T> Transposed matrix.
         */
        inline Matrix<N, M, T> GetTransposed() const {
            Matrix<N, M, T> matrix;
            for (std::size_t i = 0; i < M; ++i) {
                for (std::size_t j = 0; j < N; ++j) {
                    matrix[j][i] = m_Elements[i][j];
                }
            }
            return matrix;
        }
        
        /**
         * @brief Transposes a matrix.
         * @note %Matrix must be a square matrix.
         */
        inline void Transpose() {
            static_assert(M == N, "Matrix must be a square matrix.");
            for (std::size_t i = 0; i < M; ++i) {
                for (std::size_t j = 0; j < i; ++j) {
                    T swap = m_Elements[i][j];
                    m_Elements[i][j] = m_Elements[j][i];
                    m_Elements[j][i] = swap;
                }
            }
        }

        /**
         * @brief Computes matrix minor.
         * @details If row or column indexes exceed the matrix row or column count, they are treated as if they're refering to the largest row or column index respectively.
         * @note %Matrix must be a square matrix.
         * @param rowIndex Row index of the minor.
         * @param columnIndex Column index of the minor.
         * @return T Minor determinant.
         */
        inline T GetMinor(std::size_t rowIndex, std::size_t columnIndex) const {
            static_assert(M == N, "Matrix must be a square matrix.");
            if (M == 2) {
                if (rowIndex == 0) {
                    if (columnIndex == 0) return m_Elements[1][1];
                    return m_Elements[1][0];
                }
                if (columnIndex == 0) return m_Elements[0][1];
                return m_Elements[0][0];
            }
            Matrix<M - 1, N - 1, T> submatrix;
            for (std::size_t i = 0; i < rowIndex; ++i) {
                std::copy_n(static_cast<const T*>(m_Elements[i]), columnIndex, static_cast<T*>(submatrix[i]));
                std::copy_n(static_cast<const T*>(m_Elements[i] + columnIndex + 1), M - columnIndex - 1, static_cast<T*>(submatrix[i] + columnIndex));
            }
            for (std::size_t i = rowIndex + 1; i < M; ++i) {
                std::copy_n(static_cast<const T*>(m_Elements[i]), columnIndex, static_cast<T*>(submatrix[i - 1]));
                std::copy_n(static_cast<const T*>(m_Elements[i] + columnIndex + 1), M - columnIndex - 1, static_cast<T*>(submatrix[i - 1] + columnIndex));
            }
            return submatrix.GetDeterminant();
        }

        /**
         * @brief Gets the raw matrix data.
         * @note Raw data is stored in row-major order.
         * @return const T* Raw data.
         */
        inline const T* GetRawData() const {
            return reinterpret_cast<const T*>(m_Elements);
        }

    private:

        T m_Elements[M][N];

    };

    /**
     * @brief Unary plus operation.
     * @tparam M Number of rows.
     * @tparam N Number of columns.
     * @tparam T Element type.
     * @param matrix %Matrix operand.
     * @return Matrix<M, N, T> Resulting matrix.
     */
    template<std::size_t M, std::size_t N, typename T>
    inline Matrix<M, N, T> operator+(Matrix<M, N, T> matrix) {
        return matrix;
    }

    /**
     * @brief Unary minus operation.
     * @tparam M Number of rows.
     * @tparam N Number of columns.
     * @tparam T Element type.
     * @param matrix %Matrix operand.
     * @return Matrix<M, N, T> Resulting matrix.
     */
    template<std::size_t M, std::size_t N, typename T>
    inline Matrix<M, N, T> operator-(Matrix<M, N, T> matrix) {
        for (std::size_t i = 0; i < M; ++i) {
            for (std::size_t j = 0; j < N; ++j) {
                matrix[i][j] = -matrix[i][j];
            }
        }
        return matrix;
    }

    /**
     * @brief Adds two matrices.
     * @tparam M Number of rows.
     * @tparam N Number of columns.
     * @tparam T Element type.
     * @param a First matrix.
     * @param b Second matrix.
     * @return Matrix<M, N, T> Resulting matrix.
     */
    template<std::size_t M, std::size_t N, typename T>
    inline Matrix<M, N, T> operator+(Matrix<M, N, T> a, const Matrix<M, N, T>& b) {
        a += b;
        return a;
    }

    /**
     * @brief Subtracts two matrices.
     * @tparam M Number of rows.
     * @tparam N Number of columns.
     * @tparam T Element type.
     * @param a First matrix.
     * @param b Second matrix.
     * @return Matrix<M, N, T> Resulting matrix.
     */
    template<std::size_t M, std::size_t N, typename T>
    inline Matrix<M, N, T> operator-(Matrix<M, N, T> a, const Matrix<M, N, T>& b) {
        a -= b;
        return a;
    }

    /**
     * @brief Multiplies two matrices.
     * @tparam M Number of rows of the first matrix.
     * @tparam N Number of columns of the first matrix and number of rows of the second matrix.
     * @tparam K Number of columns of the second matrix.
     * @tparam T Element type.
     * @param a First matrix.
     * @param b Second matrix.
     * @return Matrix<M, K, T> Resulting matrix. 
     */
    template<std::size_t M, std::size_t N, std::size_t K, typename T>
    inline Matrix<M, K, T> operator*(const Matrix<M, N, T>& a, const Matrix<N, K, T>& b) {
        Matrix<M, K, T> result;
        for (std::size_t i = 0; i < M; ++i) {
            for (std::size_t j = 0; j < K; ++j) {
                result[i][j] = static_cast<T>(0);
                for (std::size_t k = 0; k < N; ++k) {
                    result[i][j] += a[i][k] * b[k][j];
                }
            }
        }
        return result;
    }

    /**
     * @brief Multiplies matrix with a scalar.
     * @tparam M Number of rows.
     * @tparam N Number of columns.
     * @tparam T Element type.
     * @param matrix %Matrix operand.
     * @param scalar Scalar value.
     * @return Matrix<M, N, T> Resulting matrix.
     */
    template<std::size_t M, std::size_t N, typename T>
    inline Matrix<M, N, T> operator*(Matrix<M, N, T> matrix, T scalar) {
        matrix *= scalar;
        return matrix;
    }

    /**
     * @brief Multiplies scalar with a matrix.
     * @tparam M Number of rows.
     * @tparam N Number of columns.
     * @tparam T Element type.
     * @param scalar Scalar value.
     * @param matrix %Matrix operand.
     * @return Matrix<M, N, T> Resulting matrix.
     */
    template<std::size_t M, std::size_t N, typename T>
    inline Matrix<M, N, T> operator*(T scalar, Matrix<M, N, T> matrix) {
        matrix *= scalar;
        return matrix;
    }

    /**
     * @brief Divides matrix by a scalar.
     * @tparam M Number of rows.
     * @tparam N Number of columns.
     * @tparam T Element type.
     * @param matrix %Matrix operand.
     * @param scalar Scalar value.
     * @return Matrix<M, N, T> Resulting matrix.
     */
    template<std::size_t M, std::size_t N, typename T>
    inline Matrix<M, N, T> operator/(Matrix<M, N, T> matrix, T scalar) {
        matrix /= scalar;
        return matrix;
    }

    /**
     * @brief Divides scalar by a matrix.
     * @tparam M Number of rows.
     * @tparam N Number of columns.
     * @tparam T Element type.
     * @param scalar Scalar value.
     * @param matrix %Matrix operand.
     * @return Matrix<M, N, T> Resulting matrix.
     */
    template<std::size_t M, std::size_t N, typename T>
    inline Matrix<M, N, T> operator/(T scalar, Matrix<M, N, T> matrix) {
        for (std::size_t i = 0; i < M; ++i) {
            for (std::size_t j = 0; j < N; ++j) {
                matrix[i][j] = scalar / matrix[i][j];
            }
        }
        return matrix;
    }

    /**
     * @brief Writes matrix text representation to output stream.
     * @tparam M Number of rows.
     * @tparam N Number of columns.
     * @tparam T Element type.
     * @param ostr Output stream.
     * @param matrix %Matrix operand.
     * @return std::ostream& Output stream.
     */
    template<std::size_t M, std::size_t N, typename T>
    inline std::ostream& operator<<(std::ostream& ostr, const Matrix<M, N, T>& matrix) {
        ostr << "[ ";
        for (std::size_t i = 0; i < M; ++i) {
            if (i != 0) {
                ostr << "| ";
            }
            for (std::size_t j = 0; j < N; ++j) {
                ostr << matrix[i][j] << " ";
            }
        }
        ostr << "]";
        return ostr;
    }

    /**
     * @brief Reads matrix from text representation off input stream.
     * @tparam M Number of rows.
     * @tparam N Number of columns.
     * @tparam T Element type.
     * @param istr Input stream.
     * @param matrix %Matrix operand.
     * @return std::istream& Input stream.
     */
    template<std::size_t M, std::size_t N, typename T>
    inline std::istream& operator>>(std::istream& istr, Matrix<M, N, T>& matrix) {
        while (istr.peek() == ' ') istr.get();
        if (istr.peek() == '[') istr.get();
        for (std::size_t i = 0; i < M; ++i) {
            if (i != 0) {
                while (istr.peek() == ' ') istr.get();
                if (istr.peek() == '|') istr.get();
            }
            for (std::size_t j = 0; j < N; ++j) {
                istr >> matrix[i][j];
            }
        }
        while (istr.peek() == ' ') istr.get();
        if (istr.peek() == ']') istr.get();
        return istr;
    }

    /**
     * @brief Tests two matrices for equality.
     * @note When comparing floating point elements, the values must be exact (no epsilon error) for them to be considered equal.
     * @tparam M Number of rows.
     * @tparam N Number of columns.
     * @tparam T Element type.
     * @param a First matrix.
     * @param b Second matrix.
     * @return true All elements are equal.
     * @return false Some elements are not equal.
     */
    template<std::size_t M, std::size_t N, typename T>
    inline bool operator==(const Matrix<M, N, T>& a, const Matrix<M, N, T>& b) {
        return std::equal(a.GetRawData(), a.GetRawData() + M * N, b.GetRawData());
    }

    /**
     * @brief Tests two matrices for inequality.
     * @note When comparing floating point elements, the values must be exact (no epsilon error) for them to be considered equal.
     * @tparam M Number of rows.
     * @tparam N Number of columns.
     * @tparam T Element type.
     * @param a First matrix.
     * @param b Second matrix.
     * @return true Some elements are not equal.
     * @return false All elements are equal.
     */
    template<std::size_t M, std::size_t N, typename T>
    inline bool operator!=(const Matrix<M, N, T>& a, const Matrix<M, N, T>& b) {
        return !(a == b);
    }

    /**
     * @brief Alias for single precision matrix.
     * @tparam M Number of rows.
     * @tparam N Number of columns.
     */
    template<std::size_t M, std::size_t N>
    using Matrixf = Matrix<M, N, float>;

    /**
     * @brief Alias for 2x2 single precision matrix.
     */
    using Matrix2x2f = Matrixf<2, 2>;

    /**
     * @brief Alias for 2x2 single precision matrix.
     */
    using Matrix2f = Matrix2x2f;

    /**
     * @brief Alias for 2x3 single precision matrix.
     */
    using Matrix2x3f = Matrixf<2, 3>;

    /**
     * @brief Alias for 2x4 single precision matrix.
     */
    using Matrix2x4f = Matrixf<2, 4>;

    /**
     * @brief Alias for 3x2 single precision matrix.
     */
    using Matrix3x2f = Matrixf<3, 2>;

    /**
     * @brief Alias for 3x3 single precision matrix.
     */
    using Matrix3x3f = Matrixf<3, 3>;

    /**
     * @brief Alias for 3x3 single precision matrix.
     */
    using Matrix3f = Matrix3x3f;

    /**
     * @brief Alias for 3x4 single precision matrix.
     */
    using Matrix3x4f = Matrixf<3, 4>;

    /**
     * @brief Alias for 4x2 single precision matrix.
     */
    using Matrix4x2f = Matrixf<4, 2>;

    /**
     * @brief Alias for 4x3 single precision matrix.
     */
    using Matrix4x3f = Matrixf<4, 3>;

    /**
     * @brief Alias for 4x4 single precision matrix.
     */
    using Matrix4x4f = Matrixf<4, 4>;
    
    /**
     * @brief Alias for 4x4 single precision matrix.
     */
    using Matrix4f = Matrix4x4f;

    /**
     * @brief Alias for double precision matrix.
     * @tparam M Number of rows.
     * @tparam N Number of column.
     */
    template<std::size_t M, std::size_t N>
    using Matrixd = Matrix<M, N, double>;

    /**
     * @brief Alias for 2x2 double precision matrix.
     */
    using Matrix2x2d = Matrixd<2, 2>;

    /**
     * @brief Alias for 2x2 double precision matrix.
     */
    using Matrix2d = Matrix2x2d;

    /**
     * @brief Alias for 2x3 double precision matrix.
     */
    using Matrix2x3d = Matrixd<2, 3>;

    /**
     * @brief Alias for 2x4 double precision matrix.
     */
    using Matrix2x4d = Matrixd<2, 4>;

    /**
     * @brief Alias for 3x2 double precision matrix.
     */
    using Matrix3x2d = Matrixd<3, 2>;

    /**
     * @brief Alias for 3x3 double precision matrix.
     */
    using Matrix3x3d = Matrixd<3, 3>;

    /**
     * @brief Alias for 3x3 double precision matrix.
     */
    using Matrix3d = Matrix3x3d;

    /**
     * @brief Alias for 3x4 double precision matrix.
     */
    using Matrix3x4d = Matrixd<3, 4>;

    /**
     * @brief Alias for 4x2 double precision matrix.
     */
    using Matrix4x2d = Matrixd<4, 2>;

    /**
     * @brief Alias for 4x3 double precision matrix.
     */
    using Matrix4x3d = Matrixd<4, 3>;

    /**
     * @brief Alias for 4x4 double precision matrix.
     */
    using Matrix4x4d = Matrixd<4, 4>;

    /**
     * @brief Alias for 4x4 double precision matrix.
     */
    using Matrix4d = Matrix4x4d;

    /**
     * @brief Alias for integer matrix.
     * @tparam M Number of rows.
     * @tparam N Number of columns.
     */
    template<std::size_t M, std::size_t N>
    using Matrixi = Matrix<M, N, int>;

    /**
     * @brief Alias for 2x2 integer matrix.
     */
    using Matrix2x2i = Matrixi<2, 2>;

    /**
     * @brief Alias for 2x2 integer matrix.
     */
    using Matrix2i = Matrix2x2i;

    /**
     * @brief Alias for 2x3 integer matrix.
     */
    using Matrix2x3i = Matrixi<2, 3>;

    /**
     * @brief Alias for 2x4 integer matrix.
     */
    using Matrix2x4i = Matrixi<2, 4>;

    /**
     * @brief Alias for 3x2 integer matrix.
     */
    using Matrix3x2i = Matrixi<3, 2>;

    /**
     * @brief Alias for 3x3 integer matrix.
     */
    using Matrix3x3i = Matrixi<3, 3>;

    /**
     * @brief Alias for 3x3 integer matrix.
     */
    using Matrix3i = Matrix3x3i;

    /**
     * @brief Alias for 3x4 integer matrix.
     */
    using Matrix3x4i = Matrixi<3, 4>;

    /**
     * @brief Alias for 4x2 integer matrix.
     */
    using Matrix4x2i = Matrixi<4, 2>;

    /**
     * @brief Alias for 4x3 integer matrix.
     */
    using Matrix4x3i = Matrixi<4, 3>;

    /**
     * @brief Alias for 4x4 integer matrix.
     */
    using Matrix4x4i = Matrixi<4, 4>;

    /**
     * @brief Alias for 4x4 integer matrix.
     */
    using Matrix4i = Matrix4x4i;

    /**
     * @cond HIDDEN_SYMBOLS
     */
    template<typename T>
    class Matrix<1, 1, T> {

    friend class Matrix<2, 2, T>;

    private:

        inline Matrix() = default;

        inline T* operator[](std::size_t index) { return nullptr; }

        inline T GetDeterminant() { return static_cast<T>(0); }

    };
    /**
     * @endcond
     */

} }

#endif
