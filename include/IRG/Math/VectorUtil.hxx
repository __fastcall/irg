#ifndef __IRG_MATH_VECTORUTIL_HXX__
#define __IRG_MATH_VECTORUTIL_HXX__

#include "Vector.hxx"

namespace IRG { namespace Math {

    /**
     * @brief Constains vector helper methods.
     * @tparam N Number of components.
     * @tparam T Component type.
     */
    template<std::size_t N, typename T>
    class VectorUtil {

    public:

        /**
         * @brief Computes distance between two vector points.
         * @param a First vector point.
         * @param b Second vector point.
         * @return T Point distance.
         */
        inline static T Distance(const Vector<N, T>& a, const Vector<N, T>& b) {
            return (a - b).GetLength();
        }

        /**
         * @brief Computes reflected vector from input vector and surface normal.
         * @note Surface normal should be normalized. Vector must consist of either two or three components.
         * @param input Input vector.
         * @param surfaceNormal Surface normal.
         * @return Vector<N, T> Reflected vector.
         */
        inline static Vector<N, T> Reflect(const Vector<N, T>& input, const Vector<N, T>& surfaceNormal) {
            static_assert(N == 2 || N == 3, "Vector must consist of either two or three components.");
            return input - static_cast<T>(2) * (surfaceNormal * input) * surfaceNormal;
        }

        /**
         * @brief Computes refracted vector from input vector and surface normal with respects to indices of refraction.
         * @note Surface normal should be normalized. Vector must consist of either two or three components.
         * @param input Input vector.
         * @param surfaceNormal Surface normal.
         * @param eta Ratio of indices of refraction.
         * @return Vector<N, T> Refracted vector.
         */
        inline static Vector<N, T> Refract(const Vector<N, T>& input, const Vector<N, T>& surfaceNormal, T eta) {
            static_assert(N == 2 || N == 3, "Vector must consist of either two or three components.");
            T normalDotInput = surfaceNormal * input;
            T k = static_cast<T>(1) - eta * eta * (static_cast<T>(1) - normalDotInput * normalDotInput);
            if (k < static_cast<T>(0)) {
                return Vector<N, T>();
            }
            return eta * input - (eta * normalDotInput + static_cast<T>(std::sqrt(static_cast<double>(k)))) * surfaceNormal;
        }

    private:

        inline VectorUtil() = delete;

    };

    /**
     * @brief Alias for single precision vector utilities class.
     * @tparam N Number of components.
     */
    template<std::size_t N>
    using VectorUtilf = VectorUtil<N, float>;

    /**
     * @brief Alias for two-dimensional single precision vector utilities class.
     */
    using VectorUtil2f = VectorUtilf<2>;

    /**
     * @brief Alias for three-dimensional single precision vector utilities class.
     */
    using VectorUtil3f = VectorUtilf<3>;

    /**
     * @brief Alias for four-dimensional single precision vector utilities class.
     */
    using VectorUtil4f = VectorUtilf<4>;

    /**
     * @brief Alias for double precision vector utilities class.
     * @tparam N Number of components.
     */
    template<std::size_t N>
    using VectorUtild = VectorUtil<N, double>;

    /**
     * @brief Alias for two-dimensional double precision vector utilities class.
     */
    using VectorUtil2d = VectorUtild<2>;

    /**
     * @brief Alias for three-dimensional double precision vector utilities class.
     */
    using VectorUtil3d = VectorUtild<3>;

    /**
     * @brief Alias for four-dimensional double precision vector utilities class.
     */
    using VectorUtil4d = VectorUtild<4>;

    /**
     * @brief Alias for integer vector utilities class.
     * @tparam N Number of components.
     */
    template<std::size_t N>
    using VectorUtili = VectorUtil<N, int>;

    /**
     * @brief Alias for two-dimensional integer vector utilities class.
     */
    using VectorUtil2i = VectorUtili<2>;

    /**
     * @brief Alias for three-dimensional integer vector utilities class.
     */
    using VectorUtil3i = VectorUtili<3>;

    /**
     * @brief Alias for four-dimensional integer vector utilities class.
     */
    using VectorUtil4i = VectorUtili<4>;

} }

#endif
