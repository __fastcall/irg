#ifndef __IRG_MATH_MATRIXCOMPARATOR_HXX__
#define __IRG_MATH_MATRIXCOMPARATOR_HXX__

#include "Matrix.hxx"

namespace IRG { namespace Math {

    /**
     * @brief Compares matrices with account for numeric errors.
     * @tparam M Number of rows.
     * @tparam N Number of columns.
     * @tparam T Element type.
     */
    template<std::size_t M, std::size_t N, typename T>
    class MatrixComparator {
    
    public:

        /**
         * @brief Tests two matrices for equality with respect to epsilon error.
         * @param a First matrix.
         * @param b Second matrix.
         * @param epsilon Maximum allowed error.
         * @return true All elements are equal with respect to epsilon error.
         * @return false Some elements are not equal with respect to epsilon error.
         */
        inline static bool AreEqual(const Matrix<M, N, T>& a, const Matrix<M, N, T>& b, T epsilon = static_cast<T>(1e-6)) {
            for (std::size_t i = 0; i < N; ++i) {
                for (std::size_t j = 0; j < N; ++j) {
                    if (std::fabs(a[i][j] - b[i][j]) > epsilon) {
                        return false;
                    }
                }
            }
            return true;
        }

        /**
         * @brief Tests two matrices for inequality with respect to epsilon error.
         * @param a First matrix.
         * @param b Second matrix.
         * @param epsilon Maximum allowed error.
         * @return true Some elements are not equal with respect to epsilon error.
         * @return false All elements are equal with respect to epsilon error.
         */
        inline static bool AreNotEqual(const Matrix<M, N, T>& a, const Matrix<M, N, T>& b, T epsilon = static_cast<T>(1e-6)) {
            return !AreEqual(a, b, epsilon);
        }

    private:

        inline MatrixComparator() = delete;

    };

    /**
     * @brief Alias for single precision matrix comparator.
     * @tparam M Number of rows.
     * @tparam N Number of columns.
     */
    template<std::size_t M, std::size_t N>
    using MatrixComparatorf = MatrixComparator<M, N, float>;

    /**
     * @brief Alias for 2x2 single precision matrix comparator.
     */
    using MatrixComparator2x2f = MatrixComparatorf<2, 2>;

    /**
     * @brief Alias for 2x2 single precision matrix comparator.
     */
    using MatrixComparator2f = MatrixComparator2x2f;

    /**
     * @brief Alias for 2x3 single precision matrix comparator.
     */
    using MatrixComparator2x3f = MatrixComparatorf<2, 3>;

    /**
     * @brief Alias for 2x4 single precision matrix comparator.
     */
    using MatrixComparator2x4f = MatrixComparatorf<2, 4>;

    /**
     * @brief Alias for 3x2 single precision matrix comparator.
     */
    using MatrixComparator3x2f = MatrixComparatorf<3, 2>;

    /**
     * @brief Alias for 3x3 single precision matrix comparator.
     */
    using MatrixComparator3x3f = MatrixComparatorf<3, 3>;

    /**
     * @brief Alias for 3x3 single precision matrix comparator.
     */
    using MatrixComparator3f = MatrixComparator3x3f;

    /**
     * @brief Alias for 3x4 single precision matrix comparator.
     */
    using MatrixComparator3x4f = MatrixComparatorf<3, 4>;

    /**
     * @brief Alias for 4x2 single precision matrix comparator.
     */
    using MatrixComparator4x2f = MatrixComparatorf<4, 2>;

    /**
     * @brief Alias for 4x3 single precision matrix comparator.
     */
    using MatrixComparator4x3f = MatrixComparatorf<4, 3>;

    /**
     * @brief Alias for 4x4 single precision matrix comparator.
     */
    using MatrixComparator4x4f = MatrixComparatorf<4, 4>;
    
    /**
     * @brief Alias for 4x4 single precision matrix comparator.
     */
    using MatrixComparator4f = MatrixComparator4x4f;

    /**
     * @brief Alias for double precision matrix comparator.
     * @tparam M Number of rows.
     * @tparam N Number of column.
     */
    template<std::size_t M, std::size_t N>
    using MatrixComparatord = MatrixComparator<M, N, double>;

    /**
     * @brief Alias for 2x2 double precision matrix comparator.
     */
    using MatrixComparator2x2d = MatrixComparatord<2, 2>;

    /**
     * @brief Alias for 2x2 double precision matrix comparator.
     */
    using MatrixComparator2d = MatrixComparator2x2d;

    /**
     * @brief Alias for 2x3 double precision matrix comparator.
     */
    using MatrixComparator2x3d = MatrixComparatord<2, 3>;

    /**
     * @brief Alias for 2x4 double precision matrix comparator.
     */
    using MatrixComparator2x4d = MatrixComparatord<2, 4>;

    /**
     * @brief Alias for 3x2 double precision matrix comparator.
     */
    using MatrixComparator3x2d = MatrixComparatord<3, 2>;

    /**
     * @brief Alias for 3x3 double precision matrix comparator.
     */
    using MatrixComparator3x3d = MatrixComparatord<3, 3>;

    /**
     * @brief Alias for 3x3 double precision matrix comparator.
     */
    using MatrixComparator3d = MatrixComparator3x3d;

    /**
     * @brief Alias for 3x4 double precision matrix comparator.
     */
    using MatrixComparator3x4d = MatrixComparatord<3, 4>;

    /**
     * @brief Alias for 4x2 double precision matrix comparator.
     */
    using MatrixComparator4x2d = MatrixComparatord<4, 2>;

    /**
     * @brief Alias for 4x3 double precision matrix comparator.
     */
    using MatrixComparator4x3d = MatrixComparatord<4, 3>;

    /**
     * @brief Alias for 4x4 double precision matrix comparator.
     */
    using MatrixComparator4x4d = MatrixComparatord<4, 4>;

    /**
     * @brief Alias for 4x4 double precision matrix comparator.
     */
    using MatrixComparator4d = MatrixComparator4x4d;

    /**
     * @brief Alias for integer matrix comparator.
     * @tparam M Number of rows.
     * @tparam N Number of columns.
     */
    template<std::size_t M, std::size_t N>
    using MatrixComparatori = MatrixComparator<M, N, int>;

    /**
     * @brief Alias for 2x2 integer matrix comparator.
     */
    using MatrixComparator2x2i = MatrixComparatori<2, 2>;

    /**
     * @brief Alias for 2x2 integer matrix comparator.
     */
    using MatrixComparator2i = MatrixComparator2x2i;

    /**
     * @brief Alias for 2x3 integer matrix comparator.
     */
    using MatrixComparator2x3i = MatrixComparatori<2, 3>;

    /**
     * @brief Alias for 2x4 integer matrix comparator.
     */
    using MatrixComparator2x4i = MatrixComparatori<2, 4>;

    /**
     * @brief Alias for 3x2 integer matrix comparator.
     */
    using MatrixComparator3x2i = MatrixComparatori<3, 2>;

    /**
     * @brief Alias for 3x3 integer matrix comparator.
     */
    using MatrixComparator3x3i = MatrixComparatori<3, 3>;

    /**
     * @brief Alias for 3x3 integer matrix comparator.
     */
    using MatrixComparator3i = MatrixComparator3x3i;

    /**
     * @brief Alias for 3x4 integer matrix comparator.
     */
    using MatrixComparator3x4i = MatrixComparatori<3, 4>;

    /**
     * @brief Alias for 4x2 integer matrix comparator.
     */
    using MatrixComparator4x2i = MatrixComparatori<4, 2>;

    /**
     * @brief Alias for 4x3 integer matrix comparator.
     */
    using MatrixComparator4x3i = MatrixComparatori<4, 3>;

    /**
     * @brief Alias for 4x4 integer matrix comparator.
     */
    using MatrixComparator4x4i = MatrixComparatori<4, 4>;

    /**
     * @brief Alias for 4x4 integer matrix comparator.
     */
    using MatrixComparator4i = MatrixComparator4x4i;

} }

#endif
