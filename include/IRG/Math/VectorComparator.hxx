#ifndef __IRG_MATH_VECTORCOMPARATOR_HXX__
#define __IRG_MATH_VECTORCOMPARATOR_HXX__

#include "Vector.hxx"

namespace IRG { namespace Math {

    /**
     * @brief Compares vectors with account for numeric errors.
     * @tparam N Number of components.
     * @tparam T Component type.
     */
    template<std::size_t N, typename T>
    class VectorComparator {

    public:

        /**
         * @brief Tests two vectors for equality with respect to epsilon error.
         * @param a First vector.
         * @param b Second vector.
         * @param epsilon Maximum allowed error.
         * @return true All components are equal with respect to epsilon error.
         * @return false Some components are not equal with respect to epsilon error.
         */
        inline static bool AreEqual(const Vector<N, T>& a, const Vector<N, T>& b, T epsilon = static_cast<T>(1e-6)) {
            for (std::size_t i = 0; i < N; ++i) {
                if (std::fabs(a[i] - b[i]) > epsilon) {
                    return false;
                }
            }
            return true;
        }

        /**
         * @brief Tests two vectors for inequality with respect to epsilon error.
         * @param a First vector.
         * @param b Second vector.
         * @param epsilon Maximum allowed error.
         * @return true Some components are not equal with respect to epsilon error.
         * @return false All components are equal with respect to epsilon error.
         */
        inline static bool AreNotEqual(const Vector<N, T>& a, const Vector<N, T>& b, T epsilon = static_cast<T>(1e-6)) {
            return !AreEqual(a, b, epsilon);
        }

    private:

        inline VectorComparator() = delete;

    };

    /**
     * @brief Alias for single precision vector comparator.
     * @tparam N Number of components.
     */
    template<std::size_t N>
    using VectorComparatorf = VectorComparator<N, float>;

    /**
     * @brief Alias for two-dimensional single precision vector comparator.
     */
    using VectorComparator2f = VectorComparatorf<2>;

    /**
     * @brief Alias for three-dimensional single precision vector comparator.
     */
    using VectorComparator3f = VectorComparatorf<3>;

    /**
     * @brief Alias for four-dimensional single precision vector comparator.
     */
    using VectorComparator4f = VectorComparatorf<4>;

    /**
     * @brief Alias for double precision vector comparator.
     * @tparam N Number of components.
     */
    template<std::size_t N>
    using VectorComparatord = VectorComparator<N, double>;

    /**
     * @brief Alias for two-dimensional double precision vector comparator.
     */
    using VectorComparator2d = VectorComparatord<2>;

    /**
     * @brief Alias for three-dimensional double precision vector comparator.
     */
    using VectorComparator3d = VectorComparatord<3>;

    /**
     * @brief Alias for four-dimensional double precision vector comparator.
     */
    using VectorComparator4d = VectorComparatord<4>;

    /**
     * @brief Alias for integer vector comparator.
     * @tparam N Number of components.
     */
    template<std::size_t N>
    using VectorComparatori = VectorComparator<N, int>;

    /**
     * @brief Alias for two-dimensional integer vector comparator.
     */
    using VectorComparator2i = VectorComparatori<2>;

    /**
     * @brief Alias for three-dimensional integer vector comparator.
     */
    using VectorComparator3i = VectorComparatori<3>;

    /**
     * @brief Alias for four-dimensional integer vector comparator.
     */
    using VectorComparator4i = VectorComparatori<4>;

} }

#endif
