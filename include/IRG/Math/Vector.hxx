#ifndef __IRG_MATH_VECTOR_HXX__
#define __IRG_MATH_VECTOR_HXX__

#include <cstddef>
#include <cmath>
#include <type_traits>
#include <algorithm>
#include <ostream>
#include <istream>
#include <sstream>
#include <string>
#include <initializer_list>

namespace IRG { namespace Math {

    /**
     * @brief Represents a mathematical vector.
     * @note %Vector must consist of at least two components. %Vector component must be of arithmetic type.
     * @tparam N Number of components.
     * @tparam T Component type.
     */
    template<std::size_t N, typename T>
    class Vector {

        static_assert(N >= 2, "Vector must consist of at least two components.");
        static_assert(std::is_arithmetic<T>::value, "Vector component must be of arithmetic type.");

    public:

        /**
         * @brief Constructs a new vector instance.
         * @param value Value of every component.
         */
        inline Vector(T value = static_cast<T>(0)) {
            std::fill_n(m_Elements, N, value);
        }

        /**
         * @brief Constructs a new vector instance from an initializer list.
         * @param list Initializer list of component values.
         */
        inline Vector(const std::initializer_list<T>& list) {
            std::size_t limit = (N > list.size()) ? list.size() : N;
            std::copy_n(list.begin(), limit, m_Elements);
            std::fill_n(m_Elements + limit, N - limit, static_cast<T>(0));
        }

        /**
         * @brief Constructs a new vector instance by copying another vector of same component type.
         * @param vector %Vector to copy.
         */
        inline Vector(const Vector& vector) {
            std::copy_n(vector.m_Elements, N, m_Elements);
        }

        /**
         * @brief Constructs a new vector instance by partially copying another vector of same component type.
         * @details If the desired number of components exceeds the number of components in the other vector, the remaining components will be set to the provided value.
         * @tparam M Number of components in the vector to partially copy.
         * @param vector %Vector to partially copy.
         * @param value Value of every remaining component.
         */
        template<std::size_t M>
        inline Vector(const Vector<M, T>& vector, T value = static_cast<T>(0)) {
            constexpr std::size_t limit = (N > M) ? M : N;
            std::copy_n(vector.GetRawData(), limit, m_Elements);
            std::fill_n(m_Elements + limit, N - limit, value);
        }

        /**
         * @brief Constructs a new vector instance from its string representation.
         * @param string %Vector string representation.
         */
        inline Vector(const std::string& string) {
            std::istringstream istr(string);
            istr >> *this;
        }

        /**
         * @brief Construct a new vector instance from raw data of component values.
         * @param rawData Raw data of component values.
         */
        inline Vector(const T* rawData) {
            std::copy_n(rawData, N, m_Elements);
        }

        /**
         * @brief Copies component values from another vector of same component type.
         * @param vector %Vector to copy.
         * @return Vector& Resulting vector.
         */
        inline Vector& operator=(const Vector& vector) {
            std::copy_n(vector.m_Elements, N, m_Elements);
            return *this;
        }

        /**
         * @brief Adds vector of same dimension and component type.
         * @param vector %Vector to add.
         * @return Vector& Resulting vector.
         */
        inline Vector& operator+=(const Vector& vector) {
            for (std::size_t i = 0; i < N; ++i) {
                m_Elements[i] += vector.m_Elements[i];
            }
            return *this;
        }

        /**
         * @brief Subtracts vector of same dimension and component type.
         * @param vector %Vector to subtract.
         * @return Vector& Resulting vector.
         */
        inline Vector& operator-=(const Vector& vector) {
            for (std::size_t i = 0; i < N; ++i) {
                m_Elements[i] -= vector.m_Elements[i];
            }
            return *this;
        }

        /**
         * @brief Multiplies with a scalar value.
         * @param scalar Scalar value.
         * @return Vector& Resulting vector.
         */
        inline Vector& operator*=(T scalar) {
            for (std::size_t i = 0; i < N; ++i) {
                m_Elements[i] *= scalar;
            }
            return *this;
        }

        /**
         * @brief Divides by a scalar value.
         * @param scalar Scalar value.
         * @return Vector& Resulting vector.
         */
        inline Vector& operator/=(T scalar) {
            for (std::size_t i = 0; i < N; ++i) {
                m_Elements[i] /= scalar;
            }
            return *this;
        }

        /**
         * @brief Gets the immutable component value.
         * @param index Component index.
         * @return T Component value.
         */
        inline T operator[](std::size_t index) const {
            return m_Elements[index];
        }

        /**
         * @brief Gets the mutable component value.
         * @param index Component index.
         * @return T& Component value.
         */
        inline T& operator[](std::size_t index) {
            return m_Elements[index];
        }

        /**
         * @brief Converts vector to string.
         * @return std::string %Vector string.
         */
        inline operator std::string() const {
            std::ostringstream ostr;
            ostr << *this;
            return ostr.str();
        }

        /**
         * @brief Gets the vector dimension.
         * @return constexpr std::size_t Number of components.
         */
        inline constexpr std::size_t GetDimension() const {
            return N;
        }

        /**
         * @brief Computes the length of the vector.
         * @return T %Vector length.
         */
        inline T GetLength() const {
            T result = static_cast<T>(0);
            for (std::size_t i = 0; i < N; ++i) {
                result += m_Elements[i] * m_Elements[i];
            }
            return static_cast<T>(std::sqrt(static_cast<long double>(result)));
        }

        /**
         * @brief Computes a normalized vector copy.
         * @return Vector Normalized vector.
         */
        inline Vector GetNormalized() const {
            Vector result = *this;
            result.Normalize();
            return result;
        }

        /**
         * @brief Normalizes a vector.
         */
        inline void Normalize() {
            *this /= GetLength();
        }

        /**
         * @brief Gets the raw vector data.
         * @return const T* Raw data.
         */
        inline const T* GetRawData() const {
            return static_cast<const T*>(m_Elements);
        }

        /**
         * @brief Constructs a Cartesian vector from homogeneous vector (last coordinate is treated as homogeneous coordinate).
         * @param vector Homogeneous vector.
         * @return Vector Cartesian vector.
         */
        inline static Vector FromHomogeneous(const Vector<N + 1, T>& vector) {
            Vector result(vector);
            result /= vector[N];
            return result;
        }

        /**
         * @brief Constructs a homogeneous vector (with homogeneous coordinate set to 1) from Cartesian vector.
         * @param vector Cartesian vector.
         * @return Vector Homogeneous vector.
         */
        inline static Vector ToHomogeneous(const Vector<N - 1, T>& vector) {
            return Vector(vector, static_cast<T>(1));
        }

        /**
         * @brief Computes a cosine of the angle between two vectors.
         * @param a First vector.
         * @param b Second vector.
         * @return T Cosine of the angle.
         */
        inline static T Cosine(const Vector& a, const Vector& b) {
            return (a * b) / (a.GetLength() * b.GetLength());
        }

        /**
         * @brief Computes a dot product of two vectors.
         * @param a First vector.
         * @param b Second vector.
         * @return T Dot product.
         */
        inline static T DotProduct(const Vector& a, const Vector& b) {
            return a * b;
        }

        /**
         * @brief Computes a cross product of two three-dimensional vectors.
         * @param a First vector.
         * @param b Second vector.
         * @return Vector<3, T> Cross product.
         */
        inline static Vector<3, T> CrossProduct(const Vector<3, T>& a, const Vector<3, T>& b) {
            static_assert(N == 3, "Vector must consist of exactly three components.");
            Vector<3, T> result;
            result.m_Elements[0] = a.m_Elements[1] * b.m_Elements[2] - a.m_Elements[2] * b.m_Elements[1];
            result.m_Elements[1] = a.m_Elements[2] * b.m_Elements[0] - a.m_Elements[0] * b.m_Elements[2];
            result.m_Elements[2] = a.m_Elements[0] * b.m_Elements[1] - a.m_Elements[1] * b.m_Elements[0];
            return result;
        }

    private:

        T m_Elements[N];

    };

    /**
     * @brief Unary plus operation.
     * @tparam N Number of components.
     * @tparam T Component type.
     * @param vector %Vector operand.
     * @return Vector<N, T> Resulting vector.
     */
    template<std::size_t N, typename T>
    inline Vector<N, T> operator+(Vector<N, T> vector) {
        return vector;
    }

    /**
     * @brief Unary minus operation.
     * @tparam N Number of components.
     * @tparam T Component type.
     * @param vector %Vector operand.
     * @return Vector<N, T> Resulting vector.
     */
    template<std::size_t N, typename T>
    inline Vector<N, T> operator-(Vector<N, T> vector) {
        for (std::size_t i = 0; i < N; ++i) {
            vector[i] = -vector[i];
        }
        return vector;
    }

    /**
     * @brief Adds two vectors.
     * @tparam N Number of components.
     * @tparam T Component type.
     * @param a First vector.
     * @param b Second vector.
     * @return Vector<N, T> Resulting vector.
     */
    template<std::size_t N, typename T>
    inline Vector<N, T> operator+(Vector<N, T> a, const Vector<N, T>& b) {
        a += b;
        return a;
    }

    /**
     * @brief Subtracts two vectors.
     * @tparam N Number of components.
     * @tparam T Component type.
     * @param a First vector.
     * @param b Second vector.
     * @return Vector<N, T> Resulting vector.
     */
    template<std::size_t N, typename T>
    inline Vector<N, T> operator-(Vector<N, T> a, const Vector<N, T>& b) {
        a -= b;
        return a;
    }

    /**
     * @brief Computes a dot product of two vectors.
     * @tparam N Number of components.
     * @tparam T Component type.
     * @param a First vector.
     * @param b Second vector.
     * @return T Dot product.
     */
    template<std::size_t N, typename T>
    inline T operator*(const Vector<N, T>& a, const Vector<N, T>& b) {
        T result = static_cast<T>(0);
        for (std::size_t i = 0; i < N; ++i) {
            result += a[i] * b[i];
        }
        return result;
    }

    /**
     * @brief Multiplies vector with a scalar.
     * @tparam N Number of components.
     * @tparam T Component type.
     * @param vector %Vector operand.
     * @param scalar Scalar value.
     * @return Vector<N, T> Resulting vector.
     */
    template<std::size_t N, typename T>
    inline Vector<N, T> operator*(Vector<N, T> vector, T scalar) {
        vector *= scalar;
        return vector;
    }

    /**
     * @brief Multiplies scalar with a vector.
     * @tparam N Number of components.
     * @tparam T Component type.
     * @param scalar Scalar value.
     * @param vector %Vector operand.
     * @return Vector<N, T> Resulting vector.
     */
    template<std::size_t N, typename T>
    inline Vector<N, T> operator*(T scalar, Vector<N, T> vector) {
        vector *= scalar;
        return vector;
    }

    /**
     * @brief Divides vector by a scalar.
     * @tparam N Number of components.
     * @tparam T Component type.
     * @param vector %Vector operand.
     * @param scalar Scalar value.
     * @return Vector<N, T> Resulting vector.
     */
    template<std::size_t N, typename T>
    inline Vector<N, T> operator/(Vector<N, T> vector, T scalar) {
        vector /= scalar;
        return vector;
    }

    /**
     * @brief Divides scalar by a vector.
     * @tparam N Number of components.
     * @tparam T Component type.
     * @param scalar Scalar value.
     * @param vector %Vector operand.
     * @return Vector<N, T> Resulting vector.
     */
    template<std::size_t N, typename T>
    inline Vector<N, T> operator/(T scalar, Vector<N, T> vector) {
        for (std::size_t i = 0; i < N; ++i) {
            vector[i] = scalar / vector[i];
        }
        return vector;
    }

    /**
     * @brief Writes vector text representation to output stream.
     * @tparam N Number of components.
     * @tparam T Component type.
     * @param ostr Output stream.
     * @param vector %Vector operand.
     * @return std::ostream& Output stream.
     */
    template <std::size_t N, typename T>
    inline std::ostream& operator<<(std::ostream& ostr, const Vector<N, T>& vector) {
        ostr << "[ ";
        for (std::size_t i = 0; i < N; ++i) {
            ostr << vector[i] << " ";
        }
        ostr << "]";
        return ostr;
    }

    /**
     * @brief Reads vector from text representation off input stream.
     * @tparam N Number of components.
     * @tparam T Component type.
     * @param istr Input stream.
     * @param vector %Vector operand.
     * @return std::istream& Input stream.
     */
    template <std::size_t N, typename T>
    inline std::istream& operator>>(std::istream& istr, Vector<N, T>& vector) {
        while (istr.peek() == ' ') istr.get();
        if (istr.peek() == '[') istr.get();
        for (std::size_t i = 0; i < N; ++i) {
            istr >> vector[i];
        }
        while (istr.peek() == ' ') istr.get();
        if (istr.peek() == ']') istr.get();
        return istr;
    }

    /**
     * @brief Tests two vectors for equality.
     * @note When comparing floating point components, the values must be exact (no epsilon error) for them to be considered equal.
     * @tparam N Number of components.
     * @tparam T Component type.
     * @param a First vector.
     * @param b Second vector.
     * @return true All components are equal.
     * @return false Some components are not equal.
     */
    template<std::size_t N, typename T>
    inline bool operator==(const Vector<N, T>& a, const Vector<N, T>& b) {
        return std::equal(a.GetRawData(), a.GetRawData() + N, b.GetRawData());
    }

    /**
     * @brief Tests two vectors for inequality.
     * @note When comparing floating point components, the values must be exact (no epsilon error) for them to be considered equal.
     * @tparam N Number of components.
     * @tparam T Component type.
     * @param a First vector.
     * @param b Second vector.
     * @return true Some components are not equal.
     * @return false All components are equal.
     */
    template<std::size_t N, typename T>
    inline bool operator!=(const Vector<N, T>& a, const Vector<N, T>& b) {
        return !(a == b);
    }

    /**
     * @brief Alias for single precision vector.
     * @tparam N Number of components.
     */
    template<std::size_t N>
    using Vectorf = Vector<N, float>;

    /**
     * @brief Alias for two-dimensional single precision vector.
     */
    using Vector2f = Vectorf<2>;

    /**
     * @brief Alias for three-dimensional single precision vector.
     */
    using Vector3f = Vectorf<3>;

    /**
     * @brief Alias for four-dimensional single precision vector.
     */
    using Vector4f = Vectorf<4>;

    /**
     * @brief Alias for double precision vector.
     * @tparam N Number of components.
     */
    template<std::size_t N>
    using Vectord = Vector<N, double>;

    /**
     * @brief Alias for two-dimensional double precision vector.
     */
    using Vector2d = Vectord<2>;

    /**
     * @brief Alias for three-dimensional double precision vector.
     */
    using Vector3d = Vectord<3>;

    /**
     * @brief Alias for four-dimensional double precision vector.
     */
    using Vector4d = Vectord<4>;

    /**
     * @brief Alias for integer vector.
     * @tparam N Number of components.
     */
    template<std::size_t N>
    using Vectori = Vector<N, int>;

    /**
     * @brief Alias for two-dimensional integer vector.
     */
    using Vector2i = Vectori<2>;

    /**
     * @brief Alias for three-dimensional integer vector.
     */
    using Vector3i = Vectori<3>;

    /**
     * @brief Alias for four-dimensional integer vector.
     */
    using Vector4i = Vectori<4>;

} }

#endif
