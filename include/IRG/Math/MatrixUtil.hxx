#ifndef __IRG_MATH_MATRIXUTIL_HXX__
#define __IRG_MATH_MATRIXUTIL_HXX__

#include "Matrix.hxx"
#include <cmath>

/**
 * @brief Converts degrees to radians.
 */
#define DEGREES(d) ((d) * 3.14159265358979323846264338327950288 / 180.0)

namespace IRG { namespace Math {

    /**
     * @brief Constains transformation matrix construction methods.
     * @tparam N Number of rows and columns.
     * @tparam T Element type.
     */
    template<std::size_t N, typename T>
    class MatrixUtil {

    public:

        /**
         * @brief Creates an identity matrix.
         * @return Matrix<N, N, T> Identity matrix.
         */
        inline static Matrix<N, N, T> CreateIdentity() {
            Matrix<N, N, T> matrix;
            for (std::size_t i = 0; i < N; ++i) {
                matrix[i][i] = static_cast<T>(1);
            }
            return matrix;
        }

        /**
         * @brief Creates a translation matrix.
         * @note %Matrix must be four dimensional.
         * @param tx Translation on X axis.
         * @param ty Translation on Y axis.
         * @param tz Translation on Z axis.
         * @return Matrix<N, N, T> Translation matrix.
         */
        inline static Matrix<N, N, T> CreateTranslation(T tx, T ty, T tz) {
            static_assert(N == 4, "Matrix must be four dimensional.");
            Matrix<N, N, T> translation = CreateIdentity();
            translation[0][3] = tx;
            translation[1][3] = ty;
            translation[2][3] = tz;
            return translation;
        }

        /**
         * @brief Creates a translation matrix.
         * @note %Matrix must be four dimensional.
         * @param translation Translation values.
         * @return Matrix<N, N, T> Translation matrix.
         */
        inline static Matrix<N, N, T> CreateTranslation(const Vector<3, T>& translation) {
            return CreateTranslation(translation[0], translation[1], translation[2]);
        }

        /**
         * @brief Creates a rotation matrix around the X axis.
         * @note %Matrix must be four dimensional.
         * @param rx Rotation around the X axis.
         * @return Matrix<N, N, T> Rotation matrix.
         */
        inline static Matrix<N, N, T> CreateRotationX(T rx) {
            static_assert(N == 4, "Matrix must be four dimensional.");
            Matrix<N, N, T> rotation;
            rotation[0][0] = static_cast<T>(1);
            rotation[1][1] = static_cast<T>(std::cos(static_cast<double>(rx)));
            rotation[1][2] = static_cast<T>(-std::sin(static_cast<double>(rx)));
            rotation[2][1] = static_cast<T>(std::sin(static_cast<double>(rx)));
            rotation[2][2] = static_cast<T>(std::cos(static_cast<double>(rx)));
            rotation[3][3] = static_cast<T>(1);
            return rotation;
        }

        /**
         * @brief Creates a rotation matrix around the Y axis.
         * @note %Matrix must be four dimensional.
         * @param ry Rotation around the Y axis.
         * @return Matrix<N, N, T> Rotation matrix.
         */
        inline static Matrix<N, N, T> CreateRotationY(T ry) {
            static_assert(N == 4, "Matrix must be four dimensional.");
            Matrix<N, N, T> rotation;
            rotation[0][0] = static_cast<T>(std::cos(static_cast<double>(ry)));
            rotation[0][2] = static_cast<T>(std::sin(static_cast<double>(ry)));
            rotation[1][1] = static_cast<T>(1);
            rotation[2][0] = static_cast<T>(-std::sin(static_cast<double>(ry)));
            rotation[2][2] = static_cast<T>(std::cos(static_cast<double>(ry)));
            rotation[3][3] = static_cast<T>(1);
            return rotation;
        }

        /**
         * @brief Creates a rotation matrix around the Z axis.
         * @note %Matrix must be four dimensional.
         * @param rz Rotation around the Z axis.
         * @return Matrix<N, N, T> Rotation matrix.
         */
        inline static Matrix<N, N, T> CreateRotationZ(T rz) {
            static_assert(N == 4, "Matrix must be four dimensional.");
            Matrix<N, N, T> rotation;
            rotation[0][0] = static_cast<T>(std::cos(static_cast<double>(rz)));
            rotation[0][1] = static_cast<T>(-std::sin(static_cast<double>(rz)));
            rotation[1][0] = static_cast<T>(std::sin(static_cast<double>(rz)));
            rotation[1][1] = static_cast<T>(std::cos(static_cast<double>(rz)));
            rotation[2][2] = static_cast<T>(1);
            rotation[3][3] = static_cast<T>(1);
            return rotation;
        }

        /**
         * @brief Creates a rotation matrix around an axis.
         * @note %Matrix must be four dimensional.
         * @param axis Desired axis.
         * @param r Rotation around the axis.
         * @return Matrix<N, N, T> Rotation matrix.
         */
        inline static Matrix<N, N, T> CreateRotationAxis(Vector<3, T> axis, T r) {
            static_assert(N == 4, "Matrix must be four dimensional.");
            axis.Normalize();
            T x2 = axis[0] * axis[0];
            T y2 = axis[1] * axis[1];
            T z2 = axis[2] * axis[2];
            T sinr = static_cast<T>(std::sin(static_cast<double>(r)));
            T cosr = static_cast<T>(std::cos(static_cast<double>(r)));
            Matrix<N, N, T> rotation;
            rotation[0][0] = x2 + (static_cast<T>(1) - x2) * cosr;
            rotation[0][1] = axis[0] * axis[1] * (static_cast<T>(1) - cosr) - axis[2] * sinr;
            rotation[0][2] = axis[0] * axis[2] * (static_cast<T>(1) - cosr) + axis[1] * sinr;
            rotation[1][0] = axis[0] * axis[1] * (static_cast<T>(1) - cosr) + axis[2] * sinr;
            rotation[1][1] = y2 + (static_cast<T>(1) - y2) * cosr;
            rotation[1][2] = axis[1] * axis[2] * (static_cast<T>(1) - cosr) - axis[0] * sinr;
            rotation[2][0] = axis[0] * axis[2] * (static_cast<T>(1) - cosr) - axis[1] * sinr;
            rotation[2][1] = axis[1] * axis[2] * (static_cast<T>(1) - cosr) + axis[0] * sinr;
            rotation[2][2] = z2 + (static_cast<T>(1) - z2) * cosr;
            rotation[3][3] = static_cast<T>(1);
            return rotation;
        }

        /**
         * @brief Creates a scaling matrix.
         * @note %Matrix must be four dimensional.
         * @param sx Scale on X axis.
         * @param sy Scale on Y axis.
         * @param sz Scale on Z axis.
         * @return Matrix<N, N, T> Scaling matrix.
         */
        inline static Matrix<N, N, T> CreateScaling(T sx, T sy, T sz) {
            static_assert(N == 4, "Matrix must be four dimensional.");
            Matrix<N, N, T> scale;
            scale[0][0] = sx;
            scale[1][1] = sy;
            scale[2][2] = sz;
            scale[3][3] = static_cast<T>(1);
            return scale;
        }

        /**
         * @brief Creates a scaling matrix.
         * @note %Matrix must be four dimensional.
         * @param scale Scale values.
         * @return Matrix<N, N, T> Scaling matrix.
         */
        inline static Matrix<N, N, T> CreateScaling(const Vector<3, T>& scale) {
            return CreateScaling(scale[0], scale[1], scale[2]);
        }

        /**
         * @brief Creates a look at matrix.
         * @note %Matrix must be four dimensional.
         * @param eye Position of the eye (camera).
         * @param center Position of the reference point.
         * @param up Direction of the up vector.
         * @return Matrix<N, N, T> Look at matrix.
         */
        inline static Matrix<N, N, T> CreateLookAtMatrix(const Vector<3, T>& eye, const Vector<3, T>& center, const Vector<3, T>& up) {
            static_assert(N == 4, "Matrix must be four dimensional.");
            Vector<3, T> zAxis = (eye - center).GetNormalized();
            Vector<3, T> xAxis = Vector<3, T>::CrossProduct(up, zAxis).GetNormalized();
            Vector<3, T> yAxis = Vector<3, T>::CrossProduct(zAxis, xAxis);
            Matrix<N, N, T> result;
            result[0][0] = xAxis[0];
            result[0][1] = xAxis[1];
            result[0][2] = xAxis[2];
            result[0][3] = -(xAxis * eye);
            result[1][0] = yAxis[0];
            result[1][1] = yAxis[1];
            result[1][2] = yAxis[2];
            result[1][3] = -(yAxis * eye);
            result[2][0] = zAxis[0];
            result[2][1] = zAxis[1];
            result[2][2] = zAxis[2];
            result[2][3] = -(zAxis * eye);
            result[3][3] = static_cast<T>(1);
            return result;
        }

        /**
         * @brief Creates an orthogonal projection matrix.
         * @note %Matrix must be four dimensional.
         * @param width Width of the volume display.
         * @param height Height of the volume display.
         * @param zNear Minimum z-component value of the volume.
         * @param zFar Maximum z-component value of the volume.
         * @return Matrix<N, N, T> Orthogonal projection matrix.
         */
        inline static Matrix<N, N, T> CreateOrthoProjection(T width, T height, T zNear, T zFar) {
            static_assert(N == 4, "Matrix must be four dimensional.");
            Matrix<N, N, T> result;
            result[0][0] = static_cast<T>(2) / width;
            result[1][1] = static_cast<T>(2) / height;
            result[2][2] = static_cast<T>(1) / (zNear - zFar);
            result[2][3] = zNear / (zNear - zFar);
            result[3][3] = static_cast<T>(1);
            return result;
        }

        /**
         * @brief Creates an orthogonal projection off center matrix.
         * @note %Matrix must be four dimensional.
         * @param left Left volumentric limit.
         * @param right Right volumentric limit.
         * @param bottom Bottom volumentric limit.
         * @param top Top volumentric limit.
         * @param zNear Minimum z-component value of the volume.
         * @param zFar Maximum z-component value of the volume.
         * @return Matrix<N, N, T> Orthogonal projection matrix.
         */
        inline static Matrix<N, N, T> CreateOrhtoProjectionOffCenter(T left, T right, T bottom, T top, T zNear, T zFar) {
            static_assert(N == 4, "Matrix must be four dimensional.");
            Matrix<N, N, T> result;
            result[0][0] = static_cast<T>(2) / (right - left);
            result[0][3] = (left + right) / (left - right);
            result[1][1] = static_cast<T>(2) / (top - bottom);
            result[1][3] = (top + bottom) / (bottom - top);
            result[2][2] = static_cast<T>(1) / (zNear - zFar);
            result[2][3] = zNear / (zNear - zFar);
            result[3][3] = static_cast<T>(1);
            return result;
        }

        /**
         * @brief Creates a perspective projection matrix.
         * @note %Matrix must be four dimensional.
         * @param width Width of the volume display.
         * @param height Height of the volume display.
         * @param zNear Minimum z-component value of the volume.
         * @param zFar Maximum z-component value of the volume.
         * @return Matrix<N, N, T> Perspective projection matrix.
         */
        inline static Matrix<N, N, T> CreatePerspectiveMatrix(T width, T height, T zNear, T zFar) {
            static_assert(N == 4, "Matrix must be four dimensional.");
            Matrix<N, N, T> result;
            result[0][0] = static_cast<T>(2) * zNear / width;
            result[1][1] = static_cast<T>(2) * zNear / height;
            result[2][2] = zFar / (zNear - zFar);
            result[2][3] = zNear * zFar / (zNear - zFar);
            result[3][2] = static_cast<T>(-1);
            return result;
        }

        /**
         * @brief Creates a perspective projection off center matrix.
         * @note %Matrix must be four dimensional.
         * @param left Left volumetric limit.
         * @param right Right volumentric limit.
         * @param bottom Bottom volumetric limit.
         * @param top Top volumetric limit.
         * @param zNear Minimum z-component value of the volume.
         * @param zFar Maximum z-component value of the volume.
         * @return Matrix<N, N, T> Perspective projection matrix.
         */
        inline static Matrix<N, N, T> CreatePerspectiveMatrixOffCenter(T left, T right, T bottom, T top, T zNear, T zFar) {
            static_assert(N == 4, "Matrix must be four dimensional.");
            Matrix<N, N, T> result;
            result[0][0] = static_cast<T>(2) * zNear / (right - left);
            result[0][2] = (left + right) / (right - left);
            result[1][1] = static_cast<T>(2) * zNear / (top - bottom);
            result[1][2] = (top + bottom) / (top - bottom);
            result[2][2] = zFar / (zNear - zFar);
            result[2][3] = zNear * zFar / (zNear - zFar);
            result[3][2] = static_cast<T>(-1);
            return result;
        }

        /**
         * @brief Creates a perspective projection matrix based on field of view.
         * @note %Matrix must be four dimensional.
         * @param fovy Field of view on the y-axis.
         * @param aspect Aspect ratio.
         * @param zNear Minimum z-component value of the volume.
         * @param zFar Maximum z-component value of the volume.
         * @return Matrix<N, N, T> Perspective projection matrix.
         */
        inline static Matrix<N, N, T> CreatePerspectiveMatrixFOV(T fovy, T aspect, T zNear, T zFar) {
            static_assert(N == 4, "Matrix must be four dimensional.");
            Matrix<N, N, T> result;
            T yScale = static_cast<T>(1) / std::tan(fovy / 2.0);
            T xScale = yScale / aspect;
            result[0][0] = xScale;
            result[1][1] = yScale;
            result[2][2] = zFar / (zNear - zFar);
            result[2][3] = zNear * zFar / (zNear - zFar);
            result[3][2] = static_cast<T>(-1);
            return result;
        }

    private:

        inline MatrixUtil() = delete;

    };

    /**
     * @brief Alias for single precision matrix utilities class.
     * @tparam N Number of rows and columns.
     */
    template<std::size_t N>
    using MatrixUtilf = MatrixUtil<N, float>;

    /**
     * @brief Alias for single precision 2x2 matrix utilities class.
     */
    using MatrixUtil2x2f = MatrixUtilf<2>;

    /**
     * @brief Alias for single precision 2x2 matrix utilities class.
     */
    using MatrixUtil2f = MatrixUtil2x2f;

    /**
     * @brief Alias for single precision 3x3 matrix utilities class.
     */
    using MatrixUtil3x3f = MatrixUtilf<3>;

    /**
     * @brief Alias for single precision 3x3 matrix utilities class.
     */
    using MatrixUtil3f = MatrixUtil3x3f;

    /**
     * @brief Alias for single precision 4x4 matrix utilities class.
     */
    using MatrixUtil4x4f = MatrixUtilf<4>;

    /**
     * @brief Alias for single precision 4x4 matrix utilities class.
     */
    using MatrixUtil4f = MatrixUtil4x4f;

    /**
     * @brief Alias for double precision matrix utilities class.
     * @tparam N Number of rows and columns.
     */
    template<std::size_t N>
    using MatrixUtild = MatrixUtil<N, double>;

    /**
     * @brief Alias for double precision 2x2 matrix utilities class.
     */
    using MatrixUtil2x2d = MatrixUtild<2>;

    /**
     * @brief Alias for double precision 2x2 matrix utilities class.
     */
    using MatrixUtil2d = MatrixUtil2x2d;

    /**
     * @brief Alias for double precision 3x3 matrix utilities class.
     */
    using MatrixUtil3x3d = MatrixUtild<3>;

    /**
     * @brief Alias for double precision 3x3 matrix utilities class.
     */
    using MatrixUtil3d = MatrixUtil3x3d;

    /**
     * @brief Alias for double precision 4x4 matrix utilities class.
     */
    using MatrixUtil4x4d = MatrixUtild<4>;

    /**
     * @brief Alias for double precision 4x4 matrix utilities class.
     */
    using MatrixUtil4d = MatrixUtil4x4d;

    /**
     * @brief Alias for integer matrix utilities class.
     * @tparam N Number of rows and columns.
     */
    template<std::size_t N>
    using MatrixUtili = MatrixUtil<N, int>;

    /**
     * @brief Alias for integer 2x2 matrix utilities class.
     */
    using MatrixUtil2x2i = MatrixUtili<2>;

    /**
     * @brief Alias for integer 2x2 matrix utilities class.
     */
    using MatrixUtil2i = MatrixUtil2x2i;

    /**
     * @brief Alias for integer 3x3 matrix utilities class.
     */
    using MatrixUtil3x3i = MatrixUtili<3>;

    /**
     * @brief Alias for integer 3x3 matrix utilities class.
     */
    using MatrixUtil3i = MatrixUtil3x3i;

    /**
     * @brief Alias for integer 4x4 matrix utilities class.
     */
    using MatrixUtil4x4i = MatrixUtili<4>;

    /**
     * @brief Alias for integer 4x4 matrix utilities class.
     */
    using MatrixUtil4i = MatrixUtil4x4i;

} }

#endif
