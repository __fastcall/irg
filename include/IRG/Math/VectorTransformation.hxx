#ifndef __IRG_MATH_VECTORTRANSFORMATION_HXX__
#define __IRG_MATH_VECTORTRANSFORMATION_HXX__

#include "Vector.hxx"
#include "Matrix.hxx"

namespace IRG { namespace Math {

    /**
     * @brief Multiplies vector with a matrix.
     * @details %Vector is treated as a column matrix.
     * @tparam N %Vector and matrix dimension.
     * @tparam T Component and element type.
     * @param vector %Vector operand.
     * @param matrix %Matrix operand.
     * @return Vector<N, T>& Resulting vector.
     */
    template<std::size_t N, typename T>
    inline Vector<N, T>& operator*=(Vector<N, T>& vector, const Matrix<N, N, T>& matrix) {
        Vector<N, T> self = vector;
        for (std::size_t j = 0; j < N; ++j) {
            vector[j] = static_cast<T>(0);
            for (std::size_t k = 0; k < N; ++k) {
                vector[j] += self[k] * matrix[k][j];
            }
        }
        return vector;
    }

    /**
     * @brief Multiplies vector with a matrix.
     * @details %Vector is treated as a column matrix.
     * @tparam N %Vector and matrix dimension.
     * @tparam T Component and element type.
     * @param vector %Vector operand.
     * @param matrix %Matrix operand.
     * @return Vector<N, T> Resulting vector.
     */
    template<std::size_t N, typename T>
    inline Vector<N, T> operator*(Vector<N, T> vector, const Matrix<N, N, T>& matrix) {
        vector *= matrix;
        return vector;
    }

    /**
     * @brief Multiplies matrix with a vector.
     * @details %Vector is treated as a row matrix.
     * @tparam N %Vector and matrix dimension.
     * @tparam T Component and element type.
     * @param matrix %Matrix operand.
     * @param vector %Vector operand.
     * @return Vector<N, T> Resulting vector.
     */
    template<std::size_t N, typename T>
    inline Vector<N, T> operator*(const Matrix<N, N, T>& matrix, const Vector<N, T>& vector) {
        Vector<N, T> result = vector;
        for (std::size_t i = 0; i < N; ++i) {
            result[i] = static_cast<T>(0);
            for (std::size_t k = 0; k < N; ++k) {
                result[i] += matrix[i][k] * vector[k];
            }
        }
        return result;
    }

} }

#endif
