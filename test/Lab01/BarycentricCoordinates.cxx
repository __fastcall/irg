#include <IRG/Math.hxx>
#include <cassert>
#include <cmath>
#include <iostream>

using namespace IRG::Math;

int main() {

    Vector3d a("1 0 0");
    Vector3d b("5 0 0");
    Vector3d c("3 8 0");

    Vector3d t("3 4 0");

    double pov = Vector3d::CrossProduct(b - a, c - a).GetLength() / 2.0;
    double povA = Vector3d::CrossProduct(b - t, c - t).GetLength() / 2.0;
    double povB = Vector3d::CrossProduct(a - t, c - t).GetLength() / 2.0;
    double povC = Vector3d::CrossProduct(a - t, b - t).GetLength() / 2.0;

    double t1 = povA / pov;
    double t2 = povB / pov;
    double t3 = povC / pov;

    std::cout << "Baricentricne koordinate su: (" << t1 << "," << t2 << "," << t3 << ")";

    assert(
        std::fabs(t1 - 0.25) < 1e-6 &&
        std::fabs(t2 - 0.25) < 1e-6 &&
        std::fabs(t3 - 0.5) < 1e-6
    );

    return 0;

}
