#include <IRG/Math.hxx>
#include <cassert>
#include <cmath>
#include <iostream>

using namespace IRG::Math;

int main() {

    Matrix3d a("1 5 3 | 0 0 8 | 1 1 1");
    Vector3d r("3 4 1");

    Vector3d v = a.GetInverse() * r;

    std::cout << "Rjesenje sustava je: " << v;

    assert(
        std::fabs(v[0] - 0.25) < 1e-6 &&
        std::fabs(v[1] - 0.25) < 1e-6 &&
        std::fabs(v[2] - 0.5) < 1e-6
    );

    return 0;

}
