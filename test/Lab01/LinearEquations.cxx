#include <IRG/Math.hxx>
#include <cassert>
#include <cmath>
#include <iostream>

using namespace IRG::Math;

int main() {
    
    Matrix2d a("3 5 | 2 10");
    Vector2d r("2 8");

    Vector2d v = a.GetInverse() * r;

    std::cout << "Rjesenje sustava je: " << v;

    assert(
        std::fabs(v[0] - (-1.0)) < 1e-6 &&
        std::fabs(v[1] - 1.0) < 1e-6
        );

    return 0;

}
