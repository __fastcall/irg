#include <IRG/Math.hxx>
#include <cassert>

using namespace IRG::Math;

static void VectorComparatorTests() {

    /* Initialize vectors. */
    double rawData[] { 1.0, -3.01, -5.1, 14.3 };
    Vector4d a(rawData);
    Vector4d b(rawData);
    Vector4d c(rawData);
    b[0] = 1.00000006431;
    b[2] = -5.100000601;
    c[1] = -3.0155;

    /* AreEqual() method test. */
    bool d = VectorComparator4d::AreEqual(a, b);
    bool e = VectorComparator4d::AreEqual(a, c);
    assert(d && !e);

    /* AreNotEqual() method test. */
    bool f = VectorComparator4d::AreNotEqual(a, b);
    bool g = VectorComparator4d::AreNotEqual(a, c);
    assert(!f && g);

}

static void MatrixComparatorTests() {

    /* Initialize matrices. */
    double rawData[] = {
        1.0, 2.0,
        3.0, 4.0
    };
    Matrix2d a(rawData);
    Matrix2d b(rawData);
    Matrix2d c(rawData);
    b[0][1] = 2.0000006;
    c[1][0] = 3.00015;

    /* AreEqual() method test. */
    bool d = MatrixComparator2d::AreEqual(a, b);
    bool e = MatrixComparator2d::AreEqual(a, c);
    assert(d && !e);

    /* AreNotEqual() method test. */
    bool f = MatrixComparator2d::AreNotEqual(a, b);
    bool g = MatrixComparator2d::AreNotEqual(a, c);
    assert(!f && g);

}

int main() {

    /* Execute vector comparator tests. */
    VectorComparatorTests();

    /* Execute matrix comparator tests. */
    MatrixComparatorTests();

    return 0;

}