#include <IRG/Math/VectorTransformation.hxx>
#include <cassert>
#include <cmath>

using namespace IRG::Math;

static void VectorTransformationAssignmentTests() {

    /* Initialize vector and matrix. */
    double aRawData[] { 1.2, 3.5, 6.2, -5.3, 3.1 };
    double bRawData[] {
        1.56, -53.4, 4.1, 4.3, 1.1,
        -0.1, -0.2, 3.3, 1.5, 8.3,
        1.1, 64.2, 2.1, 75.3, 884.01,
        -62.2, -0.221, 3.5, 442.2, 7.5,
        21.21, -7.53, 2.2, 5.11, 0.63
    };
    Vectord<5> a(aRawData);
    Matrixd<5, 5> b(bRawData);

    /* Multiplication assignment operator test. */
    a *= b;
    assert(
        std::fabs(a[0] - 403.753) < 1e-6 &&
        std::fabs(a[1] - 311.0883) < 1e-6 &&
        std::fabs(a[2] - 17.76) < 1e-6 &&
        std::fabs(a[3] - (-1850.549)) < 1e-6 &&
        std::fabs(a[4] - 5473.435) < 1e-6
        );

}

static void VectorTransformationArithmeticTests() {

    /* Initialize vector and matrix. */
    int aRawData[] = { -1, 2, 7, -4 };
    int bRawData[] = {
        1, 76, 32, 15,
        85, 22, 14, -43,
        -5, -2, -626, 2,
        26, 22, -6, -69
    };
    Vector4i a(aRawData);
    Matrix4i b(bRawData);

    /* Vector-matrix multiplication operator test. */
    Vector4i c = a * b;
    assert(c[0] == 30 && c[1] == -134 && c[2] == -4362 && c[3] == 189);

    /* Matrix-vector multiplication operator test. */
    Vector4i d = b * a;
    assert(d[0] == 315 && d[1] == 229 && d[2] == -4389 && d[3] == 252);

}

int main() {

    /* Execute vector transformation assignment tests. */
    VectorTransformationAssignmentTests();

    /* Execute vector transformation arithmetic tests. */
    VectorTransformationArithmeticTests();

    return 0;

}
