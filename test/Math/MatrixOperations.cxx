#include <IRG/Math/Matrix.hxx>
#include <cassert>
#include <cmath>
#include <string>
#include <sstream>

using namespace IRG::Math;

static void MatrixConstructorTests() {

    /* Value constructor test. */
    Matrix2f a(1.2f);
    assert(
        a[0][0] == 1.2f && a[0][1] == 1.2f &&
        a[1][0] == 1.2f && a[1][1] == 1.2f
        );

    /* Initializer list constructor test. */
    Matrix2f inl({1.1f, -5.3f, -2.1f});
    assert(
        inl[0][0] == 1.1f && inl[0][1] == -5.3f &&
        inl[1][0] == -2.1f && inl[1][1] == 0.0f
        );

    /* Copy constructor test. */
    a[0][1] = 2.0f;
    Matrix2f b(a);
    assert(
        b[0][0] == 1.2f && b[0][1] == 2.0f &&
        b[1][0] == 1.2f && b[1][1] == 1.2f
        );

    /* Partial copy constructor test with expansion. */
    Matrix3x4f c(b, -1.0f);
    assert(
        c[0][0] == 1.2f && c[0][1] == 2.0f && c[0][2] == -1.0f && c[0][3] == -1.0f && 
        c[1][0] == 1.2f && c[1][1] == 1.2f && c[1][2] == -1.0f && c[1][3] == -1.0f &&
        c[2][0] == -1.0f && c[2][1] == -1.0f && c[2][2] == -1.0f && c[2][3] == -1.0f
        );

    /* Partial copy constructor test with reduction. */
    Matrix2x3f d(c);
    assert(
        d[0][0] == 1.2f && d[0][1] == 2.0f && d[0][2] == -1.0f &&
        d[1][0] == 1.2f && d[1][1] == 1.2f && d[1][2] == -1.0f
        );

    /* Partial copy constructor test with expansion and reduction. */
    Matrix3x2f e(d, 0.5f);
    assert(
        e[0][0] == 1.2f && e[0][1] == 2.0f &&
        e[1][0] == 1.2f && e[1][1] == 1.2f &&
        e[2][0] == 0.5f && e[2][1] == 0.5f
        );

    /* String constructor test. */
    Matrix4x2i f("[ 1 2 | 3 4 | 5 6 | 7 8 ]");
    assert(
        f[0][0] == 1 && f[0][1] == 2 &&
        f[1][0] == 3 && f[1][1] == 4 &&
        f[2][0] == 5 && f[2][1] == 6 &&
        f[3][0] == 7 && f[3][1] == 8
        );

    /* Raw data constructor test. */
    int rawData[] = {
        1, -2, 7,
        -4, 5, 9
    };
    Matrix2x3i g(rawData);
    assert(
        g[0][0] == 1 && g[0][1] == -2 && g[0][2] == 7 &&
        g[1][0] == -4 && g[1][1] == 5 && g[1][2] == 9
        );

}

static void MatrixAssignmentTests() {

    /* Initialize matrix. */
    int rawData[] = {
        1, 6, -4,
        -6, 7, 4,
        2, 5, 7
    };
    Matrix3i a(rawData);

    /* Matrix copy assignment operator. */
    Matrix3i b = a;
    assert(
        b[0][0] == 1 && b[0][1] == 6 && b[0][2] == -4 &&
        b[1][0] == -6 && b[1][1] == 7 && b[1][2] == 4 &&
        b[2][0] == 2 && b[2][1] == 5 && b[2][2] == 7
        );
    
    /* Addition assignment operator test. */
    b += Matrix3i(5);
    assert(
        b[0][0] == 6 && b[0][1] == 11 && b[0][2] == 1 &&
        b[1][0] == -1 && b[1][1] == 12 && b[1][2] == 9 &&
        b[2][0] == 7 && b[2][1] == 10 && b[2][2] == 12
        );

    /* Subtraction assignment operator test. */
    b -= Matrix3i(2);
    assert(
        b[0][0] == 4 && b[0][1] == 9 && b[0][2] == -1 &&
        b[1][0] == -3 && b[1][1] == 10 && b[1][2] == 7 &&
        b[2][0] == 5 && b[2][1] == 8 && b[2][2] == 10
        );

    /* Matrix multiplication assignment operator test. */
    Matrix3i c(1);
    c[0][1] = 2;
    c[1][2] = 19;
    c[2][1] = -7;
    b *= c;
    assert(
        b[0][0] == 12 && b[0][1] == 24 && b[0][2] == 174 &&
        b[1][0] == 14 && b[1][1] == -45 && b[1][2] == 194 &&
        b[2][0] == 23 && b[2][1] == -52 && b[2][2] == 167
        );

    /* Scalar multiplication assignment operator test. */
    b *= 12;
    assert(
        b[0][0] == 144 && b[0][1] == 288 && b[0][2] == 2088 &&
        b[1][0] == 168 && b[1][1] == -540 && b[1][2] == 2328 &&
        b[2][0] == 276 && b[2][1] == -624 && b[2][2] == 2004
        );

    /* Scalar division assignment operator test. */
    b /= 3;
    assert(
        b[0][0] == 48 && b[0][1] == 96 && b[0][2] == 696 &&
        b[1][0] == 56 && b[1][1] == -180 && b[1][2] == 776 &&
        b[2][0] == 92 && b[2][1] == -208 && b[2][2] == 668
        );

}

static void MatrixArithmeticTests() {

    /* Initialize matrices. */
    int aRawData[] = {
        1, 2, 3,
        4, 5, 6
    };
    int bRawData[] = {
        1, 4,
        6, 7,
        -1, 3
    };
    int cRawData[] = {
        -1, -2,
        -3, 4,
        -9, 11
    };
    Matrix2x3i a(aRawData);
    Matrix3x2i b(bRawData);
    Matrix3x2i c(cRawData);

    /* Unary plus operator test. */
    Matrix3x2i d = +b;
    assert(
        d[0][0] == 1 && d[0][1] == 4 &&
        d[1][0] == 6 && d[1][1] == 7 &&
        d[2][0] == -1 && d[2][1] == 3
        );
    
    /* Unary minus operator test. */
    Matrix3x2i e = -c;
    assert(
        e[0][0] == 1 && e[0][1] == 2 &&
        e[1][0] == 3 && e[1][1] == -4 &&
        e[2][0] == 9 && e[2][1] == -11
        );

    /* Addition operator test. */
    Matrix3x2i f = b + c;
    assert(
        f[0][0] == 0 && f[0][1] == 2 &&
        f[1][0] == 3 && f[1][1] == 11 &&
        f[2][0] == -10 && f[2][1] == 14
        );

    /* Subtraction operator test. */
    Matrix3x2i g = b - c;
    assert(
        g[0][0] == 2 && g[0][1] == 6 &&
        g[1][0] == 9 && g[1][1] == 3 &&
        g[2][0] == 8 && g[2][1] == -8
        );

    /* Matrix-matrix multiplication operator test. */
    Matrix2i h = a * b;
    assert(
        h[0][0] == 10 && h[0][1] == 27 &&
        h[1][0] == 28 && h[1][1] == 69
        );

    /* Matrix-scalar multiplication operator test. */
    Matrix2i i = h * 5;
    assert(
        i[0][0] == 50 && i[0][1] == 135 &&
        i[1][0] == 140 && i[1][1] == 345
        );

    /* Scalar-matrix multiplication operator test. */
    Matrix2i j = 2 * i;
    assert(
        j[0][0] == 100 && j[0][1] == 270 &&
        j[1][0] == 280 && j[1][1] == 690
        );

    /* Matrix-scalar division operator test. */
    Matrix2i k = j / 10;
    assert(
        k[0][0] == 10 && k[0][1] == 27 &&
        k[1][0] == 28 && k[1][1] == 69
        );

    /* Scalar-matrix division operator test. */
    Matrix2i l = 521640 / k;
    assert(
        l[0][0] == 52164 && l[0][1] == 19320 &&
        l[1][0] == 18630 && l[1][1] == 7560
        );

}

static void MatrixComparisonTests() {

    /* Initialize matrices. */
    int aRawData[] = {
        1, 2,
        3, 4
    };
    int bRawData[] = {
        1, 8,
        -3, 4
    };
    Matrix2i a(aRawData);
    Matrix2i b(bRawData);
    Matrix2i c(a);

    /* Equality comparison operator test. */
    bool d = (a == b);
    bool e = (a == c);
    assert(!d && e);

    /* Inequality comparison operator test. */
    bool f = (a != b);
    bool g = (a != c);
    assert(f && !g);

}

static void MatrixMemberAccessTests() {

    /* Initialize matrix. */
    int rawData[] = {
        1, 7,
        -4, 15
    };
    Matrix2i a(rawData);

    /* Immutable member access operator test. */
    int v00 = a[0][0], v01 = a[0][1], v10 = a[1][0], v11 = a[1][1];
    assert(
        v00 == 1 && v01 == 7 &&
        v10 == -4 && v11 == 15
        );

    /* Mutable member access operator test. */
    a[0][0] = 11, a[0][1] = 12, a[1][0] = 13, a[1][1] = 14;
    v00 = a[0][0], v01 = a[0][1], v10 = a[1][0], v11 = a[1][1];
    assert(
        v00 == 11 && v01 == 12 &&
        v10 == 13 && v11 == 14
        );

}

static void MatrixStreamOperatorTests() {

    /* Initialize string streams. */
    std::string aStr = " [   1 2    -13| -11 5   -4   |11 5  2]";
    std::string bStr = " 1   3  65 | -1  3         11     |6 7  -11   ";
    std::string cStr = "  [ 111  222  3333|   555  666 444 1 2    3    ";
    std::string dStr = " 1   3   2    6  9   113   41  55  64 ]";
    std::istringstream a(aStr);
    std::istringstream b(bStr);
    std::istringstream c(cStr);
    std::istringstream d(dStr);

    /* Scan matrix in regular form test. */
    Matrix3i e;
    a >> e;
    assert(
        e[0][0] == 1 && e[0][1] == 2 && e[0][2] == -13 &&
        e[1][0] == -11 && e[1][1] == 5 && e[1][2] == -4 &&
        e[2][0] == 11 && e[2][1] == 5 && e[2][2] == 2
        );

    /* Scan matrix in irregular forms test. */
    b >> e;
    assert(
        e[0][0] == 1 && e[0][1] == 3 && e[0][2] == 65 &&
        e[1][0] == -1 && e[1][1] == 3 && e[1][2] == 11 &&
        e[2][0] == 6 && e[2][1] == 7 && e[2][2] == -11
        );
    c >> e;
    assert(
        e[0][0] == 111 && e[0][1] == 222 && e[0][2] == 3333 &&
        e[1][0] == 555 && e[1][1] == 666 && e[1][2] == 444 &&
        e[2][0] == 1 && e[2][1] == 2 && e[2][2] == 3
        );
    d >> e;
    assert(
        e[0][0] == 1 && e[0][1] == 3 && e[0][2] == 2 &&
        e[1][0] == 6 && e[1][1] == 9 && e[1][2] == 113 &&
        e[2][0] == 41 && e[2][1] == 55 && e[2][2] == 64
        );

    /* Print matrix test. */
    std::ostringstream f;
    f << e;
    std::string fStr = f.str();
    assert(fStr == "[ 1 3 2 | 6 9 113 | 41 55 64 ]");

}

static void MatrixConversionOperatorTests() {

    /* String conversion operator test. */
    Matrix4i a(51);
    std::string b = a;
    assert(b == "[ 51 51 51 51 | 51 51 51 51 | 51 51 51 51 | 51 51 51 51 ]");

}

static void MatrixMethodTests() {

    /* Initialize matrices. */
    double aRawData[] = {
        1.186, 2.75,
        -5.3, 15.3
    };
    double bRawData[] = {
        5.3, -6.4, 23.5,
        11.0, 35.13, -7.4,
        9.7, 5.3, -5.6
    };
    double cRawData[] = {
        1.1, 2.2, 3.5, 3.1,
        3.7, 5.2, 5.1, -5.1,
        3.5, 0.2, -1.3, 1.16,
        -75.2, -2.5, 1.1, 0.4
    };
    Matrix2d a(aRawData);
    Matrix3d b(bRawData);
    Matrix4d c(cRawData);
    Matrixd<5, 5> d(c, 1.5);
    d[4][4] = 1.21;

    /* GetNumberOfRows() method test. */
    assert(a.GetNumberOfRows() == 2 && b.GetNumberOfRows() == 3 && c.GetNumberOfRows() == 4);

    /* GetNumberOfColumns() method test. */
    assert(a.GetNumberOfColumns() == 2 && b.GetNumberOfColumns() == 3 && c.GetNumberOfColumns() == 4);

    /* GetDeterminant() method test. */
    assert(
        std::fabs(a.GetDeterminant() - 32.720799999999999999) < 1e-6 &&
        std::fabs(b.GetDeterminant() - (-7407.4738999999999998)) < 1e-6 &&
        std::fabs(c.GetDeterminant() - (-3319.9002399999999988)) < 1e-6 &&
        std::fabs(d.GetDeterminant() - 826.40930959999999814) < 1e-6
        );

    /* GetInverse() method test. */
    Matrix2d e = a.GetInverse();
    Matrix3d f = b.GetInverse();
    assert(
        std::fabs(e[0][0] - 0.46759247940148162638) < 1e-6 &&
        std::fabs(e[0][1] - (-0.084044399892423168137)) < 1e-6 &&
        std::fabs(e[1][0] - 0.16197647979267010586) < 1e-6 &&
        std::fabs(e[1][1] - 0.036246057553605046332) < 1e-6
        );
    assert(
        std::fabs(f[0][0] - 0.02126338912918748186) < 1e-6 &&
        std::fabs(f[0][1] - (-0.011975742499747451014)) < 1e-6 &&
        std::fabs(f[0][2] - 0.10505538197036374302) < 1e-6 &&
        std::fabs(f[1][0] - 0.001374287663706786731) < 1e-6 &&
        std::fabs(f[1][1] - 0.034779737799683641138	) < 1e-6 &&
        std::fabs(f[1][2] - (-0.040191839217955260022)) < 1e-6 &&
        std::fabs(f[2][0] - 0.038131892709065097072	) < 1e-6 &&
        std::fabs(f[2][1] - 0.012172840730495182709) < 1e-6 &&
        std::fabs(f[2][2] - (-0.034639204061184744774)) < 1e-6
        );

    /* Invert() method test. */
    c.Invert();
    assert(
        std::fabs(c[0][0] - 0.00273628703975755613) < 1e-6 &&
        std::fabs(c[0][1] - (-0.00716111879313578405)) < 1e-6 &&
        std::fabs(c[0][2] - (-0.03356486398519010911)) < 1e-6 &&
        std::fabs(c[0][3] - (-0.015172383613550990312)) < 1e-6 &&
        std::fabs(c[1][0] - 0.0061063883052100381) < 1e-6 &&
        std::fabs(c[1][1] - 0.18172491833670279211	) < 1e-6 &&
        std::fabs(c[1][2] - 0.76721702938881079136) < 1e-6 &&
        std::fabs(c[1][3] - 0.044738814200031504565) < 1e-6 &&
        std::fabs(c[2][0] - 0.14516683188046638428) < 1e-6 &&
        std::fabs(c[2][1] - (-0.05187306471594459727)) < 1e-6 &&
        std::fabs(c[2][2] - (-0.60613267102266904261)) < 1e-6 &&
        std::fabs(c[2][3] - (-0.028639776236167867506)) < 1e-6 &&
        std::fabs(c[3][0] - 0.15337810271070072875) < 1e-6 &&
        std::fabs(c[3][1] - (-0.067858665536287319276)) < 1e-6 &&
        std::fabs(c[3][2] - 0.15177684977666678318) < 1e-6 &&
        std::fabs(c[3][3] - 0.0059688540520723598603) < 1e-6
        );

    /* GetTransposed() method test. */
    Matrix3x2d g(a, 1.25);
    Matrix2x3d h = g.GetTransposed();
    assert(
        std::fabs(h[0][0] - 1.186) < 1e-6 &&
        std::fabs(h[0][1] - -5.3) < 1e-6 &&
        std::fabs(h[0][2] - 1.25) < 1e-6 &&
        std::fabs(h[1][0] - 2.75) < 1e-6 &&
        std::fabs(h[1][1] - 15.3) < 1e-6 &&
        std::fabs(h[1][2] - 1.25) < 1e-6
        );

    /* Transpose() method test. */
    a.Transpose();
    assert(
        std::fabs(a[0][0] - 1.186) < 1e-6 &&
        std::fabs(a[0][1] - -5.3) < 1e-6 &&
        std::fabs(a[1][0] - 2.75) < 1e-6 &&
        std::fabs(a[1][1] - 15.3) < 1e-6
        );

    /* GetMinor(...) method test. */
    double i = a.GetMinor(0, 1);
    assert(std::fabs(i - 2.75) < 1e-6);
    double j = b.GetMinor(2, 1);
    assert(std::fabs(j - (-297.72)) < 1e-6);

    /* GetRawData() method test. */
    const double* rawData = a.GetRawData();
    Matrix2d k(rawData);
    assert(a == k);

}

int main() {

    /* Execute matrix constructor tests. */
    MatrixConstructorTests();

    /* Execute matrix assignment tests. */
    MatrixAssignmentTests();

    /* Execute matrix arithmetic tests. */
    MatrixArithmeticTests();

    /* Execute matrix comparison tests. */
    MatrixComparisonTests();

    /* Execute matrix member access tests. */
    MatrixMemberAccessTests();

    /* Execute matrix stream operator tests. */
    MatrixStreamOperatorTests();

    /* Execute matrix conversion operator tests. */
    MatrixConversionOperatorTests();

    /* Execute matrix method tests. */
    MatrixMethodTests();

    return 0;
    
}
