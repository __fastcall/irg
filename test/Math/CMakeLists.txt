# Build tests if option is ticked
if(BUILD_TESTS)

    # Add vector operations test
    add_executable(VectorOperations ${CMAKE_CURRENT_SOURCE_DIR}/VectorOperations.cxx)
    add_test(VectorOperations VectorOperations)

    # Add matrix operations test
    add_executable(MatrixOperations ${CMAKE_CURRENT_SOURCE_DIR}/MatrixOperations.cxx)
    add_test(MatrixOperations MatrixOperations)

    # Add vector transformations test
    add_executable(VectorTransformations ${CMAKE_CURRENT_SOURCE_DIR}/VectorTransformations.cxx)
    add_test(VectorTransformations VectorTransformations)

    # Add vector utilities test
    add_executable(VectorUtilities ${CMAKE_CURRENT_SOURCE_DIR}/VectorUtilities.cxx)
    add_test(VectorUtilities VectorUtilities)

    # Add matrix utilities test
    add_executable(MatrixUtilities ${CMAKE_CURRENT_SOURCE_DIR}/MatrixUtilities.cxx)
    add_test(MatrixUtilities MatrixUtilities)

    # Add comparators test
    add_executable(Comparators ${CMAKE_CURRENT_SOURCE_DIR}/Comparators.cxx)
    add_test(Comparators Comparators)

endif()
