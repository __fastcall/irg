#include <IRG/Math.hxx>
#include <cassert>
#include <cmath>

using namespace IRG::Math;

static void MatrixConstructorTests() {

    /* Initialize vectors. */
    double rawData[] = {
        -1.12, -5.273, 1.32, 1.0
    };
    Vector4d a(rawData);

    /* Identity matrix construction test. */
    Matrix4d b = MatrixUtil4d::CreateIdentity();
    Vector4d c = b * a;
    assert(
        std::fabs(c[0] - (-1.12)) < 1e-6 &&
        std::fabs(c[1] - (-5.273)) < 1e-6 &&
        std::fabs(c[2] - (1.32)) < 1e-6 &&
        std::fabs(c[3] - (1.0)) < 1e-6
        );

    /* Translation matrix construction test. */
    Matrix4d d = MatrixUtil4d::CreateTranslation(69.0, 1.2, -6.2);
    Vector4d e = d * c;
    assert(
        std::fabs(e[0] - 67.88) < 1e-6 &&
        std::fabs(e[1] - (-4.073)) < 1e-6 &&
        std::fabs(e[2] - (-4.88)) < 1e-6 &&
        std::fabs(e[3] - (1.0)) < 1e-6
        );

    Matrix4d d2 = MatrixUtil4d::CreateTranslation(Vector3d({69.0, 1.2, -6.2}));
    Vector4d e2 = d2 * c;
    assert(
        std::fabs(e2[0] - 67.88) < 1e-6 &&
        std::fabs(e2[1] - (-4.073)) < 1e-6 &&
        std::fabs(e2[2] - (-4.88)) < 1e-6 &&
        std::fabs(e2[3] - (1.0)) < 1e-6
        );

    /* Rotation X matrix construction test. */
    Matrix4d f = MatrixUtil4d::CreateRotationX(DEGREES(67.0));
    Vector4d g = f * e;
    assert(
        std::fabs(g[0] - 67.88) < 1e-6 &&
        std::fabs(g[1] - 2.900616) < 1e-6 &&
        std::fabs(g[2] - (-5.655984)) < 1e-6 &&
        std::fabs(g[3] - (1.0)) < 1e-6
        );

    /* Rotation Y matrix construction test. */
    Matrix4d h = MatrixUtil4d::CreateRotationY(DEGREES(-12.4));
    Vector4d i = g * h.GetTransposed();
    assert(
        std::fabs(i[0] - 67.511054) < 1e-6 &&
        std::fabs(i[1] - 2.900616) < 1e-6 &&
        std::fabs(i[2] - 9.052191) < 1e-6 &&
        std::fabs(i[3] - (1.0)) < 1e-6
        );

    /* Rotation Z matrix construction test. */
    Matrix4d j = MatrixUtil4d::CreateRotationZ(DEGREES(-83.0));
    Vector4d k = j * i;
    assert(
        std::fabs(k[0] - 11.106523) < 1e-6 &&
        std::fabs(k[1] - (-66.654341)) < 1e-6 &&
        std::fabs(k[2] - 9.052191) < 1e-6 &&
        std::fabs(k[3] - (1.0)) < 1e-6
        );

    /* Rotation axis matrix construction test. */
    Matrix4d axisMat = MatrixUtil4d::CreateRotationAxis(Vector3d({-4.1, 1.5, 0.12}), DEGREES(-124.53));
    Vector4d axisVec = axisMat * k;
    assert(
        std::fabs(axisVec[0] - 38.277364) < 1e-6 &&
        std::fabs(axisVec[1] - 12.733738) < 1e-6 &&
        std::fabs(axisVec[2] - (-54.961746)) < 1e-6 &&
        std::fabs(axisVec[3] - 1.0) < 1e-6
        );

    /* Scaling matrix construction test. */
    Matrix4d l = MatrixUtil4d::CreateScaling(3.094, -2.1, 1.12);
    Vector4d m = l * k;
    assert(
        std::fabs(m[0] - 34.3635822) < 1e-6 &&
        std::fabs(m[1] - 139.974116) < 1e-6 &&
        std::fabs(m[2] - 10.1384539) < 1e-6 &&
        std::fabs(m[3] - (1.0)) < 1e-6
        );

    /* Scaling matrix construction test. */
    Matrix4d l2 = MatrixUtil4d::CreateScaling(Vector3d({3.094, -2.1, 1.12}));
    Vector4d m2 = l2 * k;
    assert(
        std::fabs(m2[0] - 34.3635822) < 1e-6 &&
        std::fabs(m2[1] - 139.974116) < 1e-6 &&
        std::fabs(m2[2] - 10.1384539) < 1e-6 &&
        std::fabs(m2[3] - (1.0)) < 1e-6
        );

    /* Look at matrix construction test. */
    Matrix4d lam = MatrixUtil4d::CreateLookAtMatrix({0.3, -1.1, 31.125}, {12.0, -24.0, -52.531}, {0.0, -4.0, 13.0});
    Vector4d lamv({1.12, -1.32, 15.0, 1.0});
    Vector4d lamt = lam * lamv;
    assert(
        std::fabs(lamt[0] - (0.4134801626)) < 1e-6 &&
        std::fabs(lamt[1] - (-4.220994949)) < 1e-6 &&
        std::fabs(lamt[2] - (-15.58039284)) < 1e-6 &&
        std::fabs(lamt[3] - 1.0) < 1e-6
        );

    /* Orthogonal projection construction test. */
    Matrix4d opm = MatrixUtil4d::CreateOrthoProjection(20.52, 19.02, 0.15, 31.42);
    Vector4d opmv({13.52, -12.35, -74.12, 1.0});
    Vector4d opmt = opm * opmv;
    assert(
        std::fabs(opmt[0] - (1.317738891)) < 1e-6 &&
        std::fabs(opmt[1] - (-1.298633099)) < 1e-6 &&
        std::fabs(opmt[2] - (2.365526199)) < 1e-6 &&
        std::fabs(opmt[3] - 1.0) < 1e-6
        );

    /* Orthogonal projection off center construction test. */
    Matrix4d opm2 = MatrixUtil4d::CreateOrhtoProjectionOffCenter(-12.2, 23.4, -17.3, 63.213, 0.06, 37.2);
    Vector4d opmv2({85.214, -262.1, -15.58, 1.0});
    Vector4d opmt2 = opm2 * opmv2;
    assert(
        std::fabs(opmt2[0] - (4.472696781)) < 1e-6 &&
        std::fabs(opmt2[1] - (-7.081005573)) < 1e-6 &&
        std::fabs(opmt2[2] - (0.4178783298)) < 1e-6 &&
        std::fabs(opmt2[3] - 1.0) < 1e-6
        );

    /* Perspective projection construction test. */
    Matrix4d ppm = MatrixUtil4d::CreatePerspectiveMatrix(351.53, 835.1, 0.123, 35.1);
    Vector4d ppmv({124.5, -23.12, -51.1254, 1.0});
    Vector4d ppmt = ppm * ppmv;
    assert(
        std::fabs(ppmt[0] - (0.08712485433)) < 1e-6 &&
        std::fabs(ppmt[1] - (-0.006810585503)) < 1e-6 &&
        std::fabs(ppmt[2] - (51.18175507)) < 1e-6 &&
        std::fabs(ppmt[3] - (51.12540054)) < 1e-6
        );

    /* Perspective projection off center construction test. */
    Matrix4d ppm2 = MatrixUtil4d::CreatePerspectiveMatrixOffCenter(-23.12, 31.4, -64.2, 145.1, 3.12, 531.12);
    Vector4d ppmv2({76.36, 256.6, -35.3, 1.0});
    Vector4d ppmt2 = ppm2 * ppmv2;
    assert(
        std::fabs(ppmt2[0] - (3.378621101)) < 1e-6 &&
        std::fabs(ppmt2[1] - (-5.994199753)) < 1e-6 &&
        std::fabs(ppmt2[2] - (32.37015533)) < 1e-6 &&
        std::fabs(ppmt2[3] - (35.29999924)) < 1e-6
        );

    /* Perspective projection FOV construction test. */
    Matrix4d ppm3 = MatrixUtil4d::CreatePerspectiveMatrixFOV(52.112, 1.23, 0.121, 1551.12);
    Vector4d ppmv3({12.7, 63.1, -21.6, 1.0});
    Vector4d ppmt3 = ppm3 * ppmv3;
    assert(
        std::fabs(ppmt3[0] - (7.809220791)) < 1e-6 &&
        std::fabs(ppmt3[1] - (47.72417831)) < 1e-6 &&
        std::fabs(ppmt3[2] - (21.48067474)) < 1e-6 &&
        std::fabs(ppmt3[3] - (21.60000038)) < 1e-6
        );

    

}

int main() {

    /* Execute matrix constructor tests. */
    MatrixConstructorTests();

    return 0;

}
