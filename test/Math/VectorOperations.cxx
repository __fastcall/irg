#include <IRG/Math/Vector.hxx>
#include <cassert>
#include <cmath>
#include <string>
#include <sstream>

using namespace IRG::Math;

static void VectorConstructorTests() {

    /* Value constructor test. */
    Vector4f a(12.3f);
    assert(a[0] == 12.3f && a[1] == 12.3f && a[2] == 12.3f && a[3] == 12.3f);

    /* Initializer list constructor test. */
    Vector4d inl({1.1, -5.3, -2.1});
    assert(inl[0] == 1.1 && inl[1] == -5.3 && inl[2] == -2.1 && inl[3] == 0.0);

    /* Copy constructor test. */
    Vector4f b(a);
    assert(b[0] == 12.3f && b[1] == 12.3f && b[2] == 12.3f && b[3] == 12.3f);

    /* Partial copy constructor test without expansion. */
    Vector2f c(b);
    assert(c[0] == 12.3f && c[1] == 12.3f);

    /* Partial copy constructor test with expansion. */
    Vector3f d(c, -1.0f);
    assert(d[0] == 12.3f && d[1] == 12.3f && d[2] == -1.0f);

    /* String constructor test.*/
    Vector3i e("[1 3 4]");
    assert(e[0] == 1 && e[1] == 3 && e[2] == 4);

    /* Raw data constructor test. */
    float rawData[] = { 1.0f, 2.0f, 3.0f };
    Vector3f f(rawData);
    assert(f[0] == 1.0f && f[1] == 2.0f && f[2] == 3.0f);

}

static void VectorAssignmentTests() {

    /* Initialize vector. */
    int rawData[] = { 1, 2, 5 };
    Vector3i a(rawData);

    /* Vector copy assignment test. */
    Vector3i b = a;
    assert(b[0] == 1 && b[1] == 2 && b[2] == 5);

    /* Addition assignment test. */
    b += Vector3i(1);
    assert(b[0] == 2 && b[1] == 3 && b[2] == 6);

    /* Subtraction assignment test. */
    b -= Vector3i(2);
    assert(b[0] == 0 && b[1] == 1 && b[2] == 4);

    /* Scalar multiplication assignment test. */
    b *= 10;
    assert(b[0] == 0 && b[1] == 10 && b[2] == 40);

    /* Scalar division assignment test. */
    b /= 5;
    assert(b[0] == 0 && b[1] == 2 && b[2] == 8);

}

static void VectorArithmeticTests() {

    /* Initialize vectors. */
    int aRawData[] { 1, 2, 3, 4 };
    int bRawData[] { 4, 5, 6, 7 };
    Vector4i a(aRawData);
    Vector4i b(bRawData);

    /* Unary plus operator test. */
    Vector4i c = +a;
    assert(c[0] == 1 && c[1] == 2 && c[2] == 3 && c[3] == 4);

    /* Unary minus operator test. */
    Vector4i d = -c;
    assert(d[0] == -1 && d[1] == -2 && d[2] == -3 && d[3] == -4);

    /* Addition operator test. */
    Vector4i e = a + b;
    assert(e[0] == 5 && e[1] == 7 && e[2] == 9 && e[3] == 11);

    /* Subtraction operator test. */
    Vector4i f = b - c;
    assert(f[0] == 3 && f[1] == 3 && f[2] == 3 && f[3] == 3);

    /* Scalar-vector multiplication operator test. */
    Vector4i g = 2 * a;
    assert(g[0] == 2 && g[1] == 4 && g[2] == 6 && g[3] == 8);

    /* Vector-scalar multiplication operator test. */
    Vector4i h = a * 3;
    assert(h[0] == 3 && h[1] == 6 && h[2] == 9 && h[3] == 12);

    /* Dot product operator test. */
    int i = a * b;
    assert(i == 60);

    /* Vector-scalar division operator test. */
    Vector4i j = h / 3;
    assert(j[0] == 1 && j[1] == 2 && j[2] == 3 && j[3] == 4);

    /* Scalar-vector division operator test. */
    Vector4i k = 12 / j;
    assert(k[0] == 12 && k[1] == 6 && k[2] == 4 && k[3] == 3);

}

static void VectorComparisonTests() {

    /* Initialize vectors. */
    int aRawData[] { 1, 2, 3, 4 };
    int bRawData[] { 1, 3, 3, 4 };
    Vector4i a(aRawData);
    Vector4i b(bRawData);
    Vector4i c(a);

    /* Equality comparison operator test. */
    bool d = (a == b);
    bool e = (a == c);
    assert(!d && e);

    /* Inequality comparison operator test. */
    bool f = (a != b);
    bool g = (a != c);
    assert(f && !g);

}

static void VectorMemberAccessTests() {

    /* Initialize vector. */
    int rawData[] { 1, 2, 3 };
    Vector3i a(rawData);

    /* Immutable member access operator test. */
    int v0 = a[0], v1 = a[1], v2 = a[2];
    assert(v0 == 1 && v1 == 2 && v2 == 3);

    /* Mutable member access operator test. */
    a[0] = 5, a[1] = 10, a[2] = -5;
    v0 = a[0], v1 = a[1], v2 = a[2];
    assert(v0 == 5 && v1 == 10 && v2 == -5);

}

static void VectorStreamOperatorTests() {

    /* Initialize string streams. */
    std::string aStr = "  [   1   2   3]  ";
    std::string bStr = "[ 1 0   -4    ";
    std::string cStr = "   0   11   9     ]  ";
    std::string dStr = "5     6 7  ";
    std::istringstream a(aStr);
    std::istringstream b(bStr);
    std::istringstream c(cStr);
    std::istringstream d(dStr);

    /* Scan vector in regular form test. */
    Vector3i e;
    a >> e;
    assert(e[0] == 1 && e[1] == 2 && e[2] == 3);

    /* Scan vector in irregular forms test. */
    b >> e;
    assert(e[0] == 1 && e[1] == 0 && e[2] == -4);
    c >> e;
    assert(e[0] == 0 && e[1] == 11 && e[2] == 9);
    d >> e;
    assert(e[0] == 5 && e[1] == 6 && e[2] == 7);

    /* Print vector test. */
    std::ostringstream f;
    f << e;
    std::string fStr = f.str();
    assert(fStr == "[ 5 6 7 ]");

}

static void VectorConversionOperatorTests() {
    
    /* String conversion operator test. */
    Vector3i a(5);
    std::string b = a;
    assert(b == "[ 5 5 5 ]");

}

static void VectorMethodTests() {

    /* GetDimension() method test. */
    Vector3d a;
    assert(a.GetDimension() == 3);

    /* GetLength() method test. */
    a[0] = 1.0, a[1] = 2.0, a[2] = 3.0;
    assert(std::fabs(a.GetLength() - 3.74165738677) < 1e-6); 

    /* GetNormalized() method test. */
    Vector3d b = a.GetNormalized();
    assert(std::fabs(b[0] - 0.267261242) < 1e-6 && std::fabs(b[1] - 0.53452248382) < 1e-6 && std::fabs(b[2] - 0.80178372573) < 1e-6);

    /* Normalize() method test. */
    a.Normalize();
    assert(std::fabs(a[0] - 0.267261242) < 1e-6 && std::fabs(a[1] - 0.53452248382) < 1e-6 && std::fabs(a[2] - 0.80178372573) < 1e-6);

    /* GetRawData() method test. */
    Vector3d c(1.4);
    const double* rawData = c.GetRawData();
    assert(rawData[0] == 1.4 && rawData[1] == 1.4 && rawData[2] == 1.4);

}

static void VectorStaticTests() {

    /* Initialize vectors. */
    int aRawData[] = { 10, 20, 30, 5 };
    double bRawData[] = { 1.1, 6.4, -4.2 };
    double cRawData[] = { -12.3, 35.2, 0.1 };
    Vector4i a(aRawData);
    Vector3d b(bRawData);
    Vector3d c(cRawData);
    
    /* FromHomogeneous(...) static method test. */
    Vector3i d = Vector3i::FromHomogeneous(a);
    assert(d[0] == 2 && d[1] == 4 && d[2] == 6);

    /* ToHomogeneous(...) static method test.*/
    Vector4i e = Vector4i::ToHomogeneous(d);
    assert(e[0] == 2 && e[1] == 4 && e[2] == 6 && e[3] == 1);

    /* Cosine(...) static method test. */
    double f = Vector3d::Cosine(b, c);
    assert(std::fabs(f - 0.7328477233972374) < 1e-6);

    /* DotProduct(...) static method test. */
    double g = Vector3d::DotProduct(b, c);
    assert(std::fabs(g - 211.33) < 1e-6);

    /* CrossProduct(...) static method test. */
    Vector3d h = Vector3d::CrossProduct(b, c);
    assert(std::fabs(h[0] - 148.48) < 1e-6 && std::fabs(h[1] - 51.55) < 1e-6 && std::fabs(h[2] - 117.44) < 1e-6);

}

int main() {

    /* Execute vector constructor tests. */
    VectorConstructorTests();

    /* Execute vector assignment tests. */
    VectorAssignmentTests();

    /* Execute vector arithmetic tests. */
    VectorArithmeticTests();

    /* Execute vector comparison tests. */
    VectorComparisonTests();

    /* Execute vector member-access tests. */
    VectorMemberAccessTests();

    /* Execute vector stream operator tests. */
    VectorStreamOperatorTests();

    /* Execute vector conversion operator tests.*/
    VectorConversionOperatorTests();

    /* Execute vector method tests. */
    VectorMethodTests();

    /* Execute vector static tests. */
    VectorStaticTests();

    return 0;

}
