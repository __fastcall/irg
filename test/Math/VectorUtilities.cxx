#include <IRG/Math.hxx>
#include <cassert>
#include <cmath>

using namespace IRG::Math;

static void VectorUtilStaticTests() {

    /* Initialize vectors. */
    double aRawData[] = { 1.0, 2.0, 3.0 };
    double bRawData[] = { 5.0, 5.0, -1.0 };
    Vector3d a(aRawData);
    Vector3d b(bRawData);

    /* Distance() method test. */
    double c = VectorUtil3d::Distance(a, b);
    assert(std::abs(c - 6.403124) < 1e-6);

    /* Reflect() method test. */
    Vector3d d = VectorUtil3d::Reflect(a, b.GetNormalized());
    assert(std::abs(d[0] - (-1.352941176470587)) < 1e-6 && std::abs(d[1] - (-0.3529411764705874)) < 1e-6 && std::abs(d[2] - 3.470588235294117) < 1e-6);

    /* Refract() method test. */
    Vector3d e = VectorUtil3d::Refract(a, b.GetNormalized(), 1.4);
    assert(std::abs(e[0] - (-1.744461418983275)) < 1e-6 && std::abs(e[1] - (-0.3444614189832755)) < 1e-6 && std::abs(e[2] - 4.828892283796654) < 1e-6);

}

int main() {

    /* Execute vector utilities static method tests. */
    VectorUtilStaticTests();

    return 0;

}
