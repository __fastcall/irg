# Configure CMake project
cmake_minimum_required(VERSION 3.1)
project(IRG VERSION 0.0.0.1)

# Add local CMake module path
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_CURRENT_SOURCE_DIR}/cmake")

# Fail if OpenGL includes and libraries were not manually entered or found automatically
if(EXISTS "${OPENGL_INCLUDE_DIRS}" AND EXISTS "${OPENGL_LIBRARIES}")
    set(OPENGL_FOUND ON)
else()
    find_package("OpenGL")
    if(NOT OPENGL_FOUND)
        message(FATAL_ERROR "OpenGL package was not found.")
    endif()
endif()

# Fail if GLUT includes and libraries were not manually entered or found automatically
if(EXISTS "${GLUT_INCLUDE_DIRS}" AND EXISTS "${GLUT_LIBRARIES}")
    set(GLUT_FOUND)
else()
    find_package("GLUT")
    if(NOT GLUT_FOUND)
        message(FATAL_ERROR "GLUT package was not found.")
    endif()
endif()

# Fail if GLFW includes and libraries were not manually entered or found automatically
if(EXISTS "${GLFW_INCLUDE_DIRS}" AND EXISTS "${GLFW_LIBRARIES}")
    set(GLFW_FOUND)
else()
    find_package("GLFW")
    if(NOT GLFW_FOUND)
        message(FATAL_ERROR "GLFW package was not found.")
    endif()
endif()

# Fail if GLEW includes and libraries were not manually entered or found automatically
if(EXISTS "${GLEW_INCLUDE_DIRS}" AND EXISTS "${GLEW_LIBRARIES}")
    set(GLEW_FOUND)
else()
    find_package("GLEW")
    if(NOT GLEW_FOUND)
        message(FATAL_ERROR "GLEW package was not found.")
    endif()
endif()

# Add option to link GLEW statically
set(GLEW_LINK_STATIC OFF CACHE BOOL "Defines GLEW_STATIC in all source files.")

# GLEW is usually linked statically on Windows
if(WIN32)
    set(GLEW_LINK_STATIC ON)
endif()

# Require C++11 standard features
set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

# Enable testing
enable_testing()

# Set include directory paths for all targets
include_directories(
    ${CMAKE_CURRENT_SOURCE_DIR}/include
    ${OPENGL_INCLUDE_DIRS}
    ${GLUT_INCLUDE_DIRS}
    ${GLFW_INCLUDE_DIRS}
    ${GLEW_INCLUDE_DIRS}
)

# FreeGLUT linked statically
add_definitions(-DFREEGLUT_STATIC)

# Define if GLEW is linked statically
if(GLEW_LINK_STATIC)
    add_definitions(-DGLEW_STATIC)
endif()

# Library dependencies
set(DEP_LIBS ${GLUT_LIBRARIES} ${OPENGL_LIBRARIES} CACHE INTERNAL "")
if(WIN32)
    set(DEP_LIBS ${DEP_LIBS} winmm)
endif()

# Library dependencies (only with GLFW now)
set(DEP_LIBS_GLFW ${GLFW_LIBRARIES} ${OPENGL_LIBRARIES} CACHE INTERNAL "")

# Library dependencies (only with GLFW and GLEW now)
set(DEP_LIBS_GLFW_GLEW ${GLFW_LIBRARIES} ${GLEW_LIBRARIES} ${OPENGL_LIBRARIES} CACHE INTERNAL "")

# Add src subdirectory
add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/src)

# Add labs subdirectory
add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/lab)

# Add documentation subdirectory
add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/doc)

# Add tests subdirectory
add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/test)
