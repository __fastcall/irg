# Interactive computer graphics

## Lab 1

Linear algebra library

## Lab 2

OpenGL triangle painting program

## Lab 3

Bresenham algorithm & Cohen-Sutherland line clipping algorithm

## Lab 4

Polygon filling algorithm & 2D hit-test algorithm

## Lab 5

Mesh loading & 3D hit-test algorithm

## Lab 6

Projection matrices construction

## Lab 7

Face culling algorithms

## Lab 8

Bezier curve

## Lab 9

Realtime shading

## Lab 10

Fractals
