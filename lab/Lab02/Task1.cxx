#include <IRG/Math.hxx>
#include <GL/glut.h>
#include <list>

using namespace IRG::Math;

enum class Status {
    Idle,
    TempLine,
    TempTriangle,
    __COUNT
};

struct Triangle {
    Vector2i p[3];
    Vector3f c;
};

static Vector3f g_Colors[] = {
    Vector3f({1.0f, 0.0f, 0.0f}),
    Vector3f({0.0f, 1.0f, 0.0f}),
    Vector3f({0.0f, 0.0f, 1.0f}),
    Vector3f({0.0f, 1.0f, 1.0f}),
    Vector3f({1.0f, 1.0f, 0.0f}),
    Vector3f({1.0f, 0.0f, 1.0f})
};
static std::list<Triangle> g_Triangles;
static Status g_CurrentState = Status::Idle;
static int g_CurrentColor = 0;
static Triangle g_TempTriangle;
static bool g_ShouldAdvance = false;

void reshapeCbck(int width, int height) {
    
    /* Obnavljanje viewporta i postavljanje ortografske projekcije s velicinom prozora. */
    glDisable(GL_DEPTH_TEST);
    glViewport(0, 0, width, height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0, width - 1, height - 1, 0, 0, 1);
    glMatrixMode(GL_MODELVIEW);

}

void displayCbck() {

    /* Brisanje ekrana i ucitavanje identitetne matrice. */
    glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);
    glLoadIdentity();

    /* Crtanje postojecih trokuta. */
    glBegin(GL_TRIANGLES);
    for (const Triangle& t : g_Triangles) {
        glColor3f(t.c[0], t.c[1], t.c[2]);
        glVertex2i(t.p[0][0], t.p[0][1]);
        glVertex2i(t.p[1][0], t.p[1][1]);
        glVertex2i(t.p[2][0], t.p[2][1]);
    }
    glEnd();

    /* Crtanje privremenog trokuta (ili linije). */
    glColor3f(g_Colors[g_CurrentColor][0], g_Colors[g_CurrentColor][1], g_Colors[g_CurrentColor][2]);
    switch (g_CurrentState) {
    case Status::TempLine:
        glBegin(GL_LINES);
        glVertex2i(g_TempTriangle.p[0][0], g_TempTriangle.p[0][1]);
        glVertex2i(g_TempTriangle.p[1][0], g_TempTriangle.p[1][1]);
        glEnd();
        break;
    case Status::TempTriangle:
        glBegin(GL_TRIANGLES);
        glVertex2i(g_TempTriangle.p[0][0], g_TempTriangle.p[0][1]);
        glVertex2i(g_TempTriangle.p[1][0], g_TempTriangle.p[1][1]);
        glVertex2i(g_TempTriangle.p[2][0], g_TempTriangle.p[2][1]);
        glEnd();
        break;
    }

    /* Crtanje indikatora trenutne boje. */
    glBegin(GL_QUADS);
    glVertex2i(5, 0);
    glVertex2i(0, 0);
    glVertex2i(0, 5);
    glVertex2i(5, 5);
    glEnd();

    /* Zamjena medjuspremnika. */
    glutSwapBuffers();

    /* Ako treba unaprijediti stanje, unaprijedi ga. */
    if (g_ShouldAdvance) {
        g_CurrentState = static_cast<Status>((static_cast<int>(g_CurrentState) + 1) % static_cast<int>(Status::__COUNT));
        g_ShouldAdvance = false;
    }

}

void keyboardCbck(unsigned char key, int x, int y) {

    /* Promjena boje crtanja. */
    if (key == 'n') {
        g_CurrentColor = (g_CurrentColor + 1) % (sizeof(g_Colors) / sizeof(g_Colors[0]));
    } else if (key == 'p') {
        g_CurrentColor = (g_CurrentColor - 1) % (sizeof(g_Colors) / sizeof(g_Colors[0]));
    } else {
        return;
    }
    glutPostRedisplay();

}

void mouseMoveCbck(int x, int y) {
    
    /* Postavljanje vrhova trokuta. */
    g_TempTriangle.p[static_cast<int>(g_CurrentState)] = { x, y };
    if (g_CurrentState != Status::Idle)
        glutPostRedisplay();

}

void mouseCbck(int button, int state, int x, int y) {

    /* Dodavanje trokuta i izmjena stanja. */
    if (button == GLUT_LEFT_BUTTON && state == GLUT_UP) {
        if (g_CurrentState == Status::TempTriangle) {
            g_TempTriangle.c = g_Colors[g_CurrentColor];
            g_Triangles.push_back(g_TempTriangle);
        }
        g_ShouldAdvance = true;
        glutPostRedisplay();
    }

}

int main(int argc, char** argv) {

    /* Inicijalizacija konteksta i prozora. */
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE);
    glutInitWindowSize(200, 200);
    glutInitWindowPosition(0, 0);
    glutCreateWindow("Crtanje trokuta");
    glutDisplayFunc(displayCbck);
    glutReshapeFunc(reshapeCbck);
    glutKeyboardFunc(keyboardCbck);
    glutPassiveMotionFunc(mouseMoveCbck);
    glutMouseFunc(mouseCbck);
    glutMainLoop();

}
