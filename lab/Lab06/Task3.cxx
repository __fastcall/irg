#include "DrawWindow.hxx"
#include <GL/gl.h>
#include <GL/glu.h>
#include <IRG/Math.hxx>
#include <cmath>

namespace Lab06 {

    static IRG::Math::Vector3f g_Eye({3.0f, 4.0f, 1.0f});
    static IRG::Math::Vector3f g_Center({0.0f, 0.0f, 0.0f});
    static IRG::Math::Vector3f g_Up({0.0f, 1.0f, 0.0f});

    class Task1_Window : public DrawWindow {

    public:

        Task1_Window(int argc, char** argv) : DrawWindow(argc, argv) { }

    protected:

        virtual void Init() override {

            /* Set clear color. */
            glClearColor(0.0f, 1.0f, 0.0f, 1.0f);

        }

        virtual void Render(float deltaTime) override {

            /* Set up projection matrix (identity). */
            glMatrixMode(GL_PROJECTION);
            glLoadIdentity();

            /* Set up model-view matrix (identity). */
            glMatrixMode(GL_MODELVIEW);
            glLoadIdentity();

            /* Get vertices and faces. */
            const std::vector<IRG::Math::Vector3f>& vertices = GetMesh().GetVertices();
            const std::vector<std::vector<IRG::Math::Vector3i>>& faces = GetMesh().GetFaces();

            /* Calculate the required matrices (my methods returns a premultiplied-convention matrix so a transpose is required). */
            IRG::Math::Vector3f eye({InvSinAngle * std::cos(Angle), g_Eye[1], InvSinAngle * std::sin(Angle)});
            IRG::Math::Matrix4f lookAt = IRG::Math::MatrixUtil4f::CreateLookAtMatrix(eye, g_Center, g_Up).GetTransposed();
            IRG::Math::Matrix4f perspective = IRG::Math::MatrixUtil4f::CreatePerspectiveMatrixOffCenter(-0.5f, 0.5f, -0.5f, 0.5f, 1.0f, 100.0f).GetTransposed();

            /* Clear screen. */
            glClear(GL_COLOR_BUFFER_BIT);

            /* Draw all faces. */
            glColor3f(1.0f, 0.0f, 0.0f);
            for (const std::vector<IRG::Math::Vector3i>& face : faces) {
                glBegin(GL_LINE_LOOP);
                for (const IRG::Math::Vector3i& index : face) {
                    IRG::Math::Vector4f vertex = IRG::Math::Vector4f::ToHomogeneous(vertices[index[0]]);
                    IRG::Math::Vector4f transformedVertex = vertex * lookAt * perspective;
                    IRG::Math::Vector3f displayedVertex = IRG::Math::Vector3f::FromHomogeneous(transformedVertex);
                    glVertex3f(displayedVertex[0], displayedVertex[1], displayedVertex[2]);
                }
                glEnd();
            }

        }

    };

}

int main(int argc, char** argv) {

    /* Create window and enter loop. */
    Lab06::Task1_Window window(argc, argv);
    window.EnterLoop();
    return 0;

}
