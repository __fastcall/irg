#include "DrawWindow.hxx"
#include <string>
#include <iostream>
#include <fstream>
#include <GLFW/glfw3.h>
#include <ctime>
#include <cmath>

#define INITIAL_ANGLE 18.4349488f
#define INITIAL_ANGLE_INCREMENT 1.0f
#define INITIAL_INVSINANGLE 3.16227766f

namespace Lab06 {

    DrawWindow::DrawWindow(int argc, char** argv) {

        /* Path to model. */
        std::string filename;

        /* Read path to model from arguments or manually enter by hand. */
        if (argc >= 2) {
            filename = argv[1];
        } else {
            std::cout << "Unesi Wavefront OBJ datoteku: ";
            std::cin >> filename;
        }

        /* Terminate application if file doesn't exist. */
        if (!std::ifstream(filename).good()) {
            std::cerr << "Datoteka '" << filename << "' ne postoji." << std::endl;
            exit(-1);
        }

        /* Load and normalize model. */
        m_Mesh = IRG::Model::Wavefront::Load(filename);
        m_Mesh.NormalizeVertices();

        /* Print statistic. */
        std::cout << "Uspjesno ucitano " << m_Mesh.GetVertices().size() << " vrhova i " << m_Mesh.GetFaces().size() << " lica" << std::endl << std::endl;

    }

    void DrawWindow::EnterLoop(const char* title) {

        /* Initialize GLFW if first time. */
        if (!m_InitializedGLFW) {
            glfwInit();
            m_InitializedGLFW = true;
        }

        /* Create window. */
        m_Window = glfwCreateWindow(640, 480, title, nullptr, nullptr);
        glfwSetWindowSizeCallback(m_Window, ResizeWindow);
        glfwSetKeyCallback(m_Window, KeyEvent);
        glfwMakeContextCurrent(m_Window);

        /* Initialize context. */
        Init();

        /* Main loop. */
        std::clock_t dStart = std::clock(), dEnd;
        while (!glfwWindowShouldClose(m_Window)) {
            glfwPollEvents();
            dEnd = std::clock();
            float delta = (dEnd - dStart) / (float)CLOCKS_PER_SEC;
            dStart = dEnd;
            if (IsRotating) Angle -= delta * AngleIncrement;
            Render(delta);
            glfwSwapBuffers(m_Window);
        }

    }

    void (*DrawWindow::KeyPressedCallback)(int key) = nullptr;

    void DrawWindow::Init() { }

    void DrawWindow::ResizeWindow(GLFWwindow* win, int width, int height) {

        /* Set OpenGL viewport. */
        glViewport(0, 0, width, height);

    }

    void DrawWindow::KeyEvent(GLFWwindow* win, int key, int scancode, int action, int mods) {

        /* Accept key down events only. */
        if (action == GLFW_PRESS) {

            /* Handle key press. */
            switch (key) {
            case GLFW_KEY_R: Angle += AngleIncrement; break;
            case GLFW_KEY_L: Angle -= AngleIncrement; break;
            case GLFW_KEY_ESCAPE: Angle = INITIAL_ANGLE; break;
            case GLFW_KEY_SPACE: IsRotating = !IsRotating; break;
            default: if (KeyPressedCallback != nullptr) KeyPressedCallback(key);
            }

        }

    }

    float DrawWindow::Angle = INITIAL_ANGLE, DrawWindow::AngleIncrement = INITIAL_ANGLE_INCREMENT, DrawWindow::InvSinAngle = INITIAL_INVSINANGLE;

    bool DrawWindow::IsRotating = false;

    bool DrawWindow::m_InitializedGLFW = false;

}
