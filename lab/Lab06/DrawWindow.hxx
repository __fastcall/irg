#ifndef __LAB06_DRAWWINDOW_HXX__
#define __LAB06_DRAWWINDOW_HXX__

#include <IRG/Model.hxx>

class GLFWwindow;

namespace Lab06 {

    class DrawWindow {

    public:

        /**
         * @brief Creates a draw window (and loads the model either from command line arguments or standard input);
         * @param argc Argument count.
         * @param argv Arguments.
         */
        DrawWindow(int argc, char** argv);

        /**
         * @brief Enters the main loop.
         * @param title Window title.
         */
        void EnterLoop(const char* title = "Lab 6");

        /**
         * @brief Called when an unassigned key is pressed.
         */
        static void (*KeyPressedCallback)(int key);

    protected:

        /**
         * @brief Gets the immutable mesh instance.
         * @return const IRG::Model::Mesh& %Mesh instance.
         */
        inline const IRG::Model::Mesh& GetMesh() const { return m_Mesh; }

        /**
         * @brief Gets the window instance.
         * @return const GLFWwindow* Window instance.
         */
        inline const GLFWwindow* GetWindow() const { return m_Window; }

        /**
         * @brief Initializes context.
         */
        virtual void Init();

        /**
         * @brief Gets called every frame for rendering the display.
         * @param deltaTime Approx. elapsed time (in seconds) since previous frame.
         */
        virtual void Render(float deltaTime) = 0;

        /**
         * @brief Helper static variables for rotating view.
         */
        static float Angle, AngleIncrement, InvSinAngle;

        /**
         * @brief Helper static variable to indicate is the eye rotating.
         */
        static bool IsRotating;

    private:

        IRG::Model::Mesh m_Mesh;

        GLFWwindow* m_Window;

        static void ResizeWindow(GLFWwindow* win, int width, int height);

        static void KeyEvent(GLFWwindow* win, int key, int scancode, int action, int mods);

        static bool m_InitializedGLFW;
    
    };

}

#endif
