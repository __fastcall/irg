#include "DrawWindow.hxx"
#include <GL/gl.h>
#include <GL/glu.h>
#include <IRG/Math.hxx>
#include <cmath>

namespace Lab06 {

    static IRG::Math::Vector3f g_Eye({3.0f, 4.0f, 1.0f});
    static IRG::Math::Vector3f g_Center({0.0f, 0.0f, 0.0f});
    static IRG::Math::Vector3f g_Up({0.0f, 1.0f, 0.0f});

    class Task1_Window : public DrawWindow {

    public:

        Task1_Window(int argc, char** argv) : DrawWindow(argc, argv) { }

    protected:

        virtual void Init() override {

            /* Set clear color. */
            glClearColor(0.0f, 1.0f, 0.0f, 1.0f);

        }

        virtual void Render(float deltaTime) override {

            /* Set up projection matrix. */
            glMatrixMode(GL_PROJECTION);
            glLoadIdentity();
            /*
               glFrustum(-0.5, 0.5, -0.5, 0.5, 1.0, 100.0);

                fovy = 2 * atan2(height / 2, zNear) = 2 * atan2((top - bottom) / 2, zNear) = 2 * atan2(0.5, 1.0) = 2 * 26.5650512 = 53.1301024
                aspect = width / height = (right - left) / (top - bottom) = (0.5 - (-0.5)) / (0.5 - (-0.5)) = 1.0
            */
            gluPerspective(53.1301024, 1.0, 1.0, 100.0);

            /* Set up model-view matrix. */
            glMatrixMode(GL_MODELVIEW);
            glLoadIdentity();
            IRG::Math::Vector3f eye({InvSinAngle * std::cos(Angle), g_Eye[1], InvSinAngle * std::sin(Angle)});
            gluLookAt(eye[0], eye[1], eye[2], g_Center[0], g_Center[1], g_Center[2], g_Up[0], g_Up[1], g_Up[2]);

            /* Get vertices and faces. */
            const std::vector<IRG::Math::Vector3f>& vertices = GetMesh().GetVertices();
            const std::vector<std::vector<IRG::Math::Vector3i>>& faces = GetMesh().GetFaces();

            /* Clear screen. */
            glClear(GL_COLOR_BUFFER_BIT);

            /* Draw all faces. */
            glColor3f(1.0f, 0.0f, 0.0f);
            for (const std::vector<IRG::Math::Vector3i>& face : faces) {
                glBegin(GL_LINE_LOOP);
                for (const IRG::Math::Vector3i& index : face) {
                    glVertex3f(vertices[index[0]][0], vertices[index[0]][1], vertices[index[0]][2]);
                }
                glEnd();
            }

        }

    };

}

int main(int argc, char** argv) {

    /* Create window and enter loop. */
    Lab06::Task1_Window window(argc, argv);
    window.EnterLoop();
    return 0;

}
