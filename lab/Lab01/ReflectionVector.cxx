#include <IRG/Math.hxx>
#include <iostream>

using namespace IRG::Math;

int main() {

    while (true) {

        std::cout << "Unesi broj dimenzija (2 ili 3): ";
        int d;
        std::cin >> d;
        if (d != 2 && d != 3) {
            break;
        }

        std::cout << "Unesi ulazni vektor: ";
        Vector3d m;
        if (d == 2) {
            Vector2d m2;
            std::cin >> m2;
            m = Vector3d(m2);
        } else {
            std::cin >> m;
        }

        std::cout << "Unesi vektor oko kojega se ulazni vektor reflektira: ";
        Vector3d n;
        if (d == 2) {
            Vector2d n2;
            std::cin >> n2;
            n = Vector3d(n2);
        } else {
            std::cin >> n;
        }

        Vector3d nNorm = n.GetNormalized();
        Vector3d k = nNorm * (nNorm * m);
        Vector3d p = k - m;
        Vector3d r = m + 2.0 * p;

        std::cout << "Reflektirani vektor je: ";
        if (d == 2) {
            std::cout << Vector2d(r);
        } else {
            std::cout << r;
        }
        std::cout << std::endl << std::endl;

    }

    return 0;

}
