#include <IRG/Math.hxx>
#include <iostream>

using namespace IRG::Math;

int main() {
    
    Matrix3d a;
    Vector3d r;

    for (int i = 0; i < 3; ++i) {

        std::cout << "Unesi vrh " << static_cast<char>(static_cast<int>('A') + i) << ": ";
        Vector3d t;
        std::cin >> t;
        a[0][i] = t[0];
        a[1][i] = t[1];
        a[2][i] = t[2];

    }

    std::cout << "Unesi tocku T: ";
    std::cin >> r;

    Vector3d v = a.GetInverse() * r;

    std::cout << "Rjesenje sustava je: " << v << std::endl;

    return 0;

}
