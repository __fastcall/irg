#include <IRG/Math.hxx>
#include <iostream>

using namespace IRG::Math;

int main() {
    
    Matrix3d a;
    Vector3d r;

    for (int i = 0; i < 3; ++i) {

        std::cout << "Jednadzba: a" << i << " * x + b" << i << " * y + c" << i << " * z = d" << i << std::endl;
        
        std::cout << "a" << i << " = ";
        std::cin >> a[i][0];

        std::cout << "b" << i << " = ";
        std::cin >> a[i][1];

        std::cout << "c" << i << " = ";
        std::cin >> a[i][2];

        std::cout << "d" << i << " = ";
        std::cin >> r[i];

        std::cout << std::endl;

    }

    Vector3d v = a.GetInverse() * r;

    std::cout << "Rjesenje sustava je: " << v << std::endl;

    return 0;

}
