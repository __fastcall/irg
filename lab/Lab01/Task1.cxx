#include <IRG/Math.hxx>
#include <iostream>

using namespace IRG::Math;

int main() {

    Vector3d v1 = Vector3d("2 3 -4") + Vector3d("-1 4 -3");
    std::cout << "v1 = " << v1 << std::endl;

    double s = v1 * Vector3d("-1 4 -3");
    std::cout << "s = " << s << std::endl;

    Vector3d v2 = Vector3d::CrossProduct(v1, Vector3d("2 2 4"));
    std::cout << "v2 = " << v2 << std::endl;

    Vector3d v3 = v2.GetNormalized();
    std::cout << "v3 = " << v3 << std::endl;

    Vector3d v4 = -v2;
    std::cout << "v4 = " << v4 << std::endl;

    Matrix3d m1 = Matrix3d("1 2 3 | 2 1 3 | 4 5 1") + Matrix3d("-1 2 -3 | 5 -2 7 | -4 -1 3");
    std::cout << "m1 = " << m1 << std::endl;

    Matrix3d m2 = Matrix3d("1 2 3 | 2 1 3 | 4 5 1") * Matrix3d("-1 2 -3 | 5 -2 7 | -4 -1 3").GetTransposed();
    std::cout << "m2 = " << m2 << std::endl;

    Matrix3d m3 = Matrix3d("-24 18 5 | 20 -15 -4 | -5 4 1").GetInverse() * Matrix3d("1 2 3 | 0 1 4 | 5 6 0").GetInverse();
    std::cout << "m3 = " << m3 << std::endl;

    return 0;

}
