#ifndef __TASK1_HXX__
#define __TASK1_HXX__

#include <string>

/* Shading mode enumeration. */
enum class ShadingMode {
    Constant = 0,
    Gouraud = 1,
    Phong = 2
};

/* Shading mode strings. */
extern const std::string ShadingModeStrings[];

#endif
