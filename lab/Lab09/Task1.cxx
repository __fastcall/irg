#include "Task1.hxx"
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <IRG/Math.hxx>
#include <IRG/Model.hxx>
#include <iostream>
#include <fstream>
#include <ctime>
#include <vector>

/* Global definitions. */
#define WIN_WIDTH 640
#define WIN_HEIGHT 480

/* Shading mode strings. */
const std::string ShadingModeStrings[] = { "Constant", "Gouraud", "Phong" };

/* Global variables. */
static GLFWwindow* g_Window = nullptr;
static IRG::Math::Matrix4f g_Model;
static IRG::Math::Matrix4f g_View;
static IRG::Math::Matrix4f g_Projection;
static ShadingMode g_ShadingMode = ShadingMode::Phong;
static bool g_CalculateAttenuation = true;
static float g_AttenuationScale = 2.5f;
static const IRG::Math::Vector3f g_CameraPosition = { 1.5f, 1.5f, 1.5f };
static float g_RotationY = 0.0f;
static bool g_IsRotating = false;
static GLuint g_Program;
static GLuint g_VAO;
static GLuint g_Count;

/* Light uniforms. */
static IRG::Math::Vector3f g_LightPosition = { -0.5f, 1.5f, 1.5f };
static IRG::Math::Vector4f g_LightAmbient = { 0.1f, 0.1f, 0.1f, 1.0f };
static IRG::Math::Vector4f g_LightDiffuse = { 1.0f, 1.0f, 1.0f, 1.0f };
static IRG::Math::Vector4f g_LightSpecular = { 1.0f, 1.0f, 1.0f, 1.0f };

/* Material uniforms. */
static IRG::Math::Vector4f g_MaterialAmbient = { 1.0f, 0.0f, 0.0f, 1.0f };
static IRG::Math::Vector4f g_MaterialDiffuse = { 1.0f, 0.0f, 0.0f, 1.0f };
static IRG::Math::Vector4f g_MaterialSpecular = { 1.0f, 1.0f, 1.0f, 1.0f };
static float g_MaterialShininess = 64.0f;

/* Resize function. */
static void resizeCbck(GLFWwindow* win, int width, int height) {

    /* Reset viewport. */
    glViewport(0, 0, width, height);

    /* Reset projection matrix. */
    g_Projection = IRG::Math::MatrixUtil4f::CreatePerspectiveMatrixFOV(DEGREES(60.0f), (float)width / height, 0.1f, 100.0f);

}

/* Keyboard key function. */
static void keyCbck(GLFWwindow* win, int key, int scancode, int action, int mods) {

    /* Only react to key down events. */
    if (action == GLFW_PRESS) {

        /* Switch on key. */
        switch (key) {

        /* Toggle face culling. */
        case GLFW_KEY_F:
            if (glIsEnabled(GL_CULL_FACE))
                glDisable(GL_CULL_FACE);
            else
                glEnable(GL_CULL_FACE);
            break;

        /* Toggle depth testing. */
        case GLFW_KEY_D:
            if (glIsEnabled(GL_DEPTH_TEST))
                glDisable(GL_DEPTH_TEST);
            else
                glEnable(GL_DEPTH_TEST);
            break;

        /* Toggle light attenuation. */
        case GLFW_KEY_A:
            g_CalculateAttenuation = !g_CalculateAttenuation;
            break;

        /* Set constant shading mode. */
        case GLFW_KEY_1:
        case GLFW_KEY_KP_1:
        case GLFW_KEY_C:
            g_ShadingMode = ShadingMode::Constant;
            break;

        /* Set Gouraud shading mode. */
        case GLFW_KEY_2:
        case GLFW_KEY_KP_2:
        case GLFW_KEY_G:
            g_ShadingMode = ShadingMode::Gouraud;
            break;

        /* Set Phong shading mode. */
        case GLFW_KEY_3:
        case GLFW_KEY_KP_3:
        case GLFW_KEY_P:
            g_ShadingMode = ShadingMode::Phong;
            break;

        /* Toggle model rotation. */
        case GLFW_KEY_SPACE:
            g_IsRotating = !g_IsRotating;
            return;

        /* Increase light strength (attenuation only). */
        case GLFW_KEY_KP_ADD:
            if (g_AttenuationScale >= 10.0f) return;
            g_AttenuationScale += 0.25f;
            return;

        /* Decrease light strength (attenuation only). */
        case GLFW_KEY_KP_SUBTRACT:
            if (g_AttenuationScale <= 0.0f) return;
            g_AttenuationScale -= 0.25f;
            return;

        /* Ignore all other keys. */
        default:
            return;

        }

        /* Print state. */
        std::cout
            << "GL_CULL_FACE = " << (glIsEnabled(GL_CULL_FACE) ? "On" : "Off") << "; "
            << "GL_DEPTH_TEST = " << (glIsEnabled(GL_DEPTH_TEST) ? "On" : "Off") << "; "
            << "Attenuation = " << (g_CalculateAttenuation ? "On" : "Off") << "; "
            << "ShadingMode = " << ShadingModeStrings[static_cast<int>(g_ShadingMode)]
            << std::endl;

    }

}

/* Render function. */
static void render(float deltaTime) {

    /* Clear color and depth buffer. */
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    /* Update model matrix. */
    if (g_IsRotating) {
        g_RotationY += 0.75f * deltaTime;
        g_Model = IRG::Math::MatrixUtil4f::CreateRotationY(g_RotationY);
    }

    /* Set uniform variables. */
    glUniformMatrix4fv(glGetUniformLocation(g_Program, "model"), 1, true, g_Model.GetRawData());
    glUniformMatrix4fv(glGetUniformLocation(g_Program, "view"), 1, true, g_View.GetRawData());
    glUniformMatrix4fv(glGetUniformLocation(g_Program, "proj"), 1, true, g_Projection.GetRawData());
    glUniform1i(glGetUniformLocation(g_Program, "shading_mode"), static_cast<int>(g_ShadingMode));
    glUniform1i(glGetUniformLocation(g_Program, "calc_atten"), static_cast<int>(g_CalculateAttenuation));
    glUniform1f(glGetUniformLocation(g_Program, "atten_scale"), g_AttenuationScale);
    glUniform3fv(glGetUniformLocation(g_Program, "camera_pos"), 1, g_CameraPosition.GetRawData());
    glUniform3fv(glGetUniformLocation(g_Program, "light_pos"), 1, g_LightPosition.GetRawData());
    glUniform4fv(glGetUniformLocation(g_Program, "light_amb"), 1, g_LightAmbient.GetRawData());
    glUniform4fv(glGetUniformLocation(g_Program, "light_diff"), 1, g_LightDiffuse.GetRawData());
    glUniform4fv(glGetUniformLocation(g_Program, "light_spec"), 1, g_LightSpecular.GetRawData());
    glUniform4fv(glGetUniformLocation(g_Program, "mat_amb"), 1, g_MaterialAmbient.GetRawData());
    glUniform4fv(glGetUniformLocation(g_Program, "mat_diff"), 1, g_MaterialDiffuse.GetRawData());
    glUniform4fv(glGetUniformLocation(g_Program, "mat_spec"), 1, g_MaterialSpecular.GetRawData());
    glUniform1f(glGetUniformLocation(g_Program, "mat_shin"), g_MaterialShininess);

    /* Use program and bind VAO. */
    glUseProgram(g_Program);
    glBindVertexArray(g_VAO);

    /* Draw object. */
    glDrawArrays(GL_TRIANGLES, 0, g_Count);

    /* Unbind VAO. */
    glBindVertexArray(0);

    /* Swap buffers. */
    glfwSwapBuffers(g_Window);

}

/* Load and compile GLSL shader. */
static GLuint compileShader(const std::string& filename, GLenum shaderType) {

    /* Create shader. */
    GLuint shader = glCreateShader(shaderType);

    /* Open file stream and determine file length. */
    std::ifstream ifs(filename, std::ios::in | std::ios::binary);
    ifs.seekg(0, std::ios::end);
    size_t size = ifs.tellg();
    ifs.seekg(0, std::ios::beg);

    /* Read file data. */
    GLchar data[size];
    ifs.read(data, size);

    /* Close file stream. */
    ifs.close();

    /* Load shader source. */
    GLchar* srcs[] = { data };
    GLint lens[] = { (GLint)size };
    glShaderSource(shader, 1, srcs, lens);

    /* Compile shader. */
    glCompileShader(shader);

    /* Get shader compile status. */
    GLint status;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &status);

    /* Print error message and exit if compilation failed. */
    if (status == GL_FALSE) {
        GLchar infolog[512];
        GLsizei length;
        glGetShaderInfoLog(shader, sizeof(infolog), &length, infolog);
        std::cerr << infolog;
        std::exit(1);
    }

    /* Return shader. */
    return shader;

}

/* Entry function. */
int main(int argc, char** argv) {
    
    /* Assure that the program was called properly. */
    if (argc != 2) {
        std::cerr << "Usage: " << argv[0] << " obj_file" << std::endl;
        return 1;
    }

    /* Load mesh. */
    IRG::Model::Mesh mesh = IRG::Model::Wavefront::Load(argv[1]);
    mesh.NormalizeVertices();
    mesh.CalculateNormals();
    const std::vector<IRG::Math::Vector3f>& vertices = mesh.GetVertices();
    const std::vector<IRG::Math::Vector3f>& normals = mesh.GetNormals();
    const std::vector<std::vector<IRG::Math::Vector3i>>& faces = mesh.GetFaces();

    /* Generate buffer. */
    std::vector<float> buff;
    for (const std::vector<IRG::Math::Vector3i>& face : faces) {
        IRG::Math::Vector3f avgpos;
        for (const IRG::Math::Vector3i& ids : face) {
            avgpos += vertices[ids[0]];
        }
        avgpos /= face.size();
        for (size_t i = 0; i < 3; ++i) {
            const IRG::Math::Vector3i& ids = face[i];
            buff.push_back(vertices[ids[0]][0]);
            buff.push_back(vertices[ids[0]][1]);
            buff.push_back(vertices[ids[0]][2]);
            buff.push_back(normals[ids[2]][0]);
            buff.push_back(normals[ids[2]][1]);
            buff.push_back(normals[ids[2]][2]);
            buff.push_back(avgpos[0]);
            buff.push_back(avgpos[1]);
            buff.push_back(avgpos[2]);
            IRG::Math::Vector3f avgnorm;
            int nnorm = 0;
            for (const std::vector<IRG::Math::Vector3i>& face2 : faces) {
                for (const IRG::Math::Vector3i& ids2 : face2) {
                    if (ids2[0] == ids[0]) {
                        avgnorm += normals[ids2[2]];
                        nnorm++;
                    }
                }
            }
            avgnorm /= nnorm;
            buff.push_back(avgnorm[0]);
            buff.push_back(avgnorm[1]);
            buff.push_back(avgnorm[2]);
        }
    }
    g_Count = buff.size() / 12;

    /* Initialize GLFW. */
    glfwInit();

    /* Create window and bind events. */
    glfwWindowHint(GLFW_SAMPLES, 4);
    g_Window = glfwCreateWindow(WIN_WIDTH, WIN_HEIGHT, "Lab 9", nullptr, nullptr);
    glfwSetWindowSizeCallback(g_Window, resizeCbck);
    glfwSetKeyCallback(g_Window, keyCbck);

    /* Make GL context current. */
    glfwMakeContextCurrent(g_Window);

    /* Set up GL context properties. */
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glEnable(GL_CULL_FACE);
    glCullFace(GL_BACK);
    glFrontFace(GL_CCW);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);
    glEnable(GL_MULTISAMPLE);

    /* Initialize GLEW. */
    glewInit();

    /* Compile GLSL shaders. */
    GLuint vertexShader = compileShader("vertex.glsl", GL_VERTEX_SHADER);
    GLuint fragmentShader = compileShader("fragment.glsl", GL_FRAGMENT_SHADER);

    /* Create program and attach shaders. */
    g_Program = glCreateProgram();
    glAttachShader(g_Program, vertexShader);
    glAttachShader(g_Program, fragmentShader);

    /* Link program. */
    glLinkProgram(g_Program);

    /* Get program link status. */
    GLint status;
    glGetProgramiv(g_Program, GL_LINK_STATUS, &status);

    /* Print error message and exit if linking failed. */
    if (status == GL_FALSE) {
        GLchar infolog[512];
        GLsizei length;
        glGetProgramInfoLog(g_Program, sizeof(infolog), &length, infolog);
        std::cerr << infolog;
        return 1;
    }

    /* Delete GLSL shaders. */
    glDeleteShader(vertexShader);
    glDeleteShader(fragmentShader);

    /* Create and bind vertex array. */
    glCreateVertexArrays(1, &g_VAO);
    glBindVertexArray(g_VAO);

    /* Create and bind array buffer. */
    GLuint vbo;
    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);

    /* Upload data to GPU buffer. */
    glBufferData(GL_ARRAY_BUFFER, buff.size() * sizeof(float), buff.data(), GL_STATIC_DRAW);

    /* Map vertex array attributes. */
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 12 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 12 * sizeof(float), (void*)(3 * sizeof(float)));
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 12 * sizeof(float), (void*)(6 * sizeof(float)));
    glEnableVertexAttribArray(2);
    glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, 12 * sizeof(float), (void*)(9 * sizeof(float)));
    glEnableVertexAttribArray(3);

    /* Unbind vertex array. */
    glBindVertexArray(0);

    /* Initialize matrices. */
    g_Model = IRG::Math::MatrixUtil4f::CreateIdentity();
    g_View = IRG::Math::MatrixUtil4f::CreateLookAtMatrix(g_CameraPosition, { 0.0f, 0.0f, 0.0f }, { 0.0f, 1.0f, 0.0f });
    resizeCbck(g_Window, WIN_WIDTH, WIN_HEIGHT);

    /* Event-render loop. */
    std::clock_t dStart = std::clock(), dEnd;
    while (!glfwWindowShouldClose(g_Window)) {
        glfwPollEvents();
        dEnd = std::clock();
        float deltaTime = (dEnd - dStart) / (float)CLOCKS_PER_SEC;
        dStart = dEnd;
        render(deltaTime);
    }

    /* Exit success. */
    return 0;

}
