#include <IRG/Math.hxx>
#include <GLFW/glfw3.h>
#include <list>
#include <algorithm>
#include <iostream>
#include <cstddef>

using namespace IRG::Math;

struct Line {
    Vector2i start;
    Vector2i end;
};

static GLFWwindow* g_Window = nullptr;
static std::list<Line> g_Lines;
static Line g_TempLine;
static Vector2i g_CutoutTopLeft, g_CutoutBottomRight;
static bool g_Pressed = false;
static bool g_Control = false, g_Cutout = false;

void bresenham_90_bottom(const Line& line) {

    /* Izracunaj zajednicke varijable. */
    int x = line.start[0];
    int y = line.start[1];
    int diffx = line.end[0] - line.start[0];
    int diffy = line.end[1] - line.start[1];

    /* Pocni sa crtanjem. */
    glColor3f(0.0f, 0.0f, 0.0f);
    glBegin(GL_POINTS);

    /* Odredi po kojoj osi iterirati. */
    if (diffx > diffy) {

        /* Izracunaj parametre iteriranja po x. */
        int a = 2 * diffy;
        int yfrac = -diffx;
        int ycorr = 2 * yfrac;

        /* Iteriraj po x. */
        for (; x < line.end[0]; ++x) {
            glVertex2i(x, y);
            yfrac += a;
            if (yfrac >= 0) yfrac += ycorr, y++;
        }

    } else {

        /* Izracunaj parametre iteriranja po y. */
        int a = 2 * diffx;
        int xfrac = -diffy;
        int xcorr = 2 * xfrac;

        /* Iteriraj po y. */
        for (; y < line.end[1]; ++y) {
            glVertex2i(x, y);
            xfrac += a;
            if (xfrac >= 0) xfrac += xcorr, x++;
        }

    }

    /* Zavrsi sa crtanjem. */
    glEnd();

}

void bresenham_90_top(const Line& line) {

    /* Izracunaj zajednicke varijable. */
    int x = line.start[0];
    int y = line.start[1];
    int diffx = line.end[0] - line.start[0];
    int diffy = line.end[1] - line.start[1];

    /* Pocni sa crtanjem. */
    glColor3f(0.0f, 0.0f, 0.0f);
    glBegin(GL_POINTS);

    /* Odredi po kojoj osi iterirati. */
    if (diffx > -diffy) {

        /* Izracunaj parametre iteriranja po x. */
        int a = 2 * diffy;
        int yfrac = diffx;
        int ycorr = 2 * yfrac;

        /* Iteriraj po x. */
        for (; x < line.end[0]; ++x) {
            glVertex2i(x, y);
            yfrac += a;
            if (yfrac <= 0) yfrac += ycorr, y--;
        }

    } else {

        /* Izracunaj parametre iteriranja po y. */
        int a = 2 * diffx;
        int xfrac = diffy;
        int xcorr = 2 * xfrac;

        /* Iteriraj po y. */
        for (; y >= line.end[1]; --y) {
            glVertex2i(x, y);
            xfrac += a;
            if (xfrac >= 0) xfrac += xcorr, x++;
        }

    }

    /* Zavrsi sa crtanjem. */
    glEnd();

}

inline std::uint8_t computeCode(const Vector2i& point) {

    /* Izracunaj statusne zastavice. */
    std::uint8_t code = 0;
	if (point[0] < g_CutoutTopLeft[0])
		code |= 0x1;
	else if (point[0] > g_CutoutBottomRight[0])
		code |= 0x2;
	if (point[1] < g_CutoutTopLeft[1])
		code |= 0x4;
	else if (point[1] > g_CutoutBottomRight[1])
		code |= 0x8;

	return code;
}

bool clipLine(Line& line) {

    /* Izracunavanje zastavica za obe tocke. */
    std::uint8_t codeStart = computeCode(line.start);
    std::uint8_t codeEnd = computeCode(line.end);

    /* Ponavljaj dok ne odsjeces sve dijelove. */
    while (true) {
        if (!(codeStart | codeEnd)) {
            return true;
        } else if (codeStart & codeEnd) {
            return false;
        } else {
            Vector2i point;
            std::uint8_t codeOut = codeStart ? codeStart : codeEnd;
            if (codeOut & 0x8) {
				point[0] = line.start[0] + (line.end[0] - line.start[0]) * (g_CutoutBottomRight[1] - line.start[1]) / (line.end[1] - line.start[1]);
				point[1] = g_CutoutBottomRight[1];
			} else if (codeOut & 0x4) {
				point[0] = line.start[0] + (line.end[0] - line.start[0]) * (g_CutoutTopLeft[1] - line.start[1]) / (line.end[1] - line.start[1]);
				point[1] = g_CutoutTopLeft[1];
			} else if (codeOut & 0x2) {
				point[1] = line.start[1] + (line.end[1] - line.start[1]) * (g_CutoutBottomRight[0] - line.start[0]) / (line.end[0] - line.start[0]);
				point[0] = g_CutoutBottomRight[0];
			} else if (codeOut & 0x1) {
				point[1] = line.start[1] + (line.end[1] - line.start[1]) * (g_CutoutTopLeft[0] - line.start[0]) / (line.end[0] - line.start[0]);
				point[0] = g_CutoutTopLeft[0];
			}
			if (codeOut == codeStart) {
                line.start = point;
				codeStart = computeCode(line.start);
			} else {
				line.end = point;
				codeEnd = computeCode(line.end);
			}
        }
    }

}

void bresenham(Line line) {

    /* Kopirana linija. */
    Line copy = line;

    /* Odrezi visak linije ako je to omoguceno. */
    if (!g_Cutout || clipLine(line)) {

        /* Zamjeni pocetnu i krajnju liniju ako je u suprotnom smjeru. */
        if (line.end[0] < line.start[0])
            std::swap(line.start, line.end);

        /* Crtaj za +90 ili -90 stupnjeva nagiba. */
        if (line.end[1] > line.start[1])
            bresenham_90_bottom(line);
        else
            bresenham_90_top (line);

    }

    /* Crtaj ako je kontrola ukljucena. */
    if (g_Control) {
        glBegin(GL_LINES);
        glColor3f(1.0f, 0.0f, 0.0f);
        glVertex2i(copy.start[0] + 4, copy.start[1] + 4);
        glVertex2i(copy.end[0] + 4, copy.end[1] + 4);
        glEnd();
    }

}

void resizeCbck(GLFWwindow* win, int width, int height) {

    /* Izracunaj velicinu pravokutnika odsijecanja. */
    g_CutoutTopLeft = { width / 4, height / 4 };
    g_CutoutBottomRight = 3 * g_CutoutTopLeft;

    /* Promijeni velicinu viewporta i ortografske projekcije. */
    glViewport(0, 0, width, height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0, width, height, 0, 0, 1);
    glMatrixMode(GL_MODELVIEW);

}

void keyCbck(GLFWwindow* win, int key, int scancode, int action, int mods) {

    /* Tipke za postavljanje kontrole te odsijecanja. */
    if (action == GLFW_PRESS) {
        if (key == GLFW_KEY_K) {
            g_Control = !g_Control;
        } else if (key == GLFW_KEY_O) {
            g_Cutout = !g_Cutout;
        } else if (key == GLFW_KEY_C) {
            g_Lines.clear();
        }
    }

}

void mousebtnCbck(GLFWwindow* win, int button, int action, int mods) {

    /* Pocni i prekini crtanje. */
    if (button == GLFW_MOUSE_BUTTON_LEFT) {
        if (action == GLFW_PRESS) {
            double x, y;
            glfwGetCursorPos(g_Window, &x, &y);
            g_TempLine.start = { (int)x, (int)y };
            g_Pressed = true;
        } else if (action == GLFW_RELEASE) {
            g_Lines.push_back(g_TempLine);
            g_Pressed = false;
        }
    }

}

void update() {

    /* Postavi lokaciju */
    if (g_Pressed) {
        double x, y;
        glfwGetCursorPos(g_Window, &x, &y);
        g_TempLine.end = { (int)x, (int)y };
    }

}

void render() {

    /* Pocisti i ucitaj matricu identiteta. */
    glClear(GL_COLOR_BUFFER_BIT);
    glLoadIdentity();

    /* Prikazi granice odsijecanja. */
    if (g_Cutout) {
        glBegin(GL_LINE_LOOP);
        glColor3f(0.0f, 1.0f, 0.0f);
        glVertex2i(g_CutoutBottomRight[0], g_CutoutTopLeft[1]);
        glVertex2i(g_CutoutTopLeft[0], g_CutoutTopLeft[1]);
        glVertex2i(g_CutoutTopLeft[0], g_CutoutBottomRight[1]);
        glVertex2i(g_CutoutBottomRight[0], g_CutoutBottomRight[1]);
        glEnd();
    }

    /* Crtaj postojece linije. */
    for (const Line& line : g_Lines) {
        bresenham(line);
    }

    /* Crtaj trenutnu liniju. */
    if (g_Pressed)
        bresenham(g_TempLine);

    /* Zamjeni medjuspremnike. */
    glfwSwapBuffers(g_Window);

}

void errorCbck(int c, const char* msg) {

    /* Ispisi pogresku. */
    std::cerr << "ERROR " << c << ": " << msg << std::endl;

}

int main() {

    /* Iniciraj GLFW prozor. */
    glfwSetErrorCallback(errorCbck);
    glfwInit();
    g_Window = glfwCreateWindow(500, 500, "Bresenham & Cohen-Sutherland algorithms", nullptr, nullptr);
    glfwSetWindowSizeCallback(g_Window, resizeCbck);
    glfwSetKeyCallback(g_Window, keyCbck);
    glfwSetMouseButtonCallback(g_Window, mousebtnCbck);
    glfwMakeContextCurrent(g_Window);

    /* Postavi projekcijsku matricu. */
    resizeCbck(g_Window, 500, 500);

    /* Postavi boju ciscenja. */
    glClearColor(1.0f, 1.0f, 1.0f, 1.0f);

    /* Glavna petlja. */
    while (!glfwWindowShouldClose(g_Window)) {
        glfwPollEvents();
        update();
        render();
    }

    /* Terminiraj GLFW. */
    glfwTerminate();
    return 0;

}
