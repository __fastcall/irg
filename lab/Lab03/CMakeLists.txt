# Build labs if option is ticked
if(BUILD_LABS)

    # Add task 1 program
    add_executable(Lab03_Task1 WIN32 ${CMAKE_CURRENT_SOURCE_DIR}/Task1.cxx)
    target_link_libraries(Lab03_Task1 ${DEP_LIBS_GLFW})

endif()
