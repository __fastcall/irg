#include <IRG/Math.hxx>
#include <IRG/Model.hxx>
#include <iostream>
#include <fstream>
#include <sstream>

using namespace IRG::Math;
using namespace IRG::Model;

int main(int argc, char** argv) {

    /* Path to model. */
    std::string filename;

    /* Read path to model from arguments or manually enter by hand. */
    if (argc >= 2) {
        filename = argv[1];
    } else {
        std::cout << "Unesi Wavefront OBJ datoteku: ";
        std::cin >> filename;
        std::string line;
        std::getline(std::cin, line);
    }

    /* Terminate application if file doesn't exist. */
    if (!std::ifstream(filename).good()) {
        std::cerr << "Datoteka '" << filename << "' ne postoji." << std::endl;
        return -1;
    }

    /* Load mesh and print statistic. */
    Mesh mesh = Wavefront::Load(filename);
    std::cout << "Uspjesno ucitano " << mesh.GetVertices().size() << " vrhova i " << mesh.GetFaces().size() << " lica" << std::endl << std::endl;

    /* Main loop. */
    while (true) {

        /* Write command symbol. */
        std::cout << "> ";

        /* Read the entire line. */
        std::string line;
        std::getline(std::cin, line);
        std::istringstream ss(line);

        /* Read entered command. */
        std::string command;
        ss >> command;

        /* Decode command. */
        if (command == "help") {

            /* Print all commands. */
            std::cout
                << "Lista svih podrzanih naredbi: " << std::endl
                << "  help             = prikazuje ovaj ekran" << std::endl
                << "  test <x> <y> <z> = provjeri odnos sa tockom" << std::endl
                << "  center           = centriraj objekt" << std::endl
                << "  normalize        = normaliziraj (i centriraj) objekt" << std::endl
                << "  print            = ispisi sadrzaj modela" << std::endl
                << "  dump <objfile>   = zapisi sadrzaj modela u datoteku" << std::endl
                << "  exit             = izlaz" << std::endl
                << std::endl;

        } else if (command == "test") {

            /* Test point */
            Vector3f point;

            /* Read point coordinates. */
            for (std::size_t i = 0; i < 3; ++i) {
                ss >> point[i];
            }

            /* Find point relation. */
            PositionState relation = mesh.TestPosition(point);

            /* Print point relation. */
            switch (relation) {
            case PositionState::Inside:
                std::cout << "Tocka " << point << " se nalazi unutar objekta." << std::endl;
                break;
            case PositionState::OnEdge:
                std::cout << "Tocka " << point << " se nalazi na rubu objekta." << std::endl;
                break;
            case PositionState::Outside:
                std::cout << "Tocka " << point << " se nalazi izvan objekta." << std::endl;
                break;
            }

        } else if (command == "center") {

            /* Center mesh vertices. */
            mesh.CenterVertices();

            /* Print message. */
            std::cout << "Objekt centriran." << std::endl;

        } else if (command == "normalize") {

            /* Normalize mesh vertices. */
            mesh.NormalizeVertices();

            /* Print message. */
            std::cout << "Objekt normaliziran." << std::endl;

        } else if (command == "print") {

            /* Print mesh to screen. */
            Wavefront::Dump(mesh, std::cout);

        } else if (command == "dump") {

            /* Read output file name. */
            std::string output;
            ss >> output;

            /* Continue if no file name is given. */
            if (output.find_first_not_of(' ') == std::string::npos) {
                std::cout << "nepravilna naredba -- dump <objfile>" << std::endl;
                continue;
            }

            /* Query if certain. */
            std::cout << "Zapisi u '" << output << "' datoteku? (y/N) ";

            /* Get response. */
            std::string response;
            std::cin >> response;

            /* Write if positive response. */
            if (response == "y") {
                std::ofstream ofs(output);
                Wavefront::Dump(mesh, ofs);
                std::cout << "Zapisano!" << std::endl;
            } else {
                std::cout << "Prekinuto." << std::endl;
            }

            /* Eat rest of the line. */
            std::getline(std::cin, response);


        } else if (command == "exit") {

            /* Exit program. */
            std::cout << "Dovidenja." << std::endl;
            break;

        } else if (command.find_first_not_of(' ') != std::string::npos) {

            /* Print error message. */
            std::cout << "nepodrzana naredba -- " << command << std::endl << "Upisite 'help' za popis naredbi." << std::endl;

        }

    }

    return 0;

}
