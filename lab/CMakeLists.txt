# Add option to build labs
option(BUILD_LABS "Build IRG lab programs" ON)

# Build labs if option is ticked
if(BUILD_LABS)

    # Add Lab 1 subdirectory
    add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/Lab01)

    # Add Lab 2 subdirectory
    add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/Lab02)

    # Add Lab 3 subdirectory
    add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/Lab03)

    # Add Lab 4 subdirectory
    add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/Lab04)

    # Add Lab 5 subdirectory
    add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/Lab05)

    # Add Lab 6 subdirectory
    add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/Lab06)

    # Add Lab 7 subdirectory
    add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/Lab07)

    # Add Lab 8 subdirectory
    add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/Lab08)

    # Add Lab 9 subdirectory
    add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/Lab09)

    # Add Lab 10 subdirectory
    add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/Lab10)

endif()
