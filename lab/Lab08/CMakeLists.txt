# Build labs if option is ticked
if(BUILD_LABS)

    # Add task 1 program
    add_executable(Lab08_Task1 ${CMAKE_CURRENT_SOURCE_DIR}/Task1.cxx)
    target_link_libraries(Lab08_Task1 IRG ${DEP_LIBS_GLFW})

endif()
