#include <IRG/Math.hxx>
#include <GLFW/glfw3.h>
#include <GL/gl.h>
#include <vector>
#include <iostream>
#include <cmath>

#define WIN_WIDTH 640
#define WIN_HEIGHT 480
#define POINT_SZ 8.0f
#define MAX_PTS 10
#define MIN_LINE_FRAGS 5
#define MAX_LINE_FRAGS 100
#define INC_LINE_FRAGS 5

#define IS_WITHIN_PT(xpos, ypos, xpt, ypt) (((xpos) >= ((xpt) - POINT_SZ / 2.0f)) && ((xpos) <= ((xpt) + POINT_SZ / 2.0f)) && ((ypos) >= ((ypt) - POINT_SZ / 2.0f)) && ((ypos) <= ((ypt) + POINT_SZ / 2.0f)))

static GLFWwindow* g_Window = nullptr;
static std::vector<IRG::Math::Vector2d> g_ControlPoints;
static IRG::Math::Vector2d* g_HoveredPoint = nullptr;
static bool g_IsMoving = false;
static int g_LineFrags = 35;
static bool g_ShowControlPolygon = true, g_ShowApproxCurve = true, g_ShowInterpCurve = true;

static void resizeCbck(GLFWwindow* win, int width, int height) {

    /* Reset viewport. */
    glViewport(0, 0, width, height);

    /* Reset ortho projection. */
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0, width, height, 0, 0, 1);

}

static void keyCbck(GLFWwindow* win, int key, int scancode, int action, int mods) {

    /* Ignore when moving point. */
    if (g_IsMoving) return;

    /* Accept only key press events. */
    if (action == GLFW_PRESS) {

        /* Escape key clears all defined control points. */
        if (key == GLFW_KEY_ESCAPE) {
            g_ControlPoints.clear();
        /* Keypad plus increases curve segments. */
        } else if (key == GLFW_KEY_KP_ADD) {
            if (g_LineFrags < MAX_LINE_FRAGS) g_LineFrags += INC_LINE_FRAGS;
        /* Keypad minus decreases curve segments. */
        } else if (key == GLFW_KEY_KP_SUBTRACT) {
            if (g_LineFrags > MIN_LINE_FRAGS) g_LineFrags -= INC_LINE_FRAGS;
        /* Key C shows/hides control polygon. */
        } else if (key == GLFW_KEY_C) {
            g_ShowControlPolygon = !g_ShowControlPolygon;
        /* Key A shows/hides approximated Bezier curve. */
        } else if (key == GLFW_KEY_A) {
            g_ShowApproxCurve = !g_ShowApproxCurve;
        /* Key I shows/hides interpolated Bezier curve. */
        } else if (key == GLFW_KEY_I) {
            g_ShowInterpCurve = !g_ShowInterpCurve;
        }

    }

}

static void mousebtnCbck(GLFWwindow* win, int button, int action, int mods) {

    /* Check if mouse was pressed. */
    if (action == GLFW_PRESS) {

        /* Ignore when moving point. */
        if (g_IsMoving) return;

        /* Left mouse button adds new control point. */
        if ((button == GLFW_MOUSE_BUTTON_LEFT) && (g_ControlPoints.size() < MAX_PTS)) {
            double xpos, ypos;
            glfwGetCursorPos(win, &xpos, &ypos);
            g_ControlPoints.push_back({xpos, ypos});
        
        /* Right mouse button starts moving hovered control point. */
        } else if ((button == GLFW_MOUSE_BUTTON_RIGHT) && (g_HoveredPoint != nullptr)) {
            g_IsMoving = true;
        }

    /* Check if mouse was released. */
    } else if (action == GLFW_RELEASE) {

        /* Right mouse button stops moving hovered control point. */
        if (button == GLFW_MOUSE_BUTTON_RIGHT) {
            g_IsMoving = false;
        }

    }

}

static IRG::Math::Vector2d calcApproxPoint(const std::vector<IRG::Math::Vector2d>& cp, double t) {

    /* Impossible case, only one or no control points present. */
    if (cp.size() <= 1) return IRG::Math::Vector2d();

    /* Trivial case, only one control polygon segment. */
    if (cp.size() == 2) {
        return cp[0] + (cp[1] - cp[0]) * t;
    }

    /* Recursive case, interpolate all segments than call function recursively. */
    std::vector<IRG::Math::Vector2d> newCp;
    for (int i = 1; i < cp.size(); ++i) {
        IRG::Math::Vector2d seg = cp[i - 1] + (cp[i] - cp[i - 1]) * t;
        newCp.push_back(seg);
    }
    return calcApproxPoint(newCp, t);

}

static void internalRenderApproxCurve(const std::vector<IRG::Math::Vector2d>& cp, float r, float g, float b) {

    /* Draw approximated Bezier curve. */
    glBegin(GL_LINE_STRIP);
    glColor3f(r, g, b);
    for (int i = 0; i <= g_LineFrags; ++i) {
        double t = (double)i / g_LineFrags;
        IRG::Math::Vector2d ap = calcApproxPoint(cp, t);
        glVertex2d(ap[0], ap[1]);
    }
    glEnd();

}

static void renderApproxCurve() {

    /* Call internal function. */
    internalRenderApproxCurve(g_ControlPoints, 0.0f, 0.0f, 1.0f);

}

template<size_t N>
static void internalRenderInterpCurve() {

    /* Weights matrix. */
    IRG::Math::Matrixd<N, N> f;

    /* Calculate (N - 1)!. */
    double Nf = 2;
    for (int k = 3; k <= (N - 1); ++k) Nf *= k;

    /* Iterate by rows. */
    for (int i = 0; i < N; ++i) {

        /* Calculate (N - 1) nCr i. */
        double nCr = Nf;
        for (int k = 2; k <= i; ++k) nCr /= k;
        for (int k = 2; k <= (N - 1) - i; ++k) nCr /= k;

        /* Iterate by column. */
        for (int j = 0; j < N; ++j) {

            /* Calculate t. */
            double t = (double)j / (N - 1);

            /* Fill matrix element. */
            f[i][j] = nCr * std::pow(t, i) * std::pow(1 - t, (N - 1) - i);

        }

    }

    /* Transpose then invert the matrix. */
    f.Transpose();
    f.Invert();

    /* Fill points matrix. */
    IRG::Math::Matrixd<N, 2> p;
    for (int i = 0; i < N; ++i) {
        p[i][0] = g_ControlPoints[i][0];
        p[i][1] = g_ControlPoints[i][1];
    }

    /* Calculate interpolated control points. */
    IRG::Math::Matrixd<N, 2> r = f * p;

    /* Extract interpolated control points. */
    std::vector<IRG::Math::Vector2d> cp;
    for (int i = 0; i < N; ++i) {
        cp.push_back({r[i][0], r[i][1]});
    }

    /* Draw curve. */
    internalRenderApproxCurve(cp, 0.0f, 0.0f, 0.0f);

}

static void renderInterpCurve() {

    /* I must deal with these shenanigans due to my primitive types having templated dimension parameters. */
    switch (g_ControlPoints.size()) {
    case 0: case 1: break;
    case 2: internalRenderApproxCurve(g_ControlPoints, 0.0f, 0.0f, 0.0f); break;
    case 3: internalRenderInterpCurve<3>(); break;
    case 4: internalRenderInterpCurve<4>(); break;
    case 5: internalRenderInterpCurve<5>(); break;
    case 6: internalRenderInterpCurve<6>(); break;
    case 7: internalRenderInterpCurve<7>(); break;
    case 8: internalRenderInterpCurve<8>(); break;
    case 9: internalRenderInterpCurve<9>(); break;
    case 10: internalRenderInterpCurve<10>(); break;
    default: std::cerr << "Missing case for " << g_ControlPoints.size() << " control points." << std::endl; exit(1);
    }

}

static void render() {

    /* Get mouse position. */
    double xpos, ypos;
    glfwGetCursorPos(g_Window, &xpos, &ypos);

    /* Move hovered control point to current mouse position if in moving state. */
    if (g_IsMoving) { (*g_HoveredPoint)[0] = xpos; (*g_HoveredPoint)[1] = ypos; }

    /* Clear color buffer. */
    glClear(GL_COLOR_BUFFER_BIT);

    /* Draw control polygon. */
    if (g_ShowControlPolygon) {
        glBegin(GL_LINE_STRIP);
        glColor3f(1.0f, 0.0f, 0.0f);
        for (const IRG::Math::Vector2d& cp : g_ControlPoints) {
            glVertex2d(cp[0], cp[1]);
        }
        glEnd();
    }

    /* Render curves. */
    if (g_ShowApproxCurve) renderApproxCurve();
    if (g_ShowInterpCurve) renderInterpCurve();

    /* Unset hovered control point if not in moving state. */
    if (!g_IsMoving) g_HoveredPoint = nullptr;

    /* Draw control points. */
    glBegin(GL_POINTS);
    for (IRG::Math::Vector2d& cp : g_ControlPoints) {
        glColor3f(1.0f, 0.0f, 0.0f);
        if (((!g_IsMoving) && (IS_WITHIN_PT(xpos, ypos, cp[0], cp[1]))) || ((g_IsMoving) && (g_HoveredPoint == &cp))) {
            g_HoveredPoint = &cp;
            glColor3f(0.85f, 0.65f, 0.0f);
        }
        glVertex2d(cp[0], cp[1]);
    }
    glEnd();

    /* Swap buffers. */
    glfwSwapBuffers(g_Window);

}

int main(int argc, char** argv) {

    /* Initialize GLFW. */
    glfwInit();

    /* Create window and bind events. */
    g_Window = glfwCreateWindow(WIN_WIDTH, WIN_HEIGHT, "Lab 8", nullptr, nullptr);
    glfwSetWindowSizeCallback(g_Window, resizeCbck);
    glfwSetKeyCallback(g_Window, keyCbck);
    glfwSetMouseButtonCallback(g_Window, mousebtnCbck);

    /* Make GL context current. */
    glfwMakeContextCurrent(g_Window);

    /* Trigger width callback. */
    resizeCbck(g_Window, WIN_WIDTH, WIN_HEIGHT);

    /* Set clear color and set point size. */
    glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
    glPointSize(POINT_SZ);

    /* Event-render loop. */
    while (!glfwWindowShouldClose(g_Window)) {
        glfwPollEvents();
        render();
    }

    /* Exit success. */
    return 0;

}
