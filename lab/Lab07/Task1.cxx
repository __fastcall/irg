#include "../Lab06/DrawWindow.hxx"
#include <GL/gl.h>
#include <GL/glu.h>
#include <IRG/Math.hxx>
#include <cmath>

namespace Lab07 {

    static IRG::Math::Vector3f g_Eye({3.0f, 4.0f, 1.0f});
    static IRG::Math::Vector3f g_Center({0.0f, 0.0f, 0.0f});
    static IRG::Math::Vector3f g_Up({0.0f, 1.0f, 0.0f});

    class Task1_Window : public Lab06::DrawWindow {

    public:

        Task1_Window(int argc, char** argv) : DrawWindow(argc, argv) { }

    protected:

        virtual void Init() override {

            /* Set clear color. */
            glClearColor(0.0f, 1.0f, 0.0f, 1.0f);

            /* Enable face culling of back faces (CW faces). */
            glEnable(GL_CULL_FACE);
            glFrontFace(GL_CCW);
            glCullFace(GL_BACK);

            /* Draw polygons as lines. */
            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

        }

        virtual void Render(float deltaTime) override {

            /* Set up projection matrix. */
            glMatrixMode(GL_PROJECTION);
            glLoadIdentity();
            glFrustum(-0.5, 0.5, -0.5, 0.5, 1.0, 100.0);

            /* Set up model-view matrix. */
            glMatrixMode(GL_MODELVIEW);
            glLoadIdentity();
            IRG::Math::Vector3f eye({InvSinAngle * std::cos(Angle), g_Eye[1], InvSinAngle * std::sin(Angle)});
            gluLookAt(eye[0], eye[1], eye[2], g_Center[0], g_Center[1], g_Center[2], g_Up[0], g_Up[1], g_Up[2]);

            /* Get vertices and faces. */
            const std::vector<IRG::Math::Vector3f>& vertices = GetMesh().GetVertices();
            const std::vector<std::vector<IRG::Math::Vector3i>>& faces = GetMesh().GetFaces();

            /* Clear screen. */
            glClear(GL_COLOR_BUFFER_BIT);

            /* Draw all faces. */
            glColor3f(1.0f, 0.0f, 0.0f);
            for (const std::vector<IRG::Math::Vector3i>& face : faces) {
                glBegin(GL_POLYGON);
                for (const IRG::Math::Vector3i& index : face) {
                    glVertex3f(vertices[index[0]][0], vertices[index[0]][1], vertices[index[0]][2]);
                }
                glEnd();
            }

        }

    };

}

int main(int argc, char** argv) {

    /* Create window and enter loop. */
    Lab07::Task1_Window window(argc, argv);
    window.EnterLoop("Lab 7");
    return 0;

}
