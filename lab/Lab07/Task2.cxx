#include "../Lab06/DrawWindow.hxx"
#include <GL/gl.h>
#include <GL/glu.h>
#include <GLFW/glfw3.h>
#include <IRG/Math.hxx>
#include <iostream>
#include <cmath>

namespace Lab07 {

    static IRG::Math::Vector3f g_Eye({3.0f, 4.0f, 1.0f});
    static IRG::Math::Vector3f g_Center({0.0f, 0.0f, 0.0f});
    static IRG::Math::Vector3f g_Up({0.0f, 1.0f, 0.0f});

    class Task2_Window : public Lab06::DrawWindow {

    public:

        Task2_Window(int argc, char** argv) : DrawWindow(argc, argv) { }

    protected:

        virtual void Init() override {

            /* Set key pressed callback. */
            Task2_Window::KeyPressedCallback = KeyPressed;

            /* Set clear color. */
            glClearColor(0.0f, 1.0f, 0.0f, 1.0f);

        }

        virtual void Render(float deltaTime) override {

            /* Set up projection matrix (identity). */
            glMatrixMode(GL_PROJECTION);
            glLoadIdentity();

            /* Set up model-view matrix (identity). */
            glMatrixMode(GL_MODELVIEW);
            glLoadIdentity();

            /* Get vertices and faces. */
            const std::vector<IRG::Math::Vector3f>& vertices = GetMesh().GetVertices();
            const std::vector<std::vector<IRG::Math::Vector3i>>& faces = GetMesh().GetFaces();

            /* Calculate the required matrices (my methods returns a premultiplied-convention matrix so a transpose is required). */
            IRG::Math::Vector3f eye({InvSinAngle * std::cos(Angle), g_Eye[1], InvSinAngle * std::sin(Angle)});
            IRG::Math::Matrix4f lookAt = IRG::Math::MatrixUtil4f::CreateLookAtMatrix(eye, g_Center, g_Up).GetTransposed();
            IRG::Math::Matrix4f perspective = IRG::Math::MatrixUtil4f::CreatePerspectiveMatrixOffCenter(-0.5f, 0.5f, -0.5f, 0.5f, 1.0f, 100.0f).GetTransposed();

            /* Clear screen. */
            glClear(GL_COLOR_BUFFER_BIT);

            /* Calculate accumulated transformation. */
            IRG::Math::Matrix4f transformation = lookAt * perspective;

            /* Draw all faces. */
            glColor3f(1.0f, 0.0f, 0.0f);
            for (const std::vector<IRG::Math::Vector3i>& face : faces) {
                if (!Task2_Window::m_IsFrontFaceAlgorithm(face, vertices, eye, transformation)) continue;
                glBegin(GL_LINE_LOOP);
                for (const IRG::Math::Vector3i& index : face) {
                    IRG::Math::Vector4f vertex = IRG::Math::Vector4f::ToHomogeneous(vertices[index[0]]);
                    IRG::Math::Vector4f transformedVertex = vertex * transformation;
                    IRG::Math::Vector3f displayedVertex = IRG::Math::Vector3f::FromHomogeneous(transformedVertex);
                    glVertex3f(displayedVertex[0], displayedVertex[1], displayedVertex[2]);
                }
                glEnd();
            }

        }

    private:

        static void KeyPressed(int key) {

            /* Switch algorithm. */
            switch (key) {
            case GLFW_KEY_1:
            case GLFW_KEY_KP_1:
                std::cout << "Algorithm: Disabled" << std::endl;
                m_IsFrontFaceAlgorithm = IsFrontFace1;
                break;
            case GLFW_KEY_2:
            case GLFW_KEY_KP_2:
                std::cout << "Algorithm: No. 1" << std::endl;
                m_IsFrontFaceAlgorithm = IsFrontFace2;
                break;
            case GLFW_KEY_3:
            case GLFW_KEY_KP_3:
                std::cout << "Algorithm: No. 2" << std::endl;
                m_IsFrontFaceAlgorithm = IsFrontFace3;
                break;
            case GLFW_KEY_4:
            case GLFW_KEY_KP_4:
                std::cout << "Algorithm: No. 3" << std::endl;
                m_IsFrontFaceAlgorithm = IsFrontFace4;
                break;
            }

        }

        static bool IsFrontFace1(const std::vector<IRG::Math::Vector3i>& face, const std::vector<IRG::Math::Vector3f>& vertices, const IRG::Math::Vector3f& eye, const IRG::Math::Matrix4f& transformation) {

            /* No algorithm (display all faces). */
            return true;

        }

        static bool IsFrontFace2(const std::vector<IRG::Math::Vector3i>& face, const std::vector<IRG::Math::Vector3f>& vertices, const IRG::Math::Vector3f& eye, const IRG::Math::Matrix4f& transformation) {

            /* Algorithm no. 1 (assume CCW). */
            const IRG::Math::Vector3f& vertex0 = vertices[face[0][0]];
            const IRG::Math::Vector3f& vertex1 = vertices[face[1][0]];
            const IRG::Math::Vector3f& vertex2 = vertices[face[2][0]];

            /* Calculate face normal. */
            IRG::Math::Vector3f edge0 = vertex1 - vertex0;
            IRG::Math::Vector3f edge1 = vertex2 - vertex0;
            IRG::Math::Vector3f normal = IRG::Math::Vector3f::CrossProduct(edge0, edge1).GetNormalized();

            /* Calculate plane equation. */
            IRG::Math::Vector4f plane(normal, -normal * vertex0);

            /* Check if eye is above the plane. */
            return plane * IRG::Math::Vector4f::ToHomogeneous(eye) > 1e-6;

        }

        static bool IsFrontFace3(const std::vector<IRG::Math::Vector3i>& face, const std::vector<IRG::Math::Vector3f>& vertices, const IRG::Math::Vector3f& eye, const IRG::Math::Matrix4f& transformation) {
            
            /* Algorithm no. 2 (assume CCW). */
            const IRG::Math::Vector3f& vertex0 = vertices[face[0][0]];
            const IRG::Math::Vector3f& vertex1 = vertices[face[1][0]];
            const IRG::Math::Vector3f& vertex2 = vertices[face[2][0]];

            /* Calculate face normal. */
            IRG::Math::Vector3f edge0 = vertex1 - vertex0;
            IRG::Math::Vector3f edge1 = vertex2 - vertex0;
            IRG::Math::Vector3f normal = IRG::Math::Vector3f::CrossProduct(edge0, edge1).GetNormalized();

            /* Calculate normalized vector from any face point to eye. */
            IRG::Math::Vector3f toEye = (eye - vertex0).GetNormalized();

            /* Check if angle is within <-PI / 2.0f, PI / 2.0f>. */
            return (normal * toEye) > 1e-6;

        }

        static bool IsFrontFace4(const std::vector<IRG::Math::Vector3i>& face, const std::vector<IRG::Math::Vector3f>& vertices, const IRG::Math::Vector3f& eye, const IRG::Math::Matrix4f& transformation) {
            
            /* Algorithm no. 3 (assume CCW). */
            std::vector<IRG::Math::Vector3f> displayedVertices;
            displayedVertices.reserve(face.size());

            /* Calculate displayed vertices. */
            for (const IRG::Math::Vector3i& index : face) {
                const IRG::Math::Vector3f& vertex = vertices[index[0]];
                IRG::Math::Vector3f transformedVertex = IRG::Math::Vector3f::FromHomogeneous(IRG::Math::Vector4f::ToHomogeneous(vertex) * transformation);
                displayedVertices.push_back({transformedVertex[0], transformedVertex[1], 1.0f});
            }

            /* Test if any displayed vertex is below preceeding edge. */
            for (int i = 0; i < displayedVertices.size(); ++i) {
                IRG::Math::Vector3f edgeEq = IRG::Math::Vector3f::CrossProduct(displayedVertices[(i - 2) % displayedVertices.size()], displayedVertices[(i - 1) % displayedVertices.size()]);
                if (edgeEq * displayedVertices[i] < -1e-6) return false;
            }

            /* Face is front. */
            return true;

        }

        static bool (*m_IsFrontFaceAlgorithm)(const std::vector<IRG::Math::Vector3i>& face, const std::vector<IRG::Math::Vector3f>& vertices, const IRG::Math::Vector3f& eye, const IRG::Math::Matrix4f& transformation);

    };

    /* Set default algorithm. */
    bool (*Task2_Window::m_IsFrontFaceAlgorithm)(const std::vector<IRG::Math::Vector3i>& face, const std::vector<IRG::Math::Vector3f>& vertices, const IRG::Math::Vector3f& eye, const IRG::Math::Matrix4f& transformation) = Task2_Window::IsFrontFace1;

}

int main(int argc, char** argv) {

    /* Create window and enter loop. */
    Lab07::Task2_Window window(argc, argv);
    window.EnterLoop("Lab 7");
    return 0;

}
