#include "FractalWindow.hxx"
#include <GLFW/glfw3.h>
#include <GL/gl.h>
#include <complex>
#include <cmath>

/**
 * @brief Represents a Mandelbrot fractal window.
 */
class MandelbrotFractal : public Task10::FractalWindow {

public:

    /**
     * @brief Construct a new Mandelbrot fractal window.
     */
    MandelbrotFractal() : FractalWindow(640, 480, "Task 10 - Mandelbrot"), m_UseSecondPower(true), m_IsMonochrome(false) { }

protected:

    /**
     * @brief Called when keyboard key is pressed.
     * @param key Keyboard key.
     * @param scancode Key scancode.
     * @param action Key action.
     * @param mods Key modifiers.
     */
    virtual void WindowKey(int key, int scancode, int action, int mods) override {

        /* Only react to key press events. */
        if (action == GLFW_PRESS) {

            /* Switch on key. */
            switch (key) {

            /* Use second power on key 1. */
            case GLFW_KEY_1:
            case GLFW_KEY_KP_1:
                m_UseSecondPower = true;
                m_ShouldRedraw = true;
                return;

            /* Use third power on key 2. */
            case GLFW_KEY_2:
            case GLFW_KEY_KP_2:
                m_UseSecondPower = false;
                m_ShouldRedraw = true;
                return;

            /* Set monochrome on key B. */
            case GLFW_KEY_B:
                m_IsMonochrome = true;
                m_ShouldRedraw = true;
                return;

            /* Set coloured on key C. */
            case GLFW_KEY_C:
                m_IsMonochrome = false;
                m_ShouldRedraw = true;
                return;

            }

        }

        /* Call base implementation. */
        FractalWindow::WindowKey(key, scancode, action, mods);

    }

    /**
    * @brief Renders Mandelbrot fractal.
    * @param width Window width.
    * @param height Window height.
    */
    virtual void Render(int width, int height) override {
        glPointSize(1.0f);
        glBegin(GL_POINTS);
        for (int i = 0; i < width; ++i) {
            for (int j = 0; j < height; ++j) {
                IRG::Math::Vector2d point = GetPoint((double)i, (double)j);
                std::complex<double> c(point[0], point[1]);
                int d = m_UseSecondPower ? TestDivergence(c) : TestDivergence2(c);
                if (d == -1)
                    glColor3f(0.0f, 0.0f, 0.0f);
                else if (m_IsMonochrome)
                    glColor3f(1.0f, 1.0f, 1.0f);
                else
                    glColor3d(
                        (double)(d - 1) / (double)(MandelbrotFractal::TestLimit - 1),
                        1.0 - (double)(d - 1) / (double)(MandelbrotFractal::TestLimit - 1),
                        0.5 * (double)(d - 1) / (double)(MandelbrotFractal::TestLimit - 1)
                    );
                glVertex3f(i, j, 0.0f);
            }
        }
        glEnd();
    }

private:

    /**
     * @brief Divergence test limit.
     */
    static int TestLimit;

    /**
     * @brief Test divergence for f(k + 1) = f(k)^2 + c.
     * @param c Complex number.
     * @return int Index at which the number diverges or -1 if it converges.
     */
    int TestDivergence(const std::complex<double>& c) {
        std::complex<double> z;
        for (int i = 1; i <= TestLimit; ++i) {
            z = std::pow(z, 2.0) + c;
            if (std::abs(z) >= 4.0) return i;
        }
        return -1;
    }

    /**
     * @brief Test divergence for f(k + 1) = f(k)^3 + c.
     * @param c Complex number.
     * @return int Index at which the number diverges or -1 if it converges.
     */
    int TestDivergence2(const std::complex<double>& c) {
        std::complex<double> z;
        for (int i = 1; i <= TestLimit; ++i) {
            z = std::pow(z, 3.0) + c;
            if (std::abs(z) >= 8.0) return i;
        }
        return -1;
    }

    /**
     * @brief Use the second power divergence test.
     */
    bool m_UseSecondPower;

    /**
     * @brief Is the fractal drawn in monochrome.
     */
    bool m_IsMonochrome;

};

/* Divergence test limit. */
int MandelbrotFractal::TestLimit = 32;

/* Entry function. */
int main() {

    /* Create and handle window. */
    MandelbrotFractal mf;
    mf.PerformMainLoop();

    /* Exit success. */
    return 0;

}
