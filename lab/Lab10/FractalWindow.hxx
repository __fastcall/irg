#ifndef __FRACTAL_WINDOW_HXX__
#define __FRACTAL_WINDOW_HXX__

#include <IRG/Math/Vector.hxx>
#include <stack>
#include <string>
#include <complex>

/**
 * @brief GLFW window class forward-declaration.
 */
class GLFWwindow;

/**
 * @brief Task 10 namespace.
 */
namespace Task10 {

    /**
     * @brief Represents a fractal window.
     */
    class FractalWindow {

    public:

        /**
         * @brief Construct a new fractal window.
         * @param width Window width.
         * @param height Window height.
         * @param title Window title.
         */
        FractalWindow(int width, int height, const std::string& title);

        /**
         * @brief Destroy fractal window.
         */
        virtual ~FractalWindow();

        /**
         * @brief Performs main event-render loop.
         * @note Blocks until the window is closed.
         */
        void PerformMainLoop();

    protected:

        /**
         * @brief Called when window is resized.
         * @param width New window width.
         * @param height New window height.
         */
        virtual void WindowSize(int width, int height);

        /**
         * @brief Called when mouse button is pressed.
         * @param button Mouse button.
         * @param action Button action.
         * @param mods Button modifiers.
         */
        virtual void WindowMouseButton(int button, int action, int mods);

        /**
         * @brief Called when keyboard key is pressed.
         * @param key Keyboard key.
         * @param scancode Key scancode.
         * @param action Key action.
         * @param mods Key modifiers.
         */
        virtual void WindowKey(int key, int scancode, int action, int mods);

        /**
         * @brief Called when window needs to be redrawn.
         * @param width Window width.
         * @param height Window height.
         */
        virtual void Render(int width, int height) = 0;

        /**
         * @brief Get the point at given position.
         * @param x Position x-axis value.
         * @param y Position y-axis value.
         * @return IRG::Math::Vector2d Point at given position.
         */
        IRG::Math::Vector2d GetPoint(double x, double y);

        /**
         * @brief Get the point at cursor position.
         * @return IRG::Math::Vector2d Point at cursor position.
         */
        IRG::Math::Vector2d GetCursorPoint();

        /**
         * @brief Zooming factor.
         */
        static double ZoomFactor;

        /**
         * @brief Initial drawing area bounds.
         */
        static IRG::Math::Vector2d BottomLeftBound, TopRightBound;

        /**
         * @brief Drawing area bounds.
         */
        IRG::Math::Vector2d m_BottomLeftBound, m_TopRightBound;

        /**
         * @brief Drawing area bounds stack.
         */
        std::stack<std::pair<IRG::Math::Vector2d, IRG::Math::Vector2d>> m_BoundStack;

        /**
         * @brief Should the image be redrawn.
         */
        bool m_ShouldRedraw;

    private:

        /**
         * @brief Called when window is resized.
         * @param window GLFW window.
         * @param width New window width.
         * @param height New window height.
         */
        static void WindowSize(GLFWwindow* window, int width, int height);

        /**
         * @brief Called when mouse button is pressed.
         * @param window GLFW window.
         * @param button Mouse button.
         * @param action Button action.
         * @param mods Button modifiers.
         */
        static void WindowMouseButton(GLFWwindow* window, int button, int action, int mods);

        /**
         * @brief Called when keyboard key is pressed.
         * @param window GLFW window.
         * @param key Keyboard key.
         * @param scancode Key scancode.
         * @param action Key action.
         * @param mods Key modifiers.
         */
        static void WindowKey(GLFWwindow* window, int key, int scancode, int action, int mods);

        /**
         * @brief Is the GLFW library initialized.
         */
        static bool IsGlfwInitialized;

        /**
         * @brief GLFW window.
         */
        GLFWwindow* m_Window;

    };

}

#endif
