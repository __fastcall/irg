#include "FractalWindow.hxx"
#include <GLFW/glfw3.h>
#include <cassert>

/**
 * @brief Task 10 namespace.
 */
namespace Task10 {

    /* Construct a new fractal window. */
    FractalWindow::FractalWindow(int width, int height, const std::string& title) : m_BottomLeftBound(BottomLeftBound), m_TopRightBound(TopRightBound), m_ShouldRedraw(true) {

        /* Initialize GLFW library. */
        if (!FractalWindow::IsGlfwInitialized) {
            assert(glfwInit() == GLFW_TRUE);
            FractalWindow::IsGlfwInitialized = true;
        }

        /* Create GLFW window and make GL context current. */
        assert((m_Window = glfwCreateWindow(width, height, title.c_str(), nullptr, nullptr)) != nullptr);
        glfwMakeContextCurrent(m_Window);

        /* Set GL clear color. */
        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

        /* Set user pointer. */
        glfwSetWindowUserPointer(m_Window, this);

        /* Register event callbacks. */
        glfwSetWindowSizeCallback(m_Window, FractalWindow::WindowSize);
        glfwSetMouseButtonCallback(m_Window, FractalWindow::WindowMouseButton);
        glfwSetKeyCallback(m_Window, FractalWindow::WindowKey);

        /* Set matrices. */
        glMatrixMode(GL_MODELVIEW_MATRIX);
        glLoadIdentity();
        glMatrixMode(GL_PROJECTION_MATRIX);
        glLoadIdentity();
        glOrtho(0.0, (double)width, (double)height, 0.0, 0.0, 1.0);

    }

    /* Destroy fractal window. */
    FractalWindow::~FractalWindow() {

        /* Destroy GLFW window. */
        glfwDestroyWindow(m_Window);
        m_Window = nullptr;

    }

    /* Performs main event-render loop. */
    void FractalWindow::PerformMainLoop() {

        /* Main event-render loop. */
        while (!glfwWindowShouldClose(m_Window)) {
            glfwPollEvents();
            if (m_ShouldRedraw) {
                int width, height;
                glfwGetWindowSize(m_Window, &width, &height);
                glClear(GL_COLOR_BUFFER_BIT);
                Render(width, height);
                glfwSwapBuffers(m_Window);
                m_ShouldRedraw = false;
            }
        }

    }

    /* Called when window is resized. */
    void FractalWindow::WindowSize(int width, int height) {

        /* Reset viewport. */
        glViewport(0, 0, width, height);

        /* Reset projection matrix. */
        glMatrixMode(GL_PROJECTION_MATRIX);
        glLoadIdentity();
        glOrtho(0.0, (double)width, (double)height, 0.0, 0.0, 1.0);

        /* Invalidate image. */
        m_ShouldRedraw = true;

    }

    /* Called when mouse button is pressed. */
    void FractalWindow::WindowMouseButton(int button, int action, int mods) {

        /* Only react to button press events. */
        if (action == GLFW_PRESS) {

            /* Point and offset. */
            IRG::Math::Vector2d point;
            IRG::Math::Vector2d offset;

            /* Switch on button. */
            switch (button) {

            /* Zoom in on left mouse button. */
            case GLFW_MOUSE_BUTTON_LEFT:
                m_BoundStack.push(std::pair<IRG::Math::Vector2d, IRG::Math::Vector2d>(m_BottomLeftBound, m_TopRightBound));
                point = GetCursorPoint();
                offset = { (m_TopRightBound[0] - m_BottomLeftBound[0]) / (2.0 * FractalWindow::ZoomFactor), (m_TopRightBound[1] - m_BottomLeftBound[1]) / (2.0 * FractalWindow::ZoomFactor) };
                m_BottomLeftBound = point - offset;
                m_TopRightBound = point + offset;
                m_ShouldRedraw = true;
                break;
            
            /* Zoom out on right mouse button. */
            case GLFW_MOUSE_BUTTON_RIGHT:
                if (m_BoundStack.empty()) {
                    m_BottomLeftBound = BottomLeftBound;
                    m_TopRightBound = TopRightBound;
                } else {
                    m_BottomLeftBound = m_BoundStack.top().first;
                    m_TopRightBound = m_BoundStack.top().second;
                    m_BoundStack.pop();
                    m_ShouldRedraw = true;
                }
                break;

            }

        }

    }

    /* Called when keyboard key is pressed. */
    void FractalWindow::WindowKey(int key, int scancode, int action, int mods) {

        /* Only react to key press events. */
        if (action == GLFW_PRESS) {

            /* Switch on key. */
            switch (key) {

            /* Zoom out on X. */
            case GLFW_KEY_X:
                if (m_BoundStack.empty()) {
                    m_BottomLeftBound = BottomLeftBound;
                    m_TopRightBound = TopRightBound;
                } else {
                    m_BottomLeftBound = m_BoundStack.top().first;
                    m_TopRightBound = m_BoundStack.top().second;
                    m_BoundStack.pop();
                    m_ShouldRedraw = true;
                }
                break;

            /* Reset bounds on escape key. */
            case GLFW_KEY_ESCAPE:
                m_BottomLeftBound = BottomLeftBound;
                m_TopRightBound = TopRightBound;
                while (!m_BoundStack.empty()) m_BoundStack.pop();
                m_ShouldRedraw = true;
                break;

            }

        }

    }

    /* Get the point at given position. */
    IRG::Math::Vector2d FractalWindow::GetPoint(double x, double y) {
        int width, height;
        glfwGetWindowSize(m_Window, &width, &height);
        return { m_BottomLeftBound[0] + (m_TopRightBound[0] - m_BottomLeftBound[0]) * (x / (double)width), m_BottomLeftBound[1] + (m_TopRightBound[1] - m_BottomLeftBound[1]) * (1.0 - y / (double)height) };
    }

    /* Get the point at cursor position. */
    IRG::Math::Vector2d FractalWindow::GetCursorPoint() {
        double x, y;
        glfwGetCursorPos(m_Window, &x, &y);
        return GetPoint(x, y);
    }

    /* Zooming factor. */
    double FractalWindow::ZoomFactor = 2.0;

    /* Initial drawing area bounds. */
    IRG::Math::Vector2d FractalWindow::BottomLeftBound = { -2, -1.2 }, FractalWindow::TopRightBound = { 1, 1.2 };

    /* Called when window is resized. */
    void FractalWindow::WindowSize(GLFWwindow* window, int width, int height) {

        /* Call member method. */
        FractalWindow* target = reinterpret_cast<FractalWindow*>(glfwGetWindowUserPointer(window));
        target->WindowSize(width, height);

    }

    /* Called when mouse button is pressed. */
    void FractalWindow::WindowMouseButton(GLFWwindow* window, int button, int action, int mods) {

        /* Call member method. */
        FractalWindow* target = reinterpret_cast<FractalWindow*>(glfwGetWindowUserPointer(window));
        target->WindowMouseButton(button, action, mods);

    }

    /* Called when keyboard key is pressed. */
    void FractalWindow::WindowKey(GLFWwindow* window, int key, int scancode, int action, int mods) {

        /* Call member method. */
        FractalWindow* target = reinterpret_cast<FractalWindow*>(glfwGetWindowUserPointer(window));
        target->WindowKey(key, scancode, action, mods);

    }

    /* Is the GLFW library initialized. */
    bool FractalWindow::IsGlfwInitialized = false;

}
