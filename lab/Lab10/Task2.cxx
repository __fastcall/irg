#include "FractalWindow.hxx"
#include <GL/gl.h>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <random>

/**
 * @brief Represents an IFS fractal window.
 */
class IFSFractal : public Task10::FractalWindow {

public:

    /**
     * @brief Construct a new IFS fractal window.
     * @param filename IFS text file name.
     */
    IFSFractal(const std::string& filename) : FractalWindow(640, 480, "Task 10 - IFS"), m_Random(m_RandomDevice()) {

        /* Open file stream. */
        std::ifstream ifs(filename);

        /* Parse points number line. */
        std::string line;
        while (ifs.peek() == '#') std::getline(ifs, line);
        std::getline(ifs, line);
        std::istringstream iss(line);
        iss >> m_PointsNumber;

        /* Parse limit line. */
        while (ifs.peek() == '#') std::getline(ifs, line);
        std::getline(ifs, line);
        iss = std::istringstream(line);
        iss >> m_Limit;

        /* Parse etas. */
        for (int i = 0; i < 2; ++i) {
            while (ifs.peek() == '#') std::getline(ifs, line);
            std::getline(ifs, line);
            iss = std::istringstream(line);
            for (int j = 0; j < 2; ++j)
                iss >> m_Eta[2 * i + j];
        }

        /* Parse transformations. */
        while (!ifs.eof()) {
            while (ifs.peek() == '#') std::getline(ifs, line);
            if (ifs.eof()) break;
            std::getline(ifs, line);
            iss = std::istringstream(line);
            std::vector<float> trans;
            for (int j = 0; j < 7; ++j) {
                float v;
                iss >> v;
                trans.push_back(v);
            }
            m_Transformations.push_back(trans);
        }

    }

protected:

    /**
    * @brief Renders Mandelbrot fractal.
    * @param width Window width.
    * @param height Window height.
    */
    virtual void Render(int width, int height) override {
        glPointSize(1.0f);
        glBegin(GL_POINTS);
        for (int i = 0; i < m_PointsNumber; ++i) {
            IRG::Math::Vector2d point;
            for (int j = 0; j < m_Limit; ++j) {
                float p = (float)(m_Random() % 10000) / 10000.0f;
                for (const std::vector<float>& trans : m_Transformations) {
                    if (p < trans[6]) {
                        point = { trans[0] * point[0] + trans[1] * point[1] + trans[4], trans[2] * point[0] + trans[3] * point[1] + trans[5] };
                        break;
                    } else {
                        p -= trans[6];
                    }
                }
            }
            glVertex3d(point[0] * m_Eta[0] + m_Eta[1], point[1] * m_Eta[2] + m_Eta[3], 0.0);
        }
        glEnd();
    }

private:

    /**
     * @brief IFS parameters.
     */
    int m_PointsNumber, m_Limit, m_Eta[4];
    std::vector<std::vector<float>> m_Transformations;

    /**
     * @brief Random device.
     */
    std::random_device m_RandomDevice;

    /**
     * @brief Default random engine.
     */
    std::default_random_engine m_Random;

};

/* Entry function. */
int main(int argc, char** argv) {

    /* Assure that the appropriate number of parameters is passed. */
    if (argc != 2) {
        std::cerr << "Usage: " << argv[0] << " ifs_file_name" << std::endl;
        return 1;
    }

    /* Create and handle window. */
    IFSFractal ifsf(argv[1]);
    ifsf.PerformMainLoop();

    /* Exit success. */
    return 0;

}
