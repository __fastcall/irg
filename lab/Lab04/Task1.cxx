#include <IRG/Math.hxx>
#include <GLFW/glfw3.h>
#include <vector>
#include <iostream>

enum class Status {
    PolyDefine,
    PointTest,
    __COUNT
};

using namespace IRG::Math;

static GLFWwindow* g_Window = nullptr;
static Status g_CurrentState = Status::PolyDefine;
static std::vector<Vector2i> g_Poly;
static bool g_FillPoly = false, g_ConvexOnly = false;

void scanlineDraw(const std::vector<Vector2i>& poly) {

    /* Ako poligon ima 2 ili manje bridova, zavrsi funkciju. */
    if (poly.size() <= 2)
        return;

    /* Niz bridova. */
    std::vector<Vector3i> edges(poly.size());

    /* Niz koji odredjuje jeli brid lijevi ili desni. */
    std::vector<bool> isLeft(poly.size());

    /* Pronadji minimume i maximume te odredi poredak definiranja, izracunaj brid i odredi jeli je lijevi ili desni. */
    int i, xmin, xmax, ymin, ymax;
    bool isCCW;
    xmin = xmax = poly[0][0];
    ymin = ymax = poly[0][1];
    edges[0] = Vector3i::CrossProduct(Vector3i::ToHomogeneous(poly[poly.size() - 1]), Vector3i::ToHomogeneous(poly[0]));
    isLeft[0] = poly[poly.size() - 1][1] >= poly[0][1];
    for (i = 1; i < poly.size(); ++i) {
        if (xmin > poly[i][0]) xmin = poly[i][0];
        if (xmax < poly[i][0]) xmax = poly[i][0];
        if (ymin > poly[i][1]) ymin = poly[i][1];
        if (ymax < poly[i][1]) ymax = poly[i][1];
        edges[i] = Vector3i::CrossProduct(Vector3i::ToHomogeneous(poly[i - 1]), Vector3i::ToHomogeneous(poly[i]));
        isLeft[i] = poly[i - 1][1] >= poly[i][1];
        int dp = Vector3i::ToHomogeneous(poly[(i + 1) % poly.size()]) * edges[i];
        if (dp != 0) {
            isCCW = (dp > 0);
            break;
        }
    }
    for (; i < poly.size(); ++i) {
        if (xmin > poly[i][0]) xmin = poly[i][0];
        if (xmax < poly[i][0]) xmax = poly[i][0];
        if (ymin > poly[i][1]) ymin = poly[i][1];
        if (ymax < poly[i][1]) ymax = poly[i][1];
        edges[i] = Vector3i::CrossProduct(Vector3i::ToHomogeneous(poly[i - 1]), Vector3i::ToHomogeneous(poly[i]));
        isLeft[i] = poly[i - 1][1] >= poly[i][1];
    }

    /* Zapocni crtati linije. */
    glColor3f(0.0f, 0.0f, 0.0f);
    glBegin(GL_LINES);

    /* Crtaj odozgo prema dolje scanlinijama. */
    double l, r;
    for (int y = ymax; y >= ymin; --y) {

        /* Postavi granicne vrijednosti. */
        l = xmin, r = xmax;

        /* Provjeri sve vrhove/bridove. */
        for (i = 0; i < poly.size(); ++i) {

            /* Vrh brida. */
            const Vector2i& current = poly[i];

            /* Ako je brid paralelan s x osi. */
            if ((edges[i][0] == 0) && (current[1] == y)) {
                const Vector2i& prev = poly[(i - 1) % poly.size()];
                if (current[0] > prev[0])
                    l = prev[0], r = current[0];
                else
                    l = current[0], r = prev[0];
                break;
            } else {
                double x = (-edges[i][1] * y - edges[i][2]) / (double)edges[i][0];
                if (isLeft[i] ^ isCCW) {
                    if (r > x) r = x;
                } else {
                    if (l < x) l = x;
                }
            }

        }

        /* Nacrtaj liniju. */
        glVertex2i(l, y);
        glVertex2i(r, y);

    }

    /* Zavrsi s crtanjem linija. */
    glEnd();

}

bool testConvex(const std::vector<Vector2i>& poly) {

    /* Trivijalni slucaj gdje poligon sadrzi 3 ili manje vrhova i automatski je konveksan. */
    if (poly.size() <= 3) {
        return true;
    }

    /* Jeli je (pretpostavljeni) smjer suprotan kazaljki na satu. */
    bool isCCW;

    /* Zajednicki iterator. */
    int i;

    /* Trazi prvi brid na kojemu ne lezi sljedeca tocka. */
    for (i = 0; i < g_Poly.size(); ++i) {
        Vector3i edge = Vector3i::CrossProduct(Vector3i::ToHomogeneous(g_Poly[i]), Vector3i::ToHomogeneous(g_Poly[(i + 1) % g_Poly.size()]));
        Vector3i point = Vector3i::ToHomogeneous(g_Poly[(i + 2) % g_Poly.size()]);
        int dp = edge * point;
        if (dp > 0) {
            isCCW = true;
            break;
        } else if (dp < 0) {
            isCCW = false;
            break;
        }
    }

    /* Provjeravaj jeli sljedeca tocka iznad ili ispod brida ovisno o smjeru definiranja. */
    if (isCCW)
        for (; i < g_Poly.size(); ++i) {
            Vector3i edge = Vector3i::CrossProduct(Vector3i::ToHomogeneous(g_Poly[i]), Vector3i::ToHomogeneous(g_Poly[(i + 1) % g_Poly.size()]));
            Vector3i point = Vector3i::ToHomogeneous(g_Poly[(i + 2) % g_Poly.size()]);
            if (edge * point <= 0) {
                return false;
            }
        }
    else
        for (; i < g_Poly.size(); ++i) {
            Vector3i edge = Vector3i::CrossProduct(Vector3i::ToHomogeneous(g_Poly[i]), Vector3i::ToHomogeneous(g_Poly[(i + 1) % g_Poly.size()]));
            Vector3i point = Vector3i::ToHomogeneous(g_Poly[(i + 2) % g_Poly.size()]);
            if (edge * point >= 0) {
                return false;
            }
        }

    /* Poligon je konveksan. */
    return true;

}

bool testPoint(const std::vector<Vector2i>& poly, const Vector2i& point) {

    /* Trivijalni slucaj gdje je samo jedna tocka u poligonu. */
    if (poly.size() <= 1) {
        return (poly[0] == point);
    }

    /* Jeli je (pretpostavljeni) smjer suprotan kazaljki na satu. */
    bool isCCW;

    /* Zajednicki iterator. */
    int i;

    /* Trazi prvi brid na kojemu ne lezi sljedeca tocka. */
    for (i = 0; i < g_Poly.size(); ++i) {
        Vector3i edge = Vector3i::CrossProduct(Vector3i::ToHomogeneous(g_Poly[i]), Vector3i::ToHomogeneous(g_Poly[(i + 1) % g_Poly.size()]));
        int dp = edge * Vector3i::ToHomogeneous(point);
        if (dp > 0) {
            isCCW = true;
            break;
        } else if (dp < 0) {
            isCCW = false;
            break;
        }
    }

    /* Provjeravaj jeli sljedeca tocka iznad ili ispod brida ovisno o smjeru definiranja. */
    if (isCCW)
        for (; i < g_Poly.size(); ++i) {
            Vector3i edge = Vector3i::CrossProduct(Vector3i::ToHomogeneous(g_Poly[i]), Vector3i::ToHomogeneous(g_Poly[(i + 1) % g_Poly.size()]));
            if (edge * Vector3i::ToHomogeneous(point) <= 0) {
                return false;
            }
        }
    else
        for (; i < g_Poly.size(); ++i) {
            Vector3i edge = Vector3i::CrossProduct(Vector3i::ToHomogeneous(g_Poly[i]), Vector3i::ToHomogeneous(g_Poly[(i + 1) % g_Poly.size()]));
            if (edge * Vector3i::ToHomogeneous(point) >= 0) {
                return false;
            }
        }

    /* Tocka pripada poligonu. */
    return true;

}

void resizeCbck(GLFWwindow* win, int width, int height) {

    /* Promijeni velicinu viewporta i ortografske projekcije. */
    glViewport(0, 0, width, height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0, width, 0, height, 0, 1);
    glMatrixMode(GL_MODELVIEW);

}

void keyCbck(GLFWwindow* win, int key, int scancode, int action, int mods) {

    /* Prihvati samo key down evente. */
    if (action == GLFW_PRESS) {
        switch (key) {
        case GLFW_KEY_K:
            if ((!g_ConvexOnly) && (!testConvex(g_Poly))) {
                std::cerr << "GRESKA: Poligon nije konveksan." << std::endl;
                break;
            }
            glClearColor(static_cast<float>(g_ConvexOnly), 1.0f, static_cast<float>(g_ConvexOnly), 1.0f);
            g_ConvexOnly = !g_ConvexOnly;
            std::cout << "Samo konveksni poligoni: " << (g_ConvexOnly ? "DA" : "NE") << std::endl;
            break;
        case GLFW_KEY_P:
            if (g_CurrentState == Status::PolyDefine) {
                g_FillPoly = !g_FillPoly;
                std::cout << "Popunjavanje poligona: " << (g_FillPoly ? "DA" : "NE") << std::endl;
                break;
            }
            std::cerr << "GRESKA: Nije moguce promijeniti u ovom stanju." << std::endl;
            break;
        case GLFW_KEY_N:
            if ((g_CurrentState = static_cast<Status>((static_cast<int>(g_CurrentState) + 1) % static_cast<int>(Status::__COUNT))) == Status::PolyDefine) {
                g_Poly.clear();
                g_ConvexOnly = g_FillPoly = false;
                glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
            }
            std::cout << "STANJE: " << ((g_CurrentState == Status::PolyDefine) ? "Definiranje poligona" : "Provjera pripadnosti tocke") << std::endl;
            break;
        case GLFW_KEY_C:
            g_Poly.clear();
            break;
        }
    }

}

void mousebtnCbck(GLFWwindow* win, int button, int action, int mods) {

    /* Prihvati samo lijevi klik misa. */
    if ((action == GLFW_PRESS) && (button == GLFW_MOUSE_BUTTON_LEFT)) {
        
        /* Dohvati poziciju klika. */
        double x, y;
        int width, height;
        glfwGetCursorPos(g_Window, &x, &y);
        glfwGetWindowSize(g_Window, &width, &height);
        Vector2i point = { (int)x, height - (int)y };

        if (g_CurrentState == Status::PolyDefine) {
            
            /* Dodaj novi vrh. */
            g_Poly.push_back(point);

            /* Provjera konveksnosti. */
            if (g_ConvexOnly && !testConvex(g_Poly)) {
                g_Poly.pop_back();
                std::cerr << "GRESKA: Zeljeni vrh " << point << " nije konveksan." << std::endl;
                return;
            }

            /* Ispisi poruku. */
            std::cout << "Vrh " << point << " je dodan." << std::endl;

        } else if (g_CurrentState == Status::PointTest) {

            /* Ispisi jeli je vrh u poligonu. */
            if (testPoint(g_Poly, point)) {
                std::cout << "Vrh " << point << " je unutar poligona." << std::endl;
            } else {
                std::cout << "Vrh " << point << " je izvan poligona." << std::endl;
            }

        }

    }

}

void render() {

    /* Pocisti boju. */
    glClear(GL_COLOR_BUFFER_BIT);

    /* Iscrtaj poligon. */
    if ((g_FillPoly) && g_Poly.size() > 2) {
        scanlineDraw(g_Poly);
    } else {
        glColor3f(0.0f, 0.0f, 0.0f);
        if (g_Poly.size() >= 2) {
            glBegin(GL_LINE_LOOP);
            for (const Vector2i& point : g_Poly) {
                glVertex2i(point[0], point[1]);
            }
        } else if (g_Poly.size() == 1) {
            glBegin(GL_POINTS);
            glVertex2i(g_Poly[0][0], g_Poly[0][1]);
        }
        glEnd();
    }

    /* Zamijeni medjuspremnike. */
    glfwSwapBuffers(g_Window);

}

int main() {

    /* Iniciraj GLFW prozor. */
    glfwInit();
    g_Window = glfwCreateWindow(500, 500, "Scanline popunjavanje poligona", nullptr, nullptr);
    glfwSetWindowSizeCallback(g_Window, resizeCbck);
    glfwSetKeyCallback(g_Window, keyCbck);
    glfwSetMouseButtonCallback(g_Window, mousebtnCbck);
    glfwMakeContextCurrent(g_Window);

    /* Postavi projekcijsku matricu. */
    resizeCbck(g_Window, 500, 500);

    /* Postavi boju ciscenja. */
    glClearColor(1.0f, 1.0f, 1.0f, 1.0f);

    /* Glavna petlja. */
    while (!glfwWindowShouldClose(g_Window)) {
        glfwPollEvents();
        render();
    }

    /* Terminiraj GLFW. */
    glfwTerminate();
    return 0;

}
