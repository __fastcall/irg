#include <IRG/Model/Mesh.hxx>
#include <IRG/Math/VectorComparator.hxx>
#include <limits>
#include <list>
#include <cmath>

namespace IRG { namespace Model {

    Mesh::Mesh() {

        /* Initialize arrays. */
        m_Vertices = std::unique_ptr<std::vector<Math::Vector3f>>(new std::vector<Math::Vector3f>);
        m_TexCoords = std::unique_ptr<std::vector<Math::Vector2f>>(new std::vector<Math::Vector2f>);
        m_Normals = std::unique_ptr<std::vector<Math::Vector3f>>(new std::vector<Math::Vector3f>);
        m_Faces = std::unique_ptr<std::vector<std::vector<Math::Vector3i>>>(new std::vector<std::vector<Math::Vector3i>>);

    }

    void Mesh::CenterVertices() {

        /* Find minimums and maximums if possible. */
        Math::Vector2f xMinMax, yMinMax, zMinMax;
        if (!CalculateMinMax(xMinMax, yMinMax, zMinMax))
            return;

        /* Find relative adjustment vector. */
        Math::Vector3f adjustment = { (xMinMax[0] + xMinMax[1]) / 2.0f, (yMinMax[0] + yMinMax[1]) / 2.0f, (zMinMax[0] + zMinMax[1]) / 2.0f };

        /* Adjust all vertices. */
        for (Math::Vector3f& vertex : *m_Vertices) {
            vertex -= adjustment;
        }

    }

    void Mesh::NormalizeVertices(bool center) {

        /* Find minimums and maximums if possible. */
        Math::Vector2f xMinMax, yMinMax, zMinMax;
        if (!CalculateMinMax(xMinMax, yMinMax, zMinMax))
            return;

        /* Find relative adjustment vector. */
        Math::Vector3f adjustment = { (xMinMax[0] + xMinMax[1]) / 2.0f, (yMinMax[0] + yMinMax[1]) / 2.0f, (zMinMax[0] + zMinMax[1]) / 2.0f };

        /* Find greatest scale. */
        float xWidth = xMinMax[1] - xMinMax[0];
        float yWidth = yMinMax[1] - yMinMax[0];
        float zWidth = zMinMax[1] - zMinMax[0];
        float scale = ((xWidth > yWidth) ? ((xWidth > zWidth) ? xWidth : zWidth) : ((yWidth > zWidth) ? yWidth : zWidth)) / 2.0f;

        /* Adjust all vertices. */
        for (Math::Vector3f& vertex : *m_Vertices) {
            vertex -= adjustment;
            vertex /= scale;
            if (!center) vertex += adjustment;
        }

    }

    bool Mesh::CalculateMinMax(Math::Vector2f& xMinMax, Math::Vector2f& yMinMax, Math::Vector2f& zMinMax) const {

        /* Return false if there are no vertices. */
        if (m_Vertices->empty())
            return false;

        /* Initialize minimums and maximums for each of the components. */
        xMinMax[0] = yMinMax[0] = zMinMax[0] = std::numeric_limits<float>::max();
        xMinMax[1] = yMinMax[1] = zMinMax[1] = std::numeric_limits<float>::min();

        /* Find minimums and maximums. */
        for (const Math::Vector3f& vertex : *m_Vertices) {
            if (xMinMax[0] > vertex[0]) xMinMax[0] = vertex[0]; 
            if (xMinMax[1] < vertex[0]) xMinMax[1] = vertex[0];
            if (yMinMax[0] > vertex[1]) yMinMax[0] = vertex[1]; 
            if (yMinMax[1] < vertex[1]) yMinMax[1] = vertex[1];
            if (zMinMax[0] > vertex[2]) zMinMax[0] = vertex[2]; 
            if (zMinMax[1] < vertex[2]) zMinMax[1] = vertex[2];
        }

        /* Return true to indicate success. */
        return true;

    }

    void Mesh::CalculateNormals(bool isCCW) {

        /* Clear existing normals. */
        m_Normals->clear();

        /* Calculate normals for each face. */
        for (std::vector<Math::Vector3i>& face : *m_Faces) {

            /* Get first three vertices. */
            Math::Vector3f v0 = (*m_Vertices)[face[0][0]];
            Math::Vector3f v1 = (*m_Vertices)[face[1][0]];
            Math::Vector3f v2 = (*m_Vertices)[face[2][0]];

            /* Calculate two edge directions. */
            Math::Vector3f p = v1 - v0;
            Math::Vector3f k = v2 - v0;

            /* Calculate a face normal. */
            Math::Vector3f normal = (isCCW ? Math::Vector3f::CrossProduct(p, k) : Math::Vector3f::CrossProduct(k, p)).GetNormalized();

            /* Assign normal to face. */
            for (Math::Vector3i& index : face) {
                index[2] = m_Normals->size();
            }
            m_Normals->push_back(normal);

        }

    }

    PositionState Mesh::TestPosition(const Math::Vector3f& position, bool isCCW) const {

        /* List of worked out hits. */
        std::list<Math::Vector4f> hits;

        /* Counts the amount of collisions. */
        int collisionCount = 0;

        /* Test position with each face. */
        for (std::vector<Math::Vector3i>& face : *m_Faces) {

            /* Get first three vertices. */
            Math::Vector3f v0 = (*m_Vertices)[face[0][0]];
            Math::Vector3f v1 = (*m_Vertices)[face[1][0]];
            Math::Vector3f v2 = (*m_Vertices)[face[2][0]];

            /* Calculate two edge directions. */
            Math::Vector3f p = v1 - v0;
            Math::Vector3f k = v2 - v0;

            /* Calculate a face normal. */
            Math::Vector3f normal = (isCCW ? Math::Vector3f::CrossProduct(p, k) : Math::Vector3f::CrossProduct(k, p)).GetNormalized();

            /* Calculate a face plane. */
            Math::Vector4f plane(normal, -normal * v0);

            /* Check if position lays on the plane. */
            if (std::fabs(Math::Vector4f::ToHomogeneous(position) * plane) < 1e-6) {

                /* Indicates whether the loop was broken. */
                bool brokenLoop = false;

                /* Test position with each edge. */
                for (int i = 0; i < face.size(); ++i) {

                    /* Get current and previous vertices. */
                    Math::Vector3f currVertex = (*m_Vertices)[face[(i + 1) % face.size()][0]];
                    Math::Vector3f prevVertex = (*m_Vertices)[face[i][0]];

                    /* Get edge direction. */
                    Math::Vector3f edge = currVertex - prevVertex;

                    /* Calculate edge normal. */
                    Math::Vector3f edgeNormal = (isCCW ? Math::Vector3f::CrossProduct(normal, edge) : Math::Vector3f::CrossProduct(edge, normal)).GetNormalized();

                    /* Calculate edge plane. */
                    Math::Vector4f edgePlane(edgeNormal, -edgeNormal * currVertex);

                    /* Check whether position is under the edge plane. */
                    if (Math::Vector4f::ToHomogeneous(position) * edgePlane < -1e-6) {
                        brokenLoop = true;
                        break;
                    }

                }

                /* Position is layed on edge of the object. */
                if (!brokenLoop) return PositionState::OnEdge;

            }

            /* Right axis [1 0 0] raycast hit test. */
            if (plane[0] != 0.0f) {

                /* Find collided x coordinate. */
                float x = (-plane[1] * position[1] - plane[2] * position[2] - plane[3]) / plane[0];

                /* Check that x is on the right side of the position. */
                if (x >= position[0]) {
                    
                    /* Construct ray hit point. */
                    Math::Vector4f rayHit = { x, position[1], position[2], 1.0f };

                    /* Indicates whether the loop was broken. */
                    bool brokenLoop = false;

                    /* Check whether current hit was already worked out. */
                    for (const Math::Vector4f& hit : hits) {
                        if (Math::VectorComparator4f::AreEqual(hit, rayHit)) {
                            brokenLoop = true;
                            break;
                        }
                    }

                    /* Skip processing if current hit was already worked out with a different face. */
                    if (brokenLoop) continue;

                    /* Test ray hit point with each edge. */
                    for (int i = 0; i < face.size(); ++i) {

                        /* Get current and previous vertices. */
                        Math::Vector3f currVertex = (*m_Vertices)[face[(i + 1) % face.size()][0]];
                        Math::Vector3f prevVertex = (*m_Vertices)[face[i][0]];

                        /* Get edge direction. */
                        Math::Vector3f edge = currVertex - prevVertex;

                        /* Calculate edge normal. */
                        Math::Vector3f edgeNormal = (isCCW ? Math::Vector3f::CrossProduct(normal, edge) : Math::Vector3f::CrossProduct(edge, normal)).GetNormalized();

                        /* Calculate edge plane. */
                        Math::Vector4f edgePlane(edgeNormal, -edgeNormal * currVertex);

                        /* Check whether ray hit point is under the edge plane. */
                        if (rayHit * edgePlane < -1e-6) {
                            brokenLoop = true;
                            break;
                        }

                    }

                    /* Remember hit and increment collision count. */
                    if (!brokenLoop) {
                        hits.push_back(rayHit);
                        collisionCount++;
                    }

                }

            }

        }

        return (collisionCount % 2 == 0) ? PositionState::Outside : PositionState::Inside;

    }

} }
